#include "value.h"

#include <sstream>
#include "log_utils.h"


#ifdef CLOUD_COMPILE_VARIANT
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>
#endif



#undef  LOG_DEFAULT_CATEGORY
#define LOG_DEFAULT_CATEGORY PARSER

namespace cloud {

std::string value_to_string(const value &v)
{
	std::stringstream ss;
	ss << v;
	return ss.str();
}

namespace detail {

struct print_cloud_type: public boost::static_visitor<std::string>
{
	std::string operator() (const bool &) const {
		return "bool";
	}

	std::string operator() (const contact &) const {
		return "contact";
	}

	std::string operator() (const double &) const {
		return "double";
	}

	std::string operator() (const duration &) const {
		return "duration";
	}

	std::string operator() (const integer &) const {
		return "integer";
	}
	std::string operator() (const string &) const {
		return "string";
	}

	std::string operator() (const time &) const {
		return "time";
	}

	std::string operator() (const list<value> &l) const {
		if (l.size() == 0) {
//			LOG(WARN, "Nie znamy niestety zadnego elementu listy wiec nie mamy sensownego typu, ktory mozna by tu wrzucic");
			return "list";
		}
		return std::string() + "list of " + boost::apply_visitor(print_cloud_type(), l.front());
	}

	std::string operator() (const set<value> &s) const {
		if (s.size() == 0) {
//			LOG(WARN, "Nie znamy niestety zadnego elementu zbioru wiec nie mamy sensownego typu, ktory mozna by tu wrzucic");
			return "set";
		}
		return std::string() + "set of " + boost::apply_visitor(print_cloud_type(), *(s.begin()));
	}
};

}

std::string value_type_to_string(const value &v)
{
	return boost::apply_visitor(detail::print_cloud_type(), v);
}

namespace detail {

struct check_cloud_type: public boost::static_visitor<bool>
{
	value_type t;
	check_cloud_type(const value_type &t): t(t) {}

	bool operator() (const bool &) const {
		return t.collection_.size() == 0 && t.type_ == SimpleType::BOOL;
	}

	bool operator() (const contact &) const {
		return t.collection_.size() == 0 && t.type_ == SimpleType::CONTACT;
	}

	bool operator() (const double &) const {
		return t.collection_.size() == 0 && t.type_ == SimpleType::DOUBLE;
	}

	bool operator() (const duration &) const {
		return t.collection_.size() == 0 && t.type_ == SimpleType::DURATION;
	}

	bool operator() (const integer &) const {
		return t.collection_.size() == 0 && t.type_ == SimpleType::INTEGER;
	}
	bool operator() (const string &) const {
		return t.collection_.size() == 0 && t.type_ == SimpleType::STRING;
	}

	bool operator() (const time &) const {
		return t.collection_.size() == 0 && t.type_ == SimpleType::TIME;
	}

	bool operator() (const list<value> &l) const {
		if (t.collection_.size() == 0 || t.collection_.front() != CollectionType::LIST) {
			return false;
		}
		value_type new_t = t;
		new_t.collection_.erase(new_t.collection_.begin());
		//Note jesli lista jest pusta automatycznie zwrocimy true
		for (auto v: l) {
			if (!boost::apply_visitor(check_cloud_type(new_t), v)) {
				return false;
			}
		}
		return true;
	}

	bool operator() (const set<value> &s) const {
		if (t.collection_.size() == 0 || t.collection_.front() != CollectionType::SET) {
			return false;
		}
		value_type new_t = t;
		new_t.collection_.erase(new_t.collection_.begin());
		//Note jesli zbior jest pusta automatycznie zwrocimy true
		for (auto v: s) {
			if (!boost::apply_visitor(check_cloud_type(new_t), v)) {
				return false;
			}
		}
		return true;
	}
};

}


bool check_type_compatibility(const value &v, const value_type &t)
{
	return boost::apply_visitor(detail::check_cloud_type(t), v);
}

void check_type_compatibility_throw(const value &v, const value_type &t)
{
	if (!check_type_compatibility(v, t)) {
		LOG(WARN, "typ jest niepoprawny" LG(t, v));
		throw std::runtime_error("typ jest niepoprawny");
	}
}


value get_empty_value(const value_type &t)
{
	if (!t.collection_.empty()) {
		switch(t.collection_.front()) {
		case cloud::CollectionType::LIST:
			return list<value>();
		case cloud::CollectionType::SET:
			return set<value>();
		}
	}
	switch (t.type_) {
	case cloud::SimpleType::BOOL:
		return false;
	case cloud::SimpleType::CONTACT:
		return contact();
	case cloud::SimpleType::DOUBLE:
		return 0.0;
	case cloud::SimpleType::DURATION:
		return duration();
	case cloud::SimpleType::INTEGER:
		return integer(0);
	case cloud::SimpleType::STRING:
		return string("");
	case cloud::SimpleType::TIME:
		return time(0);
	}
}

value simple_value_from_string(SimpleType st, const std::string &str)
{
	switch (st) {
	case cloud::SimpleType::BOOL:
		if (str == "true") {
			return value(true);
		} else if (str == "false") {
			return value(false);
		}
		break;

	case cloud::SimpleType::CONTACT:
	{
		contact c;
		if (c.fromString(str)) {
			return value(c);
		}
	}
		break;

	case cloud::SimpleType::DOUBLE:
		return value(std::stod(str));

	case cloud::SimpleType::DURATION:
	{
		duration d;
		if (d.from_string(str)) {
			return value(d);
		}
	}
		break;

	case cloud::SimpleType::INTEGER:
		return value(integer(std::stoi(str)));

	case cloud::SimpleType::STRING:
		if (str.size() >= 2 && str[0] == '\"' && str[str.size() - 1] == '\"')	 {
			return value(str.substr(1, str.size() - 2));
		}
		break;

	case cloud::SimpleType::TIME:
	{
		time t;
		if (t.from_string(str)) {
			return value(t);
		}
	}
		break;

	}
	LOG(WARN, "Invalid simple_value_from_string" LG(st, str));
	throw std::runtime_error("simple_value_from_string");
}

static std::vector<std::string> split_collection_helper(const std::string &str)
{
	static constexpr bool kListParenthesis = true;
	static constexpr bool kSetParenthesis = false;
	std::vector<std::string> elem_vec;
	size_t begin = 0, curr = 0;
	std::list<bool> parenthesis;
	while (curr < str.size()) {
		switch (str[curr]) {
		case ',':
			if (parenthesis.empty()) {
				elem_vec.push_back(str.substr(begin, curr - begin));
				begin = curr + 1;
			}
			break;
		case '[':
			parenthesis.push_back(kListParenthesis);
			break;
		case ']':
			if (parenthesis.empty() || !parenthesis.front()) {
				LOG(WARN, "Invalid close list parenthesis");
				throw std::runtime_error("Invalid close list parenthesis");
			} else {
				parenthesis.pop_front();
			}
			break;


		case '{':
			parenthesis.push_back(kSetParenthesis);
			break;

		case '}':
			if (parenthesis.empty() || parenthesis.front()) {
				LOG(WARN, "Invalid close set parenthesis");
				throw std::runtime_error("Invalid close set parenthesis");
			} else {
				parenthesis.pop_front();
			}
			break;

		default:
			break;
		}
		++curr;
	}
	return elem_vec;
}


value value_from_string(const value_type &t, const std::string &str)
{
	std::string trimmed_str = str;
	boost::trim(trimmed_str);
	if (trimmed_str.empty()) {
		LOG(WARN, "Invalid value_from_string empty string");
		throw std::runtime_error("Invalid value_from_string empty string");
	}
	if (t.collection_.size() > 0) {
		LOG("value_from_string parsing collection" LG(t, trimmed_str));
		char beg, end;
		switch (t.collection_.front()) {
		case CollectionType::LIST:
			beg = '[';
			end = ']';
			break;
		case CollectionType::SET:
			beg = '{';
			end = '}';
			break;
		}

		if (trimmed_str.size() < 2 || trimmed_str[0] != beg || trimmed_str[trimmed_str.size() - 1] != end) {
			LOG(WARN, "Invalid collection type begin or end" LG(t, trimmed_str));
			throw std::runtime_error("Invalid collection type begin or end");
		}

		trimmed_str = trimmed_str.substr(1, trimmed_str.size() - 2);
		std::vector<std::string> elem_vec = split_collection_helper(trimmed_str);
		switch (t.collection_.front()) {
		case CollectionType::LIST:
		{
			std::list<value> list_v;
			for (auto s: elem_vec) {
				list_v.push_back(value_from_string(t.strip_collection_layer(), s));
			}
			return list_v;
		}
			break;

		case CollectionType::SET:
		{
			std::set<value> set_v;
			for (auto s: elem_vec) {
				set_v.insert(value_from_string(t.strip_collection_layer(), s));
			}
			return set_v;
		}
			break;
		}
	}

	return simple_value_from_string(t.type_, trimmed_str);
}

struct print_boost_variant_value: public boost::static_visitor<void>
{
	std::ostream &os;
	print_boost_variant_value(std::ostream &os): os(os) {}
	template<typename T>
	void operator() (const T &v) const {
		os << v;
	}
};

} /* namespace cloud */


std::ostream & operator <<(std::ostream & os, const cloud::value &v)
{
	boost::apply_visitor(cloud::print_boost_variant_value(os), v);
	return os;
}
