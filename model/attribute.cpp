#include "attribute.h"

std::ostream & operator << (std::ostream & os, const cloud::Attribute &v)
{
	os << v.name_ << ": " << v.type_ << " = " << v.value_;
	return os;
}
