#include "value_types.h"

#include <chrono>
#include <ctime>
#include <sstream>

#include "log_utils.h"

#ifdef CLOUD_COMPILE_VARIANT
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#endif

#undef  LOG_DEFAULT_CATEGORY
#define LOG_DEFAULT_CATEGORY PARSER

namespace cloud {

template<>
const std::map<std::string, SimpleType> cloudFromStringMapWrapper<SimpleType>::map =
{
	{"bool", SimpleType::BOOL},
	{"contact", SimpleType::CONTACT},
	{"double", SimpleType::DOUBLE},
	{"duration", SimpleType::DURATION},
	{"integer", SimpleType::INTEGER},
	{"string", SimpleType::STRING},
	{"time", SimpleType::TIME}
};

template<>
const std::map<std::string, CollectionType> cloudFromStringMapWrapper<CollectionType>::map =
{
	{"list", CollectionType::LIST},
	{"set", CollectionType::SET}
};

value_type value_type::strip_collection_layer() const
{
	if (collection_.empty()) {
		LOG(ERROR, "proboujemy usunac poziom kolekcji a jestesmy prymitywem" LG(*this));
		throw std::runtime_error("proboujemy usunac poziom kolekcji, a jestesmy prymitywem");
	}
	return value_type(type_, std::list<CollectionType>(++collection_.begin(), collection_.end()));
}

bool value_type::fromString(const std::string &str)
{
	std::vector<std::string> types_str;
	boost::split(types_str, str, boost::is_any_of(" "));
	if (types_str.size() == 0) {
		LOG("pusta lista stringow");
		return false;
	}
	bool is_type = true;
	std::list<CollectionType> col_types;
	SimpleType st;
	try {
		for (size_t i = 0; i < types_str.size() - 1; ++i) {
			if (!types_str[i].empty()) {
				if (is_type) {
					col_types.push_back(cloudFromString<CollectionType>(types_str[i]));
				} else {
					if (types_str[i] != "of") {
						LOG("powinnismy miec tutaj of");
						throw std::runtime_error("powinnismy miec tutaj of");
					}
				}
				is_type = !is_type;
			}
		}
		st = cloudFromString<SimpleType>(types_str[types_str.size() - 1]);
	} catch (std::exception &e) {
		LOG(ERROR, "value_type::fromString proboujemy usunac poziom kolekcji a jestesmy prymitywem" LG(str, e.what()));
		return false;
	}
	collection_ = col_types;
	type_ = st;
	return true;
}

const value_type bool_vt = value_type(SimpleType::BOOL);
const value_type contact_vt = value_type(SimpleType::CONTACT);
const value_type double_vt = value_type(SimpleType::DOUBLE);
const value_type duration_vt = value_type(SimpleType::DURATION);
const value_type integer_vt = value_type(SimpleType::INTEGER);
const value_type string_vt = value_type(SimpleType::STRING);
const value_type time_vt = value_type(SimpleType::TIME);
const value_type set_contact_vt = value_type(SimpleType::CONTACT, std::list<CollectionType>{CollectionType::SET});
const value_type set_string_vt = value_type(SimpleType::STRING, std::list<CollectionType>{CollectionType::SET});
const value_type list_string_vt = value_type(SimpleType::STRING, std::list<CollectionType>{CollectionType::LIST});

//contact

const ip localhost = ip(0x7f000001);

std::string contact_to_string(const contact &c)
{
	std::stringstream ss;
	ss << c;
	return ss.str();
}

contact contact_from_string_throw(const std::string &str)
{
	size_t i = 0;
	while (i < str.size() && str[i] != ':') ++i;
	if (i == str.size()) {
		LOG(WARN, "to_contact_conversion_visitor invalid string to contact conversion" LG(str));
		throw std::runtime_error("to_contact_conversion_visitor invalid string to contact conversion");
	}
	std::string addr_str = str.substr(0, i), port_str = str.substr(i + 1, str.size());
	boost::asio::ip::address_v4 addr;
	addr = addr.from_string(addr_str);
	unsigned long port = std::stoul(port_str);
	if (port > 65536) {
		LOG(WARN, "to_contact_conversion_visitor invalid port" LG(str, port));
		throw std::runtime_error("to_contact_conversion_visitor invalid port");
	}
	return contact(addr.to_ulong(), static_cast<std::uint16_t>(port));
}

bool contact::fromString(const std::string &contact_s)
{
	std::vector<std::string> contact_v;
	boost::split(contact_v, contact_s, boost::is_any_of(":"));
	if (contact_v.size() != 2) {
		LOG("contact::fromString err" LG(contact_s, contact_s));
		return false;
	}
	try {
		return fromString(contact_v[0], std::stoul(contact_v[1]));
	} catch (std::exception &e) {
		LOG("contact::fromString err" LG(contact_s, contact_s, e.what()));;
		return false;
	}
}

bool contact::fromString(const std::string &ip_s, std::uint16_t p) {
	boost::system::error_code ec;
	boost::asio::ip::address_v4 boost_ip;
	boost_ip = boost_ip.from_string(ip_s, ec);
	if (ec) {
		LOG("contact::fromString err" LG(ip_s, p, ec.message()));
		return false;
	}
	ip = boost_ip.to_ulong();
	port = p;
	return true;
}
std::set<contact> contact::setFromString(const std::string &contacts_string)
{
	std::vector<std::string> contacts_v;
	std::set<contact> contacts_set;
	boost::split(contacts_v, contacts_string, boost::is_any_of(", "));
	for (auto str: contacts_v) {
		if (!str.empty()) {
			cloud::contact c;
			if (!c.fromString(str)) {
				return std::set<contact>();
			}
			contacts_set.insert(c);
		}
	}
	return contacts_set;
}

bool contact::isSet() const {
	return ip != 0 && port != 0;
}

//duration

std::string duration::to_string() const
{
	struct tm * timeinfo;
	time_t t = std::abs(v) / 1000;
	char buffer[30];
	timeinfo = localtime(&t);
	size_t s = strftime(buffer, 30, ":%M:%S", timeinfo);
	std::string minutes_val(buffer, buffer + s);
	std::string ms_val = std::string() + "00" + std::to_string(std::abs(v) % 1000);
	integer days = v / (24 * 3600 * 1000);
	std::string hours_val = std::string() + "0" + std::to_string((std::abs(v) / (3600 * 1000)) % 24);
	std::string pref = (v > 0) ? "+" : "-";
	return pref + std::to_string(days) + " " + hours_val.substr(hours_val.size() - 2) + minutes_val + "." + ms_val.substr(ms_val.size() - 3);
}

bool duration::from_string(const std::string &str)
{
	bool is_plus;
	switch (str[0]) {
	case '+':
		is_plus = true;
		break;
	case '-':
		is_plus = false;
		break;
	default:
		LOG(WARN, "unable to convert from string invalid begin" LG(str));
		return false;
	}
	size_t i = 2;	//musimy miec przynajmniej 0 na poczatku
	while (i < str.size() && str[i] != ' ') ++i;
	// +0 00:00:00.000
	// 0123456789012345
	if (str.size() - i != 13) {
		LOG(WARN, "unable to convert from string not enough chars on end" LG(i, str));
		return false;
	}
	integer days, ms;
	try {
		days = std::stoul(str.substr(1, i));
		ms = std::stoul(str.substr(str.size() - 3));
	} catch (std::exception &e) {
		LOG(WARN, "unable to convert from string could not convert from str to int" LG(i, str));
		return false;
	}

	struct tm tm;
	char* res = strptime(str.substr(i + 1, str.size() - 4).c_str(), "%H:%M:%S", &tm);
	if (res == nullptr) {
		LOG(WARN, "unable to get strptime" LG(i, str));
		return false;
	}
	v = (days * 24 * 3600 * 1000)
			+ (tm.tm_hour * 3600 * 1000)
			+ (tm.tm_min * 60 * 1000)
			+ (tm.tm_sec * 1000)
			+ ms;
	v = is_plus ? v : -v;
	return true;
}

duration duration::from_string_throw(const std::string &str) {
	duration d;
	if (!d.from_string(str)) {
		throw std::runtime_error("unable to convert from string");
	}
	return d;
}

//time

time time::now()
{
	return time(getCurrentTime());
}

time time::epoch()
{
	struct tm timeinfo;
	timeinfo.tm_sec = 0;
	timeinfo.tm_min = 0;
	timeinfo.tm_hour = 0;
	timeinfo.tm_mday = 1;
	timeinfo.tm_mon = 0;
	timeinfo.tm_year = 100;
	size_t val = mktime(&timeinfo);
	LOG("epoch result" LG(val, time(val * 1000)));
	return time(val * 1000);
}

std::string time::to_string() const
{
	struct tm * timeinfo;
	time_t t = v / 1000;
	char buffer[30];
	timeinfo = localtime(&t);
	size_t s = strftime(buffer, 30, "%Y/%m/%d %H:%M:%S", timeinfo);
	std::string result(buffer, buffer + s);
	std::string ms_val = std::string() + "00" + std::to_string(v % 1000);
	return result + "." + ms_val.substr(ms_val.size() - 3);
}

bool time::from_string(const std::string &str)
{
	struct tm tm;
	char* res = strptime(str.substr(0, str.size() - 4).c_str(), "%Y/%m/%d %H:%M:%S", &tm);
	if (res == nullptr) {
		return false;
	}
	integer t = mktime(&tm);
	try {
		integer ms = std::stoul(str.substr(str.size() - 3));
		v = t * 1000 + ms;
	} catch (std::exception &e) {
		LOG(WARN, "unable to convert from string" LG(str, e.what()));
		return false;
	}
	return true;
}

time time::from_string_throw(const std::string &str) {
	time t;
	if (!t.from_string(str)) {
		throw std::runtime_error("unable to convert from string");
	}
	return t;
}

zone_path getZonePath(const ZmiFreshness &zmi)
{
	zone_path r;
	for (auto z: zmi) {
		r.push_back(z.first);
	}
	return r;
}

std::set<zone_path> getAllPaths(const zone_path &path)
{
	zone_path curr;
	std::set<zone_path> r;
	for (auto z: path) {
		curr.push_back(z);
		r.insert(curr);
	}
	return r;
}

} /* namespace cloud */

std::ostream & operator <<(std::ostream & os, const cloud::SimpleType &v)
{
	switch (v) {
	case cloud::SimpleType::BOOL:
		os << "bool";
		break;
	case cloud::SimpleType::CONTACT:
		os << "contact";
		break;
	case cloud::SimpleType::DOUBLE:
		os << "double";
		break;
	case cloud::SimpleType::DURATION:
		os << "duration";
		break;
	case cloud::SimpleType::INTEGER:
		os << "integer";
		break;
	case cloud::SimpleType::STRING:
		os << "string";
		break;
	case cloud::SimpleType::TIME:
		os << "time";
		break;
	}
	return os;
}

std::ostream & operator <<(std::ostream & os, const cloud::CollectionType &v)
{
	switch (v) {
	case cloud::CollectionType::LIST:
		os << "list";
		break;
	case cloud::CollectionType::SET:
		os << "set";
		break;
	}
	return os;
}

std::ostream & operator <<(std::ostream & os, const cloud::value_type &v)
{
	for (auto t: v.collection_) {
		os << t << " of ";
	}
	os << v.type_;
	return os;
}

std::ostream & operator << (std::ostream & os, const cloud::contact &v)
{
	os << boost::asio::ip::address_v4(v.ip).to_string()  << ":" << v.port;
	return os;
}

std::ostream & operator << (std::ostream & os, const cloud::duration &v)
{
	os << v.to_string();
	return os;
}

std::ostream & operator << (std::ostream & os, const cloud::time &v)
{
	os << v.to_string();
	return os;
}

