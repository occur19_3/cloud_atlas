#include <iostream>

#include <boost/program_options.hpp>

#include "crypto.h"

int main(int argc, char* argv[])
{
	std::string public_key_file;
	std::string secret_key_file;
	std::uint32_t seed;

	boost::program_options::options_description desc("CloudAtlas KeyGenerator options");
	desc.add_options()
		("help", "Shows help message")
		("public-key-file,p", boost::program_options::value<std::string>(&public_key_file)->default_value("public_key.key"), "Public key outfile")
		("secret-key-file,s", boost::program_options::value<std::string>(&secret_key_file)->default_value("secret_key.key"), "Secret key outfile")
		("seed,S", boost::program_options::value<std::uint32_t>(&seed)->default_value(1), "Key generation seed")
	;

	try {
		boost::program_options::variables_map vm;
		boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(desc).run(), vm);
		boost::program_options::notify(vm);
		if (vm.count("help")) {
			std::cout << desc;
			return 0;
		}
		if (public_key_file == secret_key_file) {
			throw std::logic_error("public and secret key files must be different!");
		}
	} catch (std::exception &e) {
		std::cout << "Invalid arguments provided: " << e.what() << std::endl;
		std::cout << "Check --help for usage" << std::endl;
		return 1;
	}

	std::cerr << "Generating keys" << std::endl;
	auto keys =  cloud::GenerateKeyPair(seed);


	std::cerr << "Writing keys to files" << std::endl;
	cloud::write_to_file(public_key_file, std::get<0>(keys));
	cloud::write_to_file(secret_key_file, std::get<1>(keys));


	std::cerr << "Reading from files" << std::endl;
	cloud::PublicKey pk;
	cloud::PrivateKey sk;
	if (!cloud::read_from_file(public_key_file, pk)
			|| !cloud::read_from_file(secret_key_file, sk)) {
		std::cout << "Problem z odczytem zapisanych kluczy" << std::endl;
		return 2;
	}


	std::cerr << "Signing test_query" << std::endl;
	std::string test_query = "testowe query do sprawdzenia poprawnosci zapisu i odczytu kluczy";
	cloud::time timestamp = cloud::time::now();
	cloud::time validity = cloud::time::now() + cloud::duration(3600 * 1000);
	cloud::Signature signature1 = cloud::signQuery(test_query, true, validity, timestamp, std::get<1>(keys), 0);
	cloud::Signature signature2 = cloud::signQuery(test_query, true, validity, timestamp, sk, 0);

	std::cerr << "Verifying test_query" << std::endl;
	if (!cloud::verifySignature(test_query, true, validity, timestamp, signature1, std::get<0>(keys))
			|| !cloud::verifySignature(test_query, true, validity, timestamp, signature1, pk)
			|| !cloud::verifySignature(test_query, true, validity, timestamp, signature2, std::get<0>(keys))
			|| !cloud::verifySignature(test_query, true, validity, timestamp, signature2, pk)) {
		std::cout << "Problem z odczytem zapisanych kluczy" << std::endl;
		return 2;
	}

	std::cout << "Keys generated succesfully" << std::endl;
	return 0;
}

