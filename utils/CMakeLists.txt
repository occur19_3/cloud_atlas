cmake_minimum_required(VERSION 2.6)

project(cloud_utils)

INCLUDE_DIRECTORIES(
	${PROJECT_SOURCE_DIR}
	${PROJECT_SOURCE_DIR}/../headers
)

set(utils_h
)

set(utils_cpp
	${PROJECT_SOURCE_DIR}/log_utils.cpp
)


add_library(cloud_utils_lib
	${utils_h}
	${utils_cpp}
)
