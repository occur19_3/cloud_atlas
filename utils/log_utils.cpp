#include "log_utils.h"

#include <chrono>
#include <cstring>

#ifdef CLOUD_COMPILE_VARIANT
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>
#endif

namespace cloud {

const std::string kDefaultCloudHost = "127.0.0.1";

bool shouldPrintLog(LogLevel ll) {
	return static_cast<int>(ll) >= static_cast<int>(MIN_LOG_LEVEL);
}
std::string stripFileName(const std::string& filename)
{
	for (int i = filename.size(); i != -1; --i) {
		if (filename[i] == '/') {
			return filename.substr(i + 1);
		}
	}
	return filename;
}

std::string log_time()
{
	struct tm * timeinfo;
	time_t t = getCurrentTime() / 1000;
	char buffer[30];
	timeinfo = localtime(&t);
	size_t s = strftime(buffer, 30, "%H:%M:%S", timeinfo);
	std::string result(buffer, buffer + s);
	return result;
}

std::int64_t getCurrentTime()
{
	std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds> (
		std::chrono::system_clock::now().time_since_epoch()
	);
	return ms.count();
}

std::int64_t getTimeFromBuffer(char *buffer)
{
	std::uint64_t v;
	::memcpy(reinterpret_cast<void*>(&v), buffer, sizeof(std::uint64_t));
	return v;
}

void writeTimeToBuffer(std::uint64_t  t, char *buffer)
{
	::memcpy(buffer, reinterpret_cast<void*>(&t), sizeof(std::uint64_t));
}

std::int64_t getRandomId()
{
	std::int64_t res = rand();
	res <<= 32;
	res |= rand();
	return res;
}


zone_path parseZoneName(const std::string &zone) {
	if (zone == "/") {
		return zone_path{""};
	}
	zone_path zone_names;
	boost::split(zone_names, zone, boost::is_any_of("/"));
	LOG("parseZoneName splitted" LG(zone, zone_names));
	if (zone_names.size() < 2) {
		LOG("parseZoneName splitted za malo nazw" LG(zone, zone_names));
		return zone_path();
	}
	if (zone_names[0].size() != 0) {
		LOG("parseZoneName root musi byc pusty" LG(zone, zone_names));
		return zone_path();
	}
	for (size_t i = 1; i < zone_names.size(); ++i) {
		if (!boost::regex_match(zone_names[i], boost::regex("[0-9a-zA-Z_]+"))) {
			LOG("parseZoneName niepoprawna nazwa" LG(zone_names[i], zone));
			return zone_path();
		}
	}
	return zone_names;
}

std::string printZoneName(const zone_path &zone_name)
{
	if (zone_name.size() < 2) {
		return "/";
	}
	std::string str;
	for (auto iter = ++zone_name.begin(); iter != zone_name.end(); ++ iter) {
		str += std::string("/") + *iter;
	}
	return str;
}

size_t get_eq_level(const zone_path &p1, const zone_path &p2)
{
	size_t i = 0;
	for (; i < p1.size() && i < p2.size() && p1[i] == p2[i]; ++i);
	return i;
}

} /* namespace cloud */

#define CLOUD_OSTREAM_OPERATOR_DEFINITION \
std::ostream& operator << (std::ostream& os, ::cloud::CLOUD_ENUM_CLASS v) \
{ \
	switch (v) { \
		CLOUD_ENUM_LIST \
	default: \
		os << ""; \
		break; \
	} \
	return os; \
}

#define CLOUD_ENUM(X) \
	case ::cloud::X: \
		os << #X; \
		break;

#define CLOUD_ENUM_CLASS LogLevel
#define CLOUD_ENUM_LIST LOG_LEVEL_ENUM_LIST
CLOUD_OSTREAM_OPERATOR_DEFINITION
#undef CLOUD_ENUM_CLASS
#undef CLOUD_ENUM_LIST

#define CLOUD_ENUM_CLASS LogCategory
#define CLOUD_ENUM_LIST LOG_CATEGORY_ENUM_LIST
CLOUD_OSTREAM_OPERATOR_DEFINITION
#undef CLOUD_ENUM_CLASS
#undef CLOUD_ENUM_LIST

#undef CLOUD_ENUM

