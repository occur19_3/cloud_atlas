#include "crypto.h"

#include <iostream>

#include "log_utils.h"
#include "pssr.h"
#include "rng.h"

#undef  LOG_DEFAULT_CATEGORY
#define LOG_DEFAULT_CATEGORY CRYPTO

namespace cloud {

std::tuple<PublicKey, PrivateKey> GenerateKeyPair(std::uint32_t seed)
{
	LOG(TRACE, "GenerateKeyPair" LG(seed));
	CryptoPP::LC_RNG rng(seed);
	PrivateKey sk;
	sk.GenerateRandomWithKeySize(rng, RSA_KEY_SIZE);
	return std::make_tuple(PublicKey(sk), sk);
}

bool verifySignature(const std::string &query, bool install, time validity, time signer_timestamp, const Signature &signature, const PublicKey &pk)
{
	// Verifier object
	LOG(TRACE, "verifySignature" LG(query, install, validity, signer_timestamp));
	CryptoPP::RSASS<CryptoPP::PSS, CryptoPP::SHA1>::Verifier verifier(pk);

	//data
	size_t whole_signature_size = kSignatureQueryOffset + query.size();
	byte* data = new byte[whole_signature_size];
	::memcpy(data + kSignatureInstallOffset, &install, sizeof(bool));
	::memcpy(data + kSignatureValidityOffset, &validity.v, sizeof(std::int64_t));
	::memcpy(data + kSignatureSignerTimestampOffset, &signer_timestamp.v, sizeof(std::int64_t));
	::memcpy(data + kSignatureQueryOffset, query.c_str(), query.size());

	// Verify
	bool result = verifier.VerifyMessage(data, whole_signature_size, signature, signature.size());
	LOG("Signature on message" LG(query, result));

	delete[] data;
	return result;
}

Signature signQuery(const std::string &query, bool install, time validity, time signer_timestamp, const PrivateKey &sk, std::uint32_t seed)
{
	LOG(TRACE, "signQuery" LG(query, install, validity, signer_timestamp, seed));
	CryptoPP::LC_RNG rng(seed);
	CryptoPP::RSASS<CryptoPP::PSS, CryptoPP::SHA1>::Signer signer(sk);

	// Create signature space
	size_t length = signer.MaxSignatureLength();
	Signature signature(length);

	// cpy data
	size_t whole_signature_size = kSignatureQueryOffset + query.size();
	byte* data = new byte[whole_signature_size];
	::memcpy(data + kSignatureInstallOffset, &install, sizeof(bool));
	::memcpy(data + kSignatureValidityOffset, &validity.v, sizeof(std::int64_t));
	::memcpy(data + kSignatureSignerTimestampOffset, &signer_timestamp.v, sizeof(std::int64_t));
	::memcpy(data + kSignatureQueryOffset, query.c_str(), query.size());

	// Sign message
	length = signer.SignMessage(rng, data, whole_signature_size, signature);

	// Resize now we know the true size of the signature
	signature.resize(length);

	delete[] data;
	return signature;
}

} /* namespace cloud */
