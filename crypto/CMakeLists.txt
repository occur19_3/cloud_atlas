cmake_minimum_required(VERSION 2.6)

project(cloud_crypto)

INCLUDE_DIRECTORIES(
	${PROJECT_SOURCE_DIR}
	${PROJECT_SOURCE_DIR}/../libs/cryptopp
	${PROJECT_SOURCE_DIR}/../headers
)

set(crypto_h
)

set(crypto_cpp
	${PROJECT_SOURCE_DIR}/crypto.cpp
)


add_library(cloud_crypto_lib
	${crypto_h}
	${crypto_cpp}
)


