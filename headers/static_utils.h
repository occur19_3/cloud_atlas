#pragma once

#include <type_traits>

#include <boost/utility/enable_if.hpp>

namespace cloud {

template<typename T, typename V, typename R = T>
using enable_if_same = typename boost::enable_if<std::is_same<T, V>, R>::type;

template<typename T, typename V, typename R = T>
using disable_if_same = typename boost::disable_if<std::is_same<T, V>, R>::type;

}
