cmake_minimum_required(VERSION 2.6)

project(cloud_headers)

INCLUDE_DIRECTORIES(
	${PROJECT_SOURCE_DIR}
	${PROJECT_SOURCE_DIR}/../libs/cryptopp
)


set(cloud_headers_h
	${PROJECT_SOURCE_DIR}/attribute.h
	${PROJECT_SOURCE_DIR}/crypto.h
	${PROJECT_SOURCE_DIR}/log_utils.h
	${PROJECT_SOURCE_DIR}/network.h
	${PROJECT_SOURCE_DIR}/nullable_type.h
	${PROJECT_SOURCE_DIR}/packets.h
	${PROJECT_SOURCE_DIR}/serialization.h
	${PROJECT_SOURCE_DIR}/static_utils.h
	${PROJECT_SOURCE_DIR}/utils.h
	${PROJECT_SOURCE_DIR}/value_types.h
	${PROJECT_SOURCE_DIR}/value.h
)

set(cloud_headers_cpp
	${PROJECT_SOURCE_DIR}/dummy_headers_source.cpp
)

#bo qtcreator nie umie sobie radzic z miejscami gdzie nie ma nic
add_library(cloud_dummy_header_library
	${cloud_headers_h}
	${cloud_headers_cpp}
)
