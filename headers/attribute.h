#pragma once

#include "value.h"

namespace cloud {

class Attribute {
public:
	std::string name_;
	value_type type_; //!< pewne rzeczy sie nie zmieniaja
	nullable_value value_;	//!<dodatkowo oznaczamy, czy ma odpowiednia wartosc

	Attribute(const std::string name, const value_type &type, const nullable_value &value):
		name_(name), type_(type), value_(value)
	{
		check_type_compatibility_throw(value_.val, type_);
	}

	template<typename T>
	Attribute(const std::string name, const value_type &type, const T &value):
		name_(name), type_(type), value_(value)
	{
		check_type_compatibility_throw(value_.val, type_);
	}
};

} /* namespace cloud */

std::ostream & operator << (std::ostream & os, const cloud::Attribute &v);
