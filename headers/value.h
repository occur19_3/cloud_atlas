#pragma once

#include <list>
#include <ostream>
#include <set>

#ifdef CLOUD_COMPILE_VARIANT
#include <boost/variant/apply_visitor.hpp>
#include <boost/variant/recursive_variant.hpp>
#include <boost/variant/static_visitor.hpp>
#include <boost/variant/variant.hpp>
#endif

#include "log_utils.h"
#include "nullable_type.h"
#include "static_utils.h"
#include "value_types.h"

namespace cloud {

using value = typename boost::make_recursive_variant<
	bool,
	contact,
	double,
	duration,
	integer,
	string,
	time,
	list<boost::recursive_variant_>,
	set<boost::recursive_variant_>
>::type;

using nullable_value = nullable_type<value>;

/* * * * * * * * * * get_value visitor * * * * * * * * * */

template<typename T>
struct get_value: public boost::static_visitor<T>
{
	template<typename V>
	enable_if_same<V, T, T>
	operator () (const V &v) const {
		return v;
	}
	template<typename V>
	disable_if_same<V, T, T>
	operator () (const V &v) const {
		LOG(WARN, "get_value expected T value" LG(v));
		throw std::runtime_error("get_value expected T value");
		return T();
	}
};

template<typename V>
V get_value_wrapper (const value &v) {
	return boost::apply_visitor(get_value<V>(), v);
}

std::string value_to_string(const value &v);

std::string value_type_to_string(const value &v);

bool check_type_compatibility(const value &v, const value_type &t);

void check_type_compatibility_throw(const value &v, const value_type &t);

value get_empty_value(const value_type &t);

value simple_value_from_string(SimpleType st, const std::string &str);
value value_from_string(const value_type &t, const std::string &str);

} /* namespace cloud */

std::ostream & operator <<(std::ostream & os, const cloud::value &v);
