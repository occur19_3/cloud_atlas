#pragma once

#include <list>

#include <boost/asio/ip/address_v4.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/asio/ip/tcp.hpp>

#include "packets.h"
#include "utils.h"

namespace cloud {

class NetworkManager {
public:
	struct RcvData {
		contact c;
		std::int64_t their_snd_timestamp;
		std::int64_t my_rcv_timestamp;
		PacketType type;
		PacketData data;
	};

	using SendData = std::tuple<contact, PacketData>;

	///note, funkcja nie przejmuje wladzy nad danymi pakietu
	NetworkManager(boost::asio::io_service &io_serv, std::function<void(RcvData &&)> handle_packet);
	~NetworkManager();

	bool start(std::uint16_t port);	//!<kazdy rozpoczyna nasluchiwanie na udp, ulatwi to znaczaca odpowiadanie i zycie:D
	void stop();

	/// przejmu wladze nad danymi pakietu
	void push_to_udp_send_queue(contact c, PacketData &&p);

private:
	static constexpr size_t kUdpBufferSize = 65535;
	bool sending_ = false;

	//snd timestamp, rcv_timestemp packet, data
	std::function<void(RcvData &&)> handle_packet_;
	std::uint16_t port_;
	boost::asio::io_service &io_serv_;   //mby shared_ptr

	/// udp

	boost::asio::ip::udp::socket udp_socket_;
	boost::asio::ip::udp::endpoint rcv_endpoint_, snd_endpoint_;	

	size_t udp_snd_size_ = 0;
	std::list<SendData> udp_snd_queue_;
	char udp_rcv_buffer_[kUdpBufferSize], *udp_snd_buffer_ = nullptr;

	void start_receive();
	void handle_receive(const boost::system::error_code& ec, std::size_t size);

	void send_udp(SendData &&send_data);
	void handle_send(const boost::system::error_code& ec, std::size_t size);
	void close_socket(boost::asio::ip::udp::socket &socket);
};

} /* namespace cloud */

std::ostream & operator << (std::ostream &os, const cloud::NetworkManager::RcvData &r);
