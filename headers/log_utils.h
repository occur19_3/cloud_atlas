#pragma once

#include <list>
#include <map>
#include <iomanip>
#include <iostream>
#include <set>
#include <vector>


// makro do zliczania ilosci argumentow makr
#define COUNT_PARMS2(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _, ...) _
#define COUNT_PARMS(...)\
  COUNT_PARMS2(__VA_ARGS__, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1)

// makro do zlaczania tekstu
#define CAT(A, B) CAT2(A, B)
#define CAT2(A, B) A ## B


#undef  LOG_DEFAULT_CATEGORY
#define LOG_DEFAULT_CATEGORY NORMAL

// wygodny skrot do wyswietlania zmiennych wewnatrz LOGow
// M wykorzystuje szablon do automatycznego wykorzystania getInfo()
#define LGS1(var) " "<<#var<<":"<<var<<", "
#define LGS2(var1,var2) " "<<#var1<<":"<<var1<<", "<<#var2<<":"<<var2<<", "
#define LGS3(var1,var2,var3) " "<<#var1<<":"<<var1<<", "<<#var2<<":"<<var2<<", "<<#var3<<":"<<var3<<", "
#define LGS4(var1,var2,var3,var4) " "<<#var1<<":"<<var1<<", "<<#var2<<":"<<var2<<", "<<#var3<<":"<<var3<<", "<<#var4<<":"<<var4<<", "
#define LGS5(var1,var2,var3,var4,var5) " "<<#var1<<":"<<var1<<", "<<#var2<<":"<<var2<<", "<<#var3<<":"<<var3<<", "<<#var4<<":"<<var4<<", "<<#var5<<":"<<var5<<", "
#define LGS6(var1,var2,var3,var4,var5,var6) " "<<#var1<<":"<<var1<<", "<<#var2<<":"<<var2<<", "<<#var3<<":"<<var3<<", "<<#var4<<":"<<var4<<", "<<#var5<<":"<<var5<<", "<<#var6<<":"<<var6<<", "
#define LGS7(var1,var2,var3,var4,var5,var6,var7) " "<<#var1<<":"<<var1<<", "<<#var2<<":"<<var2<<", "<<#var3<<":"<<var3<<", "<<#var4<<":"<<var4<<", "<<#var5<<":"<<var5<<", "<<#var6<<":"<<var6<<", "<<#var7<<":"<<var7<<", "
#define LGS8(var1,var2,var3,var4,var5,var6,var7,var8) " "<<#var1<<":"<<var1<<", "<<#var2<<":"<<var2<<", "<<#var3<<":"<<var3<<", "<<#var4<<":"<<var4<<", "<<#var5<<":"<<var5<<", "<<#var6<<":"<<var6<<", "<<#var7<<":"<<var7<<", "<<#var8<<":"<<var8<<", "
#define LGS9(var1,var2,var3,var4,var5,var6,var7,var8,var9) " "<<#var1<<":"<<var1<<", "<<#var2<<":"<<var2<<", "<<#var3<<":"<<var3<<", "<<#var4<<":"<<var4<<", "<<#var5<<":"<<var5<<", "<<#var6<<":"<<var6<<", "<<#var7<<":"<<var7<<", "<<#var8<<":"<<var8<<", "<<#var9<<":"<<var9<<", "

#define LGS(...)   CAT(LGS, COUNT_PARMS(__VA_ARGS__))(__VA_ARGS__)
#define LG(...) << LGS(__VA_ARGS__)

#define PRINT_LOG(category, level, msg, funcname, filename, fileline) \
	do{ \
	if(::cloud::shouldPrintLog(::cloud::level)){ \
		std::cerr << ::cloud::log_time() << " [" << std::setw(5)<< ::cloud::level <<  "|" << std::setw(6) << ::cloud::category << "]" \
			<< " {" << std::setw(16) << ::cloud::stripFileName(filename) << ":" << std::setw(3) << fileline << "}" \
			<< "     " << msg << " {FUN=" << funcname << "}" << std::endl; \
	} } while(0)


#define LOG3(category, level, msg) PRINT_LOG(category, level, msg, __func__, __FILE__, __LINE__)
#define LOG2(detail, msg) LOG3(LOG_DEFAULT_CATEGORY, detail, msg)
#define LOG1(msg) LOG2(DEBUG, msg)

//#define PRODUCTION

#ifdef PRODUCTION
#define LOG(...)
#define CLOUD_ASSERT(...)
#else

#define LOG(...)   CAT(LOG, COUNT_PARMS(__VA_ARGS__))(__VA_ARGS__)
#define CLOUD_ASSERT(x, ...) \
	assert(x); \
	LOG(__VA_ARGS__)

#endif

namespace cloud {

/* * * * * default setup data * * * * */

extern const std::string kDefaultCloudHost;	//!loopback
constexpr std::uint16_t kDefaultCloudAgentPort = 9690;
constexpr std::uint16_t kDefaultCloudQuerySignerPort = 31337;

using zone_path = std::vector<std::string>;

#define LOG_LEVEL_ENUM_LIST \
	CLOUD_ENUM(TRACE) \
	CLOUD_ENUM(DEBUG) \
	CLOUD_ENUM(WARN) \
	CLOUD_ENUM(ERROR)

#define LOG_CATEGORY_ENUM_LIST \
	CLOUD_ENUM(AGENT) \
	CLOUD_ENUM(CLIENT) \
	CLOUD_ENUM(CRYPTO) \
	CLOUD_ENUM(FETCH) \
	CLOUD_ENUM(NET) \
	CLOUD_ENUM(NORMAL) \
	CLOUD_ENUM(PARSER) \
	CLOUD_ENUM(SIGNER)

#define CLOUD_ENUM(X) X,

enum LogLevel {
	LOG_LEVEL_ENUM_LIST
	LastLogLevel
};


enum LogCategory {
	LOG_CATEGORY_ENUM_LIST
	LastLogCategory
};

#undef CLOUD_ENUM

constexpr LogLevel MIN_LOG_LEVEL = TRACE;
bool shouldPrintLog(LogLevel ll);
std::string stripFileName(const std::string& filename);
std::string log_time();

} /* namespace cloud */

std::ostream& operator << (std::ostream& os, ::cloud::LogLevel ll);
std::ostream& operator << (std::ostream& os, ::cloud::LogCategory lc);

namespace cloud {

template<typename F, int i, typename... Args>
typename std::enable_if< std::tuple_size< std::tuple<Args...> >::value == i , void>::type
tuple_for_each_impl(const F &, const std::tuple<Args...> &)
{}

template<typename F, int i, typename... Args>
typename std::enable_if<std::tuple_size<std::tuple<Args...>>::value >= i + 1, void>::type
tuple_for_each_impl(const F &f, const std::tuple<Args...> &args)
{
	f(std::get<i>(args));
	tuple_for_each_impl<F, i + 1, Args...>(f, args);
}

template<typename F, typename... Args>
void tuple_for_each(const F &f, const std::tuple<Args...> &args) {
	tuple_for_each_impl<F, 0, Args...>(f, args);
}

struct tuple_write_functor {
	std::ostream &os;
	tuple_write_functor(std::ostream &os): os(os) {}

	template<typename T>
	void operator() (const T &t) const {
		os<< t << ", ";
	}
};

template<typename Archive>
struct tuple_serialize_functor {
	Archive &ar;
	tuple_serialize_functor(Archive &ar): ar(ar) {}
	template<typename T>
	void operator() (const T& t) const {
		ar & t;
	}
};

template<typename T>
inline std::ostream & print_collection(std::ostream &os, const std::string &start, const std::string &end, const T &collection)
{
	os << start;
	if (!collection.empty()) {
		typename T::const_iterator iter = collection.cbegin();
		os << *iter;
		for (++iter; iter != collection.cend(); ++iter) {
			os << ", " << *iter;
		}
	}
	os << end;
	return os;
}

std::int64_t getCurrentTime();
std::int64_t getTimeFromBuffer(char *buffer);
void writeTimeToBuffer(std::uint64_t  t, char *buffer);

std::int64_t getRandomId();

}

template<typename... T>
inline std::ostream & operator << (std::ostream & os, const std::tuple<T...> &v)
{
	cloud::tuple_write_functor f(os);
	tuple_for_each(f, v);
	return os;
}

template<typename T1, typename T2>
inline std::ostream & operator << (std::ostream & os, const std::pair<T1, T2> &v)
{
	os << "[" << v.first << "->" << v.second << "]";
	return os;
}

template<typename... T>
inline std::ostream & operator << (std::ostream & os, const std::list<T...> &v)
{
	return cloud::print_collection(os, "[", "]", v);
}

template<typename... T>
inline std::ostream & operator << (std::ostream & os, const std::map<T...> &v)
{
	return cloud::print_collection(os, "{", "}", v);
}

template<typename... T>
inline std::ostream & operator << (std::ostream & os, const std::set<T...> &v)
{
	return cloud::print_collection(os, "{", "}", v);
}

template<typename... T>
inline std::ostream & operator << (std::ostream & os, const std::vector<T...> &v)
{
	return cloud::print_collection(os, "<", ">", v);
}

namespace boost {
namespace serialization {


template<typename Archive, typename ... Elements>
Archive& serialize(Archive& ar, std::tuple<Elements...>& t, const unsigned int version) {
	cloud::tuple_serialize_functor<Archive> f(ar);
	cloud::tuple_for_each(f, t);
	return ar;
}

} // serialization
} // boost
