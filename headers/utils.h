#pragma once

#include <set>

#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_service.hpp>

namespace cloud {

using zone_path = std::vector<std::string>;

struct RepeatTimer {
	RepeatTimer(boost::asio::io_service &io_serv, std::function<void(boost::system::error_code)> callback):
		timer_(io_serv, boost::posix_time::seconds(0)), callback_(callback)
	{}
	~RepeatTimer() {
		stop();
	}


	void repeat() {
		timer_.expires_from_now(timer_.expires_from_now() + boost::posix_time::milliseconds(rerun_time_ms_));
		timer_.async_wait(callback_);
	}

	void stop()
	{
		timer_.cancel();
	}

	boost::asio::deadline_timer timer_;
	std::int64_t rerun_time_ms_ = 0;
	std::function<void(boost::system::error_code)> callback_;
};

//implementacja w log_utils.cpp
zone_path parseZoneName(const std::string &zone_name);
std::string printZoneName(const zone_path &zone_name);
size_t get_eq_level(const zone_path &p1, const zone_path &p2);


template<typename T, typename... Args>
T getRandomFromSet(const std::set<T, Args...> &s) {
	if (s.empty()) {
		return T();
	}
	size_t val = rand() % s.size();
	auto iter = s.begin();
	for (; val > 0; --val) ++iter;
	return *iter;
}

template<typename Map, typename Key, typename Value>
inline void insertIntoMap(Map &m, const Key &k, Value v)
{
	auto iter = m.find(k);
	if (iter == m.end()) {
		m.insert(std::make_pair(k, v));
	} else {
		iter->second = v;
	}
}


}
