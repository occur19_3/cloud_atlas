#pragma once

#ifdef CLOUD_COMPILE_VARIANT
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/iostreams/stream.hpp>

#include <boost/serialization/assume_abstract.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/vector.hpp>
#endif


#include "attribute.h"
#include "secblock.h"
#include "value_types.h"

namespace cloud {

namespace serialization {

enum class serialization_value_type {
	Bool,
	Contact,
	Double,
	Duration,
	Integer,
	String,
	Time,
	List,
	Set
};

struct Value {
	virtual ~Value() {}
	virtual serialization_value_type get_value_type() = 0;
	virtual void write_to_ostream(std::ostream &os) const = 0;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int file_version){}
};

#define GENERATE_CLOUD_SERIALIZATION_VALUE(Name, UnderlyingType) \
struct CAT(Value, Name) : public Value { \
	CAT(Value, Name) () = default; \
	CAT(Value, Name) (UnderlyingType value): value_(value) {} \
	~CAT(Value, Name) ();\
	virtual serialization_value_type get_value_type() override final \
	{	\
		return serialization_value_type::Name; \
	} \
	virtual void write_to_ostream(std::ostream &os) const override final \
	{	\
		os << value_; \
	} \
	UnderlyingType value_; \
	template<class Archive> \
	void serialize(Archive & ar, const unsigned int version) \
	{ \
		boost::serialization::base_object<Value>(*this);	\
		boost::serialization::void_cast_register<Value, CAT(Value, Name) >();	\
		ar & value_; \
	} \
}

GENERATE_CLOUD_SERIALIZATION_VALUE(Bool, bool);
GENERATE_CLOUD_SERIALIZATION_VALUE(Contact, contact);
GENERATE_CLOUD_SERIALIZATION_VALUE(Double, double);
GENERATE_CLOUD_SERIALIZATION_VALUE(Duration, duration);
GENERATE_CLOUD_SERIALIZATION_VALUE(Integer, integer);
GENERATE_CLOUD_SERIALIZATION_VALUE(String, string);
GENERATE_CLOUD_SERIALIZATION_VALUE(Time, time);
GENERATE_CLOUD_SERIALIZATION_VALUE(List, std::list<Value*>);
GENERATE_CLOUD_SERIALIZATION_VALUE(Set, std::list<Value*>);		//!<i tak potem sprawdzimy, czy sa rozne:)

struct Attribute {
	std::string name_;
	value_type type_ = bool_vt;
	Value* value_ = nullptr;

	Attribute() = default;
	Attribute(const std::string &name, const value_type &type, Value* value): name_(name), type_(type), value_(value) {}

	void clear() {
		delete value_;
		value_ = nullptr;
	}


	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & name_;
		ar & type_;
		ar & value_;
	}
};

struct signature_wrapper {
	std::vector<unsigned char> value_;
	signature_wrapper() = default;
	signature_wrapper(const std::vector<unsigned char> &value): value_(value) {}
	signature_wrapper(const CryptoPP::SecByteBlock &sec): value_(sec.begin(), sec.end()) {}

	CryptoPP::SecByteBlock toSignature() const {
		CryptoPP::SecByteBlock sig(value_.size());
		std::copy(value_.begin(), value_.end(), sig.begin());
		return sig;
	}

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & value_;
	}
};

struct query_wrapper {
	query_wrapper() = default;
	query_wrapper(const std::string &query, bool install, time validity, time signer_timestamp, const signature_wrapper &signature):
		query_(query), install_(install), validity_(validity), signer_timestamp_(signer_timestamp), signature_(signature)
	{}

	std::string query_;
	bool install_;
	time validity_;
	time signer_timestamp_;
	signature_wrapper signature_;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & query_;
		ar & install_;
		ar & validity_;
		ar & signer_timestamp_;
		ar & signature_;
	}

};

} /* namespace serialization */

//konwersje z sieci rzucaja wyjatki, bo moga byc tam jakies krzaki i wgl, prawidlowa obsuga powinna byc w try_catchu
Attribute convertToCloudAttribute(const serialization::Attribute &attribute);
serialization::Attribute convertToSerializableAttribute(const Attribute &attribute);

nullable_value convertToCloudValue(const value_type &vt, serialization::Value* value);
serialization::Value* convertToSerializableValue(const nullable_value &value);

} /* namespace cloud */



std::ostream & operator<< (std::ostream &os, const cloud::serialization::serialization_value_type &v);
std::ostream & operator<< (std::ostream &os, const cloud::serialization::Value* v);
std::ostream & operator<< (std::ostream &os, const cloud::serialization::Value &v);
std::ostream & operator<< (std::ostream &os, const cloud::serialization::Attribute &v);
std::ostream & operator<< (std::ostream &os, const cloud::serialization::query_wrapper &v);
