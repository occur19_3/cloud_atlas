#pragma once

#include <tuple>

#include "files.h"
#include "rsa.h"
#include "value_types.h"

namespace cloud {

constexpr std::uint16_t RSA_KEY_SIZE = 1024;
constexpr size_t kSignatureInstallOffset = 0;
constexpr size_t kSignatureValidityOffset = sizeof(bool);
constexpr size_t kSignatureSignerTimestampOffset = sizeof(bool) + sizeof(std::int64_t);
constexpr size_t kSignatureQueryOffset = sizeof(bool) + sizeof(std::int64_t) + sizeof(std::int64_t);

using PublicKey = CryptoPP::RSA::PublicKey;
using PrivateKey = CryptoPP::RSA::PrivateKey;
using Signature = CryptoPP::SecByteBlock;

std::tuple<PublicKey, PrivateKey> GenerateKeyPair(std::uint32_t seed);

bool verifySignature(const std::string &query, bool install, time validity, time signer_timestamp, const Signature &signature, const PublicKey &pk);
Signature signQuery(const std::string &query, bool install, time validity, time signer_timestamp, const PrivateKey &sk, std::uint32_t seed);


template<typename T>
void write_to_file(const std::string &filename, const T &key)
{
	CryptoPP::ByteQueue queue;
	key.Save(queue);
	CryptoPP::FileSink file(filename.c_str());
	queue.CopyTo(file);
	file.MessageEnd();
}


template<typename T>
bool read_from_file(const std::string &filename, T &key)
{
	try {
		CryptoPP::ByteQueue queue;
		CryptoPP::FileSource file(filename.c_str(), true);
		file.TransferTo(queue);
		queue.MessageEnd();
		key.Load(queue);
		return true;
	} catch (std::exception &e) {
		std::cerr << "Nie udalo sie odczytac klucza" << std::endl;
		return false;
	}
}

} /* namespace cloud */
