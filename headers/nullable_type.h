#pragma once

#include <ostream>

namespace cloud {

template<typename T>
struct nullable_type {
	bool is;
	T val;

	bool is_null() const { return !is; }
	nullable_type(): is(false) {}
	nullable_type(T val): is(true), val(val) {}
	nullable_type(bool b, T val): is(b), val(val) {}

	bool operator< (const nullable_type &v2) const {
		if (!is) {
			if (!v2.is) {
				return val < v2.val;
			}
			return true;
		}
		if (!v2.is) {
			return false;
		}
		return val < v2.val;
	}
};

} /* namespace cloud */

template<typename T>
inline std::ostream & operator << (std::ostream & os, const cloud::nullable_type<T> &v)
{
	if (v.is) {
		os << v.val;
	} else {
		os << "null";
	}
	return os;
}
