#pragma once

#include <list>
#include <map>
#include <set>
#include <vector>
#include <tuple>

#include <boost/asio/ip/address_v4.hpp>

#include "log_utils.h"

namespace cloud {

enum class SimpleType: std::uint8_t {
	BOOL,
	CONTACT,
	DOUBLE,
	DURATION,
	INTEGER,
	STRING,
	TIME
};

enum class CollectionType: std::uint8_t {
	LIST,
	SET
};

template<typename T>
struct cloudFromStringMapWrapper {
	static const std::map<std::string, T> map;
};

template<typename T>
T cloudFromString(const std::string &str)
{
	const auto & vals = cloudFromStringMapWrapper<T>::map;
	auto iter = vals.find(str);
	if (iter == vals.end()) {
		LOG(WARN, "could not read from string" LG(str, typeid(T).name()));
		throw std::runtime_error("could not read from string");
	}
	return iter->second;
}

struct value_type {
	SimpleType type_;
	std::list<CollectionType> collection_;
	value_type(SimpleType type): type_(type) {}
	value_type(SimpleType type, std::list<CollectionType> collection) :type_(type), collection_(collection) {}

	bool isSimpleType() const {
		return collection_.empty();
	}
	bool isSimpleType(SimpleType type) const {
		return collection_.empty() && type == type_;
	}

	value_type strip_collection_layer() const;
	bool operator == (const value_type &t) const {
		if (type_ != t.type_ || collection_.size() != t.collection_.size()) {
			return false;
		}
		auto iter = collection_.begin();
		auto t_iter = t.collection_.begin();
		for (; iter != collection_.end(); ++iter, ++t_iter) {
			if (*iter != *t_iter) {
				return false;
			}
		}
		return true;
	}

	bool operator != (const value_type &t) const {
		return !(*this == t);
	}

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & type_;
		ar & collection_;
	}

	bool fromString(const std::string &str);
};

extern const value_type bool_vt;
extern const value_type contact_vt;
extern const value_type double_vt;
extern const value_type duration_vt;
extern const value_type integer_vt;
extern const value_type string_vt;
extern const value_type time_vt;
extern const value_type set_contact_vt;
extern const value_type set_string_vt;
extern const value_type list_string_vt;

//contact

using ip = std::uint32_t;
using port = std::uint16_t;

extern const ip localhost;

struct contact {
	std::uint32_t ip;
	std::uint16_t port;
	contact(): ip(0), port(0) {}
	contact(std::uint32_t ip, std::uint16_t port): ip(ip), port(port) {}

	bool operator< (const contact &v2) const {
		if (ip < v2.ip) {
			return true;
		}
		if (ip == v2.ip) {
			return port < v2.port;
		}
		return false;
	}

	template<typename Archive>
	void serialize(Archive& ar, const unsigned int version) {
		ar & ip;
		ar & port;
	}

	bool fromString(const std::string &contact_s);
	bool fromString(const std::string &ip_s, std::uint16_t p);
	static std::set<contact> setFromString(const std::string &contacts_string);
	bool isSet() const;
};

std::string contact_to_string(const contact &c);
contact contact_from_string_throw(const std::string &str);

// duration

struct duration {
	static duration from_string_throw(const std::string &str);

	duration() = default;
	duration(std::int64_t v): v(v) {}

	std::int64_t v;

	duration operator- () const {
		return duration(-v);
	}

	bool operator< (const duration &v2) const {
		return v < v2.v;
	}
	bool operator<= (const duration &v2) const {
		return v <= v2.v;
	}
	bool operator> (const duration &v2) const {
		return v > v2.v;
	}
	bool operator>= (const duration &v2) const {
		return v >= v2.v;
	}
	bool operator== (const duration &v2) const {
		return v == v2.v;
	}
	bool operator!= (const duration &v2) const {
		return v != v2.v;
	}

	std::string to_string() const;
	bool from_string(const std::string &str);

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & v;
	}
};		//!< czas w milisekundach

inline duration operator+ (const duration &v1, const duration &v2) {
	return duration(v1.v + v2.v);
}
inline duration operator- (const duration &v1, const duration &v2) {
	return duration(v1.v - v2.v);
}
inline duration operator* (const duration &v1, const std::int64_t &v2) {
	return duration(v1.v * v2);
}
inline duration operator/ (const duration &v1, const std::int64_t &v2) {
	return duration(v1.v / v2);
}

// integer

using integer = ::std::int64_t;

// string

using string = ::std::string;

// time

struct time {
	static time now();
	static time epoch();
	static time from_string_throw(const std::string &str);

	std::int64_t v;

	time() = default;
	time(std::int64_t v): v(v) {}
	bool operator< (const time &v2) const {
		return v < v2.v;
	}
	bool operator<= (const time &v2) const {
		return v <= v2.v;
	}
	bool operator> (const time &v2) const {
		return v > v2.v;
	}
	bool operator>= (const time &v2) const {
		return v >= v2.v;
	}
	bool operator== (const time &v2) const {
		return v == v2.v;
	}
	bool operator!= (const time &v2) const {
		return v != v2.v;
	}

	std::string to_string() const;
	bool from_string(const std::string &str);

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & v;
	}
};

inline time operator+ (const time &v1, const duration &v2) {
	time t1 = v1;
	t1.v += v2.v;
	return t1;
}
inline time operator- (const time &v1, const duration &v2) {
	time t1 = v1;
	t1.v -= v2.v;
	return t1;
}
inline duration operator- (const time &v1, const time &v2) {
	return duration(v1.v - v2.v);
}

// list

template<typename T>
using list = std::list<T>;

template<typename T>
using set = std::set<T>;

using ZmiTimestamp = std::pair<std::string, time>;
/** na kazdym poziomie trzymamy nazwe obecnego poziomu u agenta i liste wszystkich stref na tym poziomie
 * np. dla drzewa u agenta /z1
 *  <root>
 *  /    \
 * z1    z2
 *
 * mamy [
 *		std::pair<"", [ZmiTimestamp(<time>, "")], //level 0
 *		std::pair<"z1", [{z1" -> <last_update z1>}, {"z2" -> <last_update z1> }], //level 1
 * ]
 */
using ZmiFreshness = std::list< std::pair<std::string, std::map<std::string, time>> > ;

zone_path getZonePath(const ZmiFreshness &zmi);
std::set<zone_path> getAllPaths(const zone_path &path);


} /* namespace cloud */

std::ostream & operator <<(std::ostream & os, const cloud::SimpleType &v);
std::ostream & operator <<(std::ostream & os, const cloud::CollectionType &v);
std::ostream & operator <<(std::ostream & os, const cloud::value_type &v);

std::ostream & operator <<(std::ostream & os, const cloud::contact &v);
std::ostream & operator <<(std::ostream & os, const cloud::duration &v);
std::ostream & operator <<(std::ostream & os, const cloud::time &v);

