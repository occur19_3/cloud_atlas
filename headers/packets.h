#pragma once

#include <sstream>

#ifdef CLOUD_COMPILE_VARIANT
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/iostreams/stream.hpp>

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/vector.hpp>
#endif

#include "log_utils.h"
#include "serialization.h"

namespace cloud {

enum class PacketType: char;
static constexpr size_t kMaxPacketBufferSize = 65535;

struct PacketData {
	static constexpr size_t kTypeOffset = sizeof(std::int64_t);
	static constexpr size_t kPacketOffset = sizeof(std::int64_t) + sizeof(PacketType);

	size_t size_;
	char* data_ = nullptr;
	PacketData() = default;
	PacketData(size_t size): size_(size) {
		data_ = new char[size_];
	}
	PacketData(const char *data, size_t size): size_(size) {
		data_ = new char[size_];
		::memcpy(data_, data, size);
	}
};


enum class PacketType: char {
	/// Fetcher <-> Agent
	FetcherData = 0,		//!< Fetcher -> Agent

	/// Client <-> QuerySinger
	QuerySignRequest = 1,		//!< Client -> QuerySinger
	QuerySignResponse = 2,		//!< QuerySinger -> Client

	/// Client <-> Agent
	SetFallbackContactsRequest = 3,
	SetFallbackContactsResponse = 4,

	GetKeptZoneNamesRequest = 5,
	GetKeptZoneNamesResponse = 6,

	GetZoneAttributesRequest = 7,
	GetZoneAttributesResponse = 8,

	SetSingletonZoneAttributeRequest = 9,
	SetSingletonZoneAttributeResponse = 10,

	InstallQueryRequest = 11,
	InstallQueryResponse = 12,

	//!< Gossip
	StartGossip = 13,
	GossipReply = 14,
	GossipZoneAttributesRequest = 15
};

/** Importante: zostawia miejsce na timestamp i uzupelnia typ pakietu  deserialzacja bierze juz tylko same dane pakietu */
template<typename Packet>
PacketData serializePacketSetTypeAndAddTimeStampOffset(const Packet &p)
{
	std::string serial_str;
	boost::iostreams::back_insert_device<std::string> inserter(serial_str);
	boost::iostreams::stream<boost::iostreams::back_insert_device<std::string> > s(inserter);
	boost::archive::binary_oarchive oa(s);
	oa << p;
	s.flush();
	size_t begin_offset = sizeof(std::int64_t) + sizeof(PacketType);
#ifndef PRODUCTION
	if (serial_str.size() > 1) {
		LOG("udalo sie nam cos" LG(serial_str.size()) << " l1: " << static_cast<int>(serial_str[0]) << " " << static_cast<int>(serial_str[1]) << " " << static_cast<int>(serial_str[serial_str.size() - 1]));
	}
#endif
	PacketData pd(serial_str.size() + begin_offset );
	PacketType packet_type = Packet::type;
	::memcpy(pd.data_ + sizeof(std::int64_t), &packet_type, sizeof(PacketType));
	::memcpy(pd.data_ + begin_offset, serial_str.data(), serial_str.size());

	return pd;
}


template<typename Packet>
Packet deserializePacket(PacketData pd)
{
#ifndef PRODUCTION
	if (pd.size_ > 1) {
		LOG("deserializing" LG(pd.size_) << " l1: " << static_cast<int>(pd.data_[0]) << " " << static_cast<int>(pd.data_[1]) << " " <<  static_cast<int>(pd.data_[pd.size_ - 1]));
	}
#endif
	boost::iostreams::basic_array_source<char> sr(pd.data_, pd.size_);
	boost::iostreams::stream< boost::iostreams::basic_array_source<char> > source(sr);
	boost::archive::binary_iarchive ia(source);
	Packet p;
	ia >> p;
	return p;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * packets * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* Packet_FetcherData */

struct Packet_FetcherData {
	static constexpr PacketType type = PacketType::FetcherData;

	Packet_FetcherData() = default;

	Packet_FetcherData(const std::list<serialization::Attribute> &attributes): attributes_(attributes) {}
	std::list<serialization::Attribute> attributes_;

	~Packet_FetcherData() {
		for (auto attr: attributes_) {
			attr.clear();
		}
		attributes_.clear();
	}

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & attributes_;
	}
};

/* Packet_QuerySignRequest */

struct Packet_QuerySignRequest {
	static constexpr PacketType type = PacketType::QuerySignRequest;

	Packet_QuerySignRequest() = default;
	Packet_QuerySignRequest(std::int64_t id, bool install, time validity, const std::string &query):
		id_(id), install_(install), validity_(validity), query_(query) {}

	std::int64_t id_;
	bool install_;
	time validity_;
	std::string query_;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & id_;
		ar & install_;
		ar & validity_;
		ar & query_;
	}
};


/* Packet_QuerySignResponse */

struct Packet_QuerySignResponse {
	static constexpr PacketType type = PacketType::QuerySignResponse;

	Packet_QuerySignResponse() = default;
	Packet_QuerySignResponse(std::int64_t id, time validity, time signer_timestamp, const serialization::signature_wrapper &signature):
		id_(id), validity_(validity), signer_timestamp_(signer_timestamp), signature_(signature) {}

	std::int64_t id_;
	time validity_;
	time signer_timestamp_;
	serialization::signature_wrapper signature_;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & id_;
		ar & validity_;
		ar & signer_timestamp_;
		ar & signature_;
	}
};

/* Packet_SetFallbackContactsRequest */

struct Packet_SetFallbackContactsRequest {
	static constexpr PacketType type = PacketType::SetFallbackContactsRequest;

	Packet_SetFallbackContactsRequest() = default;
	Packet_SetFallbackContactsRequest(std::int64_t id, const std::set<contact> &contacts): id_(id), contacts_(contacts) {}

	std::int64_t id_;
	std::set<contact> contacts_;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & id_;
		ar & contacts_;
	}
};


/* Packet_SetFallbackContactsResponse */

struct Packet_SetFallbackContactsResponse {
	static constexpr PacketType type = PacketType::SetFallbackContactsResponse;

	Packet_SetFallbackContactsResponse() = default;
	Packet_SetFallbackContactsResponse(std::int64_t id, bool result): id_(id), result_(result) {}

	std::int64_t id_;
	bool result_;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & id_;
		ar & result_;
	}
};


/* Packet_GetKeptZoneNamesRequest */

struct Packet_GetKeptZoneNamesRequest {
	static constexpr PacketType type = PacketType::GetKeptZoneNamesRequest;

	Packet_GetKeptZoneNamesRequest() = default;
	Packet_GetKeptZoneNamesRequest(std::int64_t id): id_(id) {}

	std::int64_t id_;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & id_;
	}
};


/* Packet_GetKeptZoneNamesResponse */

struct Packet_GetKeptZoneNamesResponse {
	static constexpr PacketType type = PacketType::GetKeptZoneNamesResponse;

	Packet_GetKeptZoneNamesResponse() = default;
	Packet_GetKeptZoneNamesResponse(std::int64_t id, const ZmiFreshness &zones, const std::list<serialization::query_wrapper> &queries):
		id_(id), zones_(zones), queries_(queries) {}

	std::int64_t id_;
	ZmiFreshness zones_;
	std::list<serialization::query_wrapper> queries_;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & id_;
		ar & zones_;
		ar & queries_;
	}
};


/* Packet_GetZoneAttributesRequest */

struct Packet_GetZoneAttributesRequest {
	static constexpr PacketType type = PacketType::GetZoneAttributesRequest;

	Packet_GetZoneAttributesRequest() = default;
	Packet_GetZoneAttributesRequest(std::int64_t id, const std::set<zone_path> &requested_zones): id_(id), requested_zones_(requested_zones) {}

	std::int64_t id_;
	std::set<zone_path> requested_zones_;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & id_;
		ar & requested_zones_;
	}
};


/* Packet_GetZoneAttributesResponse */

struct Packet_GetZoneAttributesResponse {
	static constexpr PacketType type = PacketType::GetZoneAttributesResponse;

	Packet_GetZoneAttributesResponse() = default;
	Packet_GetZoneAttributesResponse(std::int64_t id, const zone_path &zone, time timestamp, const std::list<serialization::Attribute> &attributes):
		id_(id), zone_(zone), timestamp_(timestamp), attributes_(attributes) {}

	~Packet_GetZoneAttributesResponse() {
		for (auto attr: attributes_) {
			attr.clear();
		}
		attributes_.clear();
	}

	std::int64_t id_;
	zone_path zone_;
	time timestamp_;
	std::list<serialization::Attribute> attributes_;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & id_;
		ar & zone_;
		ar & timestamp_;
		ar & attributes_;
	}
};


/* Packet_SetSingletonZoneAttributeRequest */

struct Packet_SetSingletonZoneAttributeRequest {
	static constexpr PacketType type = PacketType::SetSingletonZoneAttributeRequest;

	Packet_SetSingletonZoneAttributeRequest() = default;
	Packet_SetSingletonZoneAttributeRequest(std::int64_t id, const std::list<serialization::Attribute> &attributes):
		id_(id), attributes_(attributes) {}

	~Packet_SetSingletonZoneAttributeRequest() {
		for (auto attr: attributes_) {
			attr.clear();
		}
		attributes_.clear();
	}

	std::int64_t id_;
	std::list<serialization::Attribute> attributes_;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & id_;
		ar & attributes_;
	}
};


/* Packet_SetSingletonZoneAttributeResponse */

struct Packet_SetSingletonZoneAttributeResponse {
	static constexpr PacketType type = PacketType::SetSingletonZoneAttributeResponse;

	Packet_SetSingletonZoneAttributeResponse() = default;
	Packet_SetSingletonZoneAttributeResponse(std::int64_t id, const bool &result): id_(id), result_(result) {}

	std::int64_t id_;
	bool result_;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & id_;
		ar & result_;
	}
};


/* Packet_InstallQueryRequest */

struct Packet_InstallQueryRequest {
	static constexpr PacketType type = PacketType::InstallQueryRequest;

	Packet_InstallQueryRequest() = default;
	Packet_InstallQueryRequest(std::int64_t id, const serialization::query_wrapper &query):
		id_(id), query_(query) {}

	std::int64_t id_;
	serialization::query_wrapper query_;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & id_;
		ar & query_;
	}
};


/* Packet_InstallQueryResponse */

struct Packet_InstallQueryResponse {
	static constexpr PacketType type = PacketType::InstallQueryResponse;

	Packet_InstallQueryResponse() = default;
	Packet_InstallQueryResponse(std::int64_t id, bool result): id_(id), result_(result) {}

	std::int64_t id_;
	bool result_;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & id_;
		ar & result_;
	}
};


/* Packet_StartGossip */

struct Packet_StartGossip {
	static constexpr PacketType type = PacketType::StartGossip;

	Packet_StartGossip() = default;
	Packet_StartGossip(std::int64_t id, const ZmiFreshness &zones, const std::list<serialization::query_wrapper> &queries):
		id_(id), zones_(zones), queries_(queries) {}

	std::int64_t id_;
	ZmiFreshness zones_;
	std::list<serialization::query_wrapper> queries_;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & id_;
		ar & zones_;
		ar & queries_;
	}
};


/* Packet_GossipReply */

struct Packet_GossipReply {
	static constexpr PacketType type = PacketType::GossipReply;

	Packet_GossipReply() = default;
	Packet_GossipReply(std::int64_t id, std::int64_t last_my_rcv_timestamp, std::int64_t last_their_snd_timestamp,
			const ZmiFreshness &zones, const std::list<serialization::query_wrapper> &queries):
		id_(id), last_my_rcv_timestamp_(last_my_rcv_timestamp), last_their_snd_timestamp_(last_their_snd_timestamp), zones_(zones), queries_(queries) {}

	std::int64_t id_;
	std::int64_t last_my_rcv_timestamp_;
	std::int64_t last_their_snd_timestamp_;
	ZmiFreshness zones_;
	std::list<serialization::query_wrapper> queries_;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & id_;
		ar & last_my_rcv_timestamp_;
		ar & last_their_snd_timestamp_;
		ar & zones_;
		ar & queries_;
	}
};

/* Packet_GossipZoneAttributesRequest */

struct Packet_GossipZoneAttributesRequest {
	static constexpr PacketType type = PacketType::GossipZoneAttributesRequest;

	Packet_GossipZoneAttributesRequest() = default;
	Packet_GossipZoneAttributesRequest(std::int64_t id,  std::int64_t last_my_rcv_timestamp, std::int64_t last_their_snd_timestamp,
			const std::set<zone_path> &requested_zones):
		id_(id), last_my_rcv_timestamp_(last_my_rcv_timestamp), last_their_snd_timestamp_(last_their_snd_timestamp), requested_zones_(requested_zones) {}

	std::int64_t id_;
	std::int64_t last_my_rcv_timestamp_;
	std::int64_t last_their_snd_timestamp_;
	std::set<zone_path> requested_zones_;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & id_;
		ar & last_my_rcv_timestamp_;
		ar & last_their_snd_timestamp_;
		ar & requested_zones_;
	}
};

} /* namespace cloud */


std::ostream & operator<< (std::ostream &os, const cloud::Packet_FetcherData &p);
std::ostream & operator<< (std::ostream &os, const cloud::Packet_QuerySignRequest &p);
std::ostream & operator<< (std::ostream &os, const cloud::Packet_QuerySignResponse &p);
std::ostream & operator<< (std::ostream &os, const cloud::Packet_SetFallbackContactsRequest &p);
std::ostream & operator<< (std::ostream &os, const cloud::Packet_SetFallbackContactsResponse &p);
std::ostream & operator<< (std::ostream &os, const cloud::Packet_GetKeptZoneNamesRequest &p);
std::ostream & operator<< (std::ostream &os, const cloud::Packet_GetKeptZoneNamesResponse &p);
std::ostream & operator<< (std::ostream &os, const cloud::Packet_GetZoneAttributesRequest &p);
std::ostream & operator<< (std::ostream &os, const cloud::Packet_GetZoneAttributesResponse &p);
std::ostream & operator<< (std::ostream &os, const cloud::Packet_SetSingletonZoneAttributeRequest &p);
std::ostream & operator<< (std::ostream &os, const cloud::Packet_SetSingletonZoneAttributeResponse &p);
std::ostream & operator<< (std::ostream &os, const cloud::Packet_InstallQueryRequest &p);
std::ostream & operator<< (std::ostream &os, const cloud::Packet_InstallQueryResponse &p);
std::ostream & operator<< (std::ostream &os, const cloud::Packet_StartGossip &p);
std::ostream & operator<< (std::ostream &os, const cloud::Packet_GossipReply &p);
std::ostream & operator<< (std::ostream &os, const cloud::Packet_GossipZoneAttributesRequest &p);
std::ostream & operator<< (std::ostream &os, const cloud::PacketType &p);
