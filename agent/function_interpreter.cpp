#include "function_interpreter.h"

#include <algorithm>
#include <cmath>
#include <sstream>
#include <type_traits>

#include "expr_result_visitors.h"
#include "log_utils.h"
#include "value_visitors.h"

#undef  LOG_DEFAULT_CATEGORY
#define LOG_DEFAULT_CATEGORY PARSER

namespace cloud {

namespace function {

using cloud_function = std::add_pointer<expr_result(const std::list<expr_result> &args)>::type;

expr_result distinct_cloud(const std::list<expr_result> &args);
expr_result unfold_cloud(const std::list<expr_result> &args);

expr_result avg_cloud(const std::list<expr_result> &args);
expr_result count_cloud(const std::list<expr_result> &args);
expr_result  sum_cloud(const std::list<expr_result> &args);
expr_result first_cloud(const std::list<expr_result> &args);
expr_result last_cloud(const std::list<expr_result> &args);
expr_result random_cloud(const std::list<expr_result> &args);
expr_result min_cloud(const std::list<expr_result> &args);
expr_result max_cloud(const std::list<expr_result> &args);
expr_result land_cloud(const std::list<expr_result> &args);
expr_result lor_cloud(const std::list<expr_result> &args);


expr_result isNull_cloud(const std::list<expr_result> &args);
expr_result now_cloud(const std::list<expr_result> &args);
expr_result epoch_cloud(const std::list<expr_result> &args);
expr_result size_cloud(const std::list<expr_result> &args);
expr_result round_cloud(const std::list<expr_result> &args);
expr_result floor_cloud(const std::list<expr_result> &args);
expr_result ceil_cloud(const std::list<expr_result> &args);

expr_result to_boolean_cloud(const std::list<expr_result> &args);
expr_result to_contact_cloud(const std::list<expr_result> &args);
expr_result to_double_cloud(const std::list<expr_result> &args);
expr_result to_duration_cloud(const std::list<expr_result> &args);
expr_result to_integer_cloud(const std::list<expr_result> &args);
expr_result to_list_cloud(const std::list<expr_result> &args);
expr_result to_set_cloud(const std::list<expr_result> &args);
expr_result to_string_cloud(const std::list<expr_result> &args);
expr_result to_time_cloud(const std::list<expr_result> &args);

static const std::map<std::string, cloud_function> cloud_supported_functions {
	{"distinct", &distinct_cloud},
	{"unfold", &unfold_cloud},

	{"avg", &avg_cloud},
	{"count", &count_cloud},
	{"sum", &sum_cloud},
	{"first", &first_cloud},
	{"last", &last_cloud},
	{"random", &random_cloud},
	{"min", &min_cloud},
	{"max", &max_cloud},
	{"land", &land_cloud},
	{"lor", &lor_cloud},


	{"isNull", &isNull_cloud},
	{"now", &now_cloud},
	{"epoch", &epoch_cloud},
	{"size", &size_cloud},
	{"round", &round_cloud},
	{"floor", &floor_cloud},
	{"ceil", &ceil_cloud},

	{"to_boolean", &to_boolean_cloud},
	{"to_contact", &to_contact_cloud},
	{"to_double", &to_double_cloud},
	{"to_duration", &to_duration_cloud},
	{"to_integer", &to_integer_cloud},
	{"to_list", &to_list_cloud},
	{"to_set", &to_set_cloud},
	{"to_string", &to_string_cloud},
	{"to_time", &to_time_cloud}
};

} /* namespace function */

expr_result process_query_function(const std::string &name, const std::list<expr_result> &args)
{
	LOG("process_query_function" LG(name, args));
	auto iter = function::cloud_supported_functions.find(name);
	if (iter == function::cloud_supported_functions.end()) {
		throw std::runtime_error("funciton does not exists");
	}
	return iter->second(args);
}


namespace function {

template<typename T>
std::list<value> filter_nulls(const T &expr) {
	std::list<value> new_vals;
	for (auto iter = expr.val().begin(); iter != expr.val().end(); ++iter) {
		if (iter->is) {
			new_vals.push_back(iter->val);
		}
	}
	return new_vals;
}

template<typename T>
std::tuple<bool, std::list<value>> filter_nulls_and_check_null(const T &expr)
{
	if (expr.is_null()) {
		return std::make_tuple(false, std::list<value>());
	}
	if (expr.val().size() == 0) {
		return std::make_tuple(true, std::list<value>());
	}
	std::list<value> vals = filter_nulls<T>(expr);
	if (vals.empty()) {
		return std::make_tuple(false, std::list<value>());
	}
	return std::make_tuple(true, vals);
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * distinct  * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

struct distinct_visitor: public boost::static_visitor<list_r>
{
	template<typename V>
	enable_if_same<V, one_r, list_r>
	operator () (const V &expr) const {
		LOG(WARN, "distinct requires col_r or list_r" LG(expr));
		throw std::runtime_error("distinct requires col_r or list_r");
		return list_r(SimpleType::BOOL);
	}

	template<typename V>
	disable_if_same<V, one_r, list_r>
	operator () (const V &expr) const {
		if (expr.is_null()) {
			return list_r(expr.t);
		}
		std::set<nullable_value> vals;
		std::list<nullable_value> new_val;
		for (auto iter = expr.val().begin(); iter != expr.val().end(); ++iter) {
			if (vals.find(*iter) == vals.end()) {
				vals.insert(*iter);
				new_val.push_back(*iter);
			}
		}
		return list_r(expr.t, new_val);
	}
};

expr_result distinct_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "distinct_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("distinct_cloud invalid function arity");
	}
	return boost::apply_visitor(distinct_visitor(), args.front());
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * unfold  * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

struct unfold_value_visitor: public boost::static_visitor<std::list<value>>
{
	template<typename T>
	enable_if_same<T, std::list<value>, std::list<value>>
	operator() (const T &v) const
	{
		return v;
	}

	template<typename T>
	enable_if_same<T, std::set<value>, std::list<value>>
	operator() (const T &v) const
	{
		return std::list<value>(v.begin(), v.end());
	}

	template<typename T>
	typename boost::disable_if<
		boost::mpl::or_<
			std::is_same<T, std::list<value> >,
			std::is_same<T, std::set<value> >
		>, std::list<value>>::type
	operator() (const T &v) const
	{
		LOG(WARN, "unfold_value_visitor expected collection value" LG(v));
		throw std::runtime_error("unfold_value_visitor expected collection value");
		return std::list<value>();
	}
};

struct unfold_visitor: public boost::static_visitor<list_r>
{
	template<typename V>
	enable_if_same<V, one_r, list_r>
	operator () (const V &expr) const {
		LOG(WARN, "unfold_visitor requires col_r or list_r" LG(expr));
		throw std::runtime_error("unfold_visitor requires col_r or list_r");
		return list_r(SimpleType::BOOL);

	}
	template<typename V>
	disable_if_same<V, one_r, list_r>
	operator () (const V &expr) const {
		auto filtered_expr = filter_nulls_and_check_null<V>(expr);
		if (!std::get<0>(filtered_expr)) {
			return list_r(expr.t.strip_collection_layer());
		}
		std::list<value> vals = std::get<1>(filtered_expr);
		std::list<nullable_value> unfolded_vals;
		for (auto iter = vals.begin(); iter != vals.end(); ++iter) {
			for (auto unfolded_val: boost::apply_visitor(unfold_value_visitor(), *iter)) {
				unfolded_vals.push_back(nullable_value(unfolded_val));
			}
		}
		return list_r(expr.t.strip_collection_layer(), unfolded_vals);
	}
};

expr_result unfold_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "unfold_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("unfold_cloud invalid function arity");
	}
	return boost::apply_visitor(unfold_visitor(), args.front());
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * avg * * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

struct avg_visitor: public boost::static_visitor<one_r>
{
	template<typename V>
	enable_if_same<V, one_r, one_r>
	operator () (const V &expr) const {
		LOG(WARN, "avg_visitor requires col_r or list_r" LG(expr));
		throw std::runtime_error("avg_visitor requires col_r or list_r");
		return one_r(SimpleType::INTEGER);

	}
	template<typename V>
	disable_if_same<V, one_r, one_r>
	operator () (const V &expr) const {
		if (!expr.t.collection_.empty()) {
			LOG(WARN, "avg_visitor na kolekcji" LG(expr, expr.t));
			throw std::runtime_error("avg_visitor na kolekcji");
		}
		value_type result_t(SimpleType::DOUBLE);
		switch (expr.t.type_) {
		case SimpleType::DURATION:
			result_t = SimpleType::DURATION;
			break;

		case SimpleType::DOUBLE:
		case SimpleType::INTEGER:
			break;
		default:
			LOG(WARN, "avg_visitor na niepoprawnym typie" LG(expr, expr.t));
			throw std::runtime_error("avg_visitor na niepoprawnym typie");
			break;
		}
		auto filtered_expr = filter_nulls_and_check_null<V>(expr);
		if (!std::get<0>(filtered_expr)) {
			return one_r(result_t);
		}
		std::list<value> vals = std::get<1>(filtered_expr);
		if (vals.size() == 0) {
			LOG(WARN, "avg_visitor srednia na pustej liscie" LG(filtered_expr));
			throw std::runtime_error("avg_visitor srednia na pustej liscie");
		}
		value sum = vals.front();
		for (auto iter = ++vals.begin(); iter != vals.end(); ++iter) {
			sum = boost::apply_visitor(add_value_operator(sum), *iter);
		}
		value avg;
		if (expr.t.type_ == SimpleType::DOUBLE) {
			avg = boost::apply_visitor(div_value_operator(value(double(vals.size()))), sum);
		} else {
			avg = boost::apply_visitor(div_value_operator(value(integer(vals.size()))), sum);
		}

		return one_r(result_t, avg);
	}
};

expr_result avg_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "avg_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("avg_cloud invalid function arity");
	}
	return boost::apply_visitor(avg_visitor(), args.front());
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * count * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

struct count_visitor: public boost::static_visitor<one_r>
{
	template<typename V>
	enable_if_same<V, one_r, one_r>
	operator () (const V &expr) const {
		LOG(WARN, "count_cloud requires col_r or list_r" LG(expr));
		throw std::runtime_error("count_cloud requires col_r or list_r");
		return one_r(SimpleType::INTEGER);

	}
	template<typename V>
	disable_if_same<V, one_r, one_r>
	operator () (const V &expr) const {
		auto filtered_expr = filter_nulls_and_check_null<V>(expr);
		if (!std::get<0>(filtered_expr)) {
			return one_r(SimpleType::INTEGER);
		}
		return one_r(SimpleType::INTEGER, integer(std::get<1>(filtered_expr).size()));
	}
};

expr_result count_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "count_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("count_cloud invalid function arity");
	}
	return boost::apply_visitor(count_visitor(), args.front());
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * sum * * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

struct sum_visitor: public boost::static_visitor<one_r>
{
	template<typename V>
	enable_if_same<V, one_r, one_r>
	operator () (const V &expr) const {
		LOG(WARN, "sum_visitor requires col_r or list_r" LG(expr));
		throw std::runtime_error("sum_visitor requires col_r or list_r");
		return one_r(SimpleType::INTEGER);

	}
	template<typename V>
	disable_if_same<V, one_r, one_r>
	operator () (const V &expr) const {
		if (!expr.t.collection_.empty()) {
			LOG(WARN, "sum_visitor na kolekcji" LG(expr, expr.t));
			throw std::runtime_error("sum_visitor na kolekcji");
		}
		value_type result_t(expr.t.type_);
		switch (expr.t.type_) {
		case SimpleType::DURATION:
		case SimpleType::DOUBLE:
		case SimpleType::INTEGER:
			break;

		default:
			LOG(WARN, "sum_visitor na niepoprawnym typie" LG(expr, expr.t));
			throw std::runtime_error("sum_visitor na niepoprawnym typie");
			break;
		}

		auto filtered_expr = filter_nulls_and_check_null<V>(expr);
		if (!std::get<0>(filtered_expr)) {
			return one_r(result_t);
		}
		std::list<value> vals = std::get<1>(filtered_expr);
		value sum = get_empty_value(result_t);
		for (auto iter = vals.begin(); iter != vals.end(); ++iter) {
			sum = boost::apply_visitor(add_value_operator(sum), *iter);
		}
		check_type_compatibility_throw(sum, result_t);
		return one_r(result_t, sum);
	}
};

expr_result sum_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "sum_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("sum_cloud invalid function arity");
	}
	return boost::apply_visitor(sum_visitor(), args.front());
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * first * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

template<typename Selector>
struct get_elements_visitor: public boost::static_visitor<one_r>
{
	size_t n;
	get_elements_visitor(size_t n): n(n) {
		if (n < 0) {
			LOG(WARN, "first_cloud negative elements to take" LG(n));
			throw std::runtime_error("first_cloud negative elements to take");
		}
	}

	template<typename V>
	enable_if_same<V, one_r, one_r>
	operator () (const V &expr) const {
		LOG(WARN, "min_max_function_visitor requires col_r or list_r" LG(expr));
		throw std::runtime_error("min_max_function_visitor requires col_r or list_r");
		return one_r(SimpleType::INTEGER);

	}
	template<typename V>
	disable_if_same<V, one_r, one_r>
	operator () (const V &expr) const {
		value_type result_t = expr.t;
		result_t.collection_.insert(result_t.collection_.begin(), CollectionType::LIST);
		auto filtered_expr = filter_nulls_and_check_null<V>(expr);
		if (!std::get<0>(filtered_expr)) {
			return one_r(result_t);
		}
		return one_r(result_t, Selector::select(n, std::get<1>(filtered_expr)));
	}
};

template<typename Selector>
one_r get_elements_wrapper(const std::list<expr_result> &args) {
	if (args.size() != 2) {
		LOG(WARN, "get_elements_wrapper invalid function arity" LG(args.size()));
		throw std::runtime_error("get_elements_wrapper invalid function arity");
	}
	auto second_arg = *(++args.begin());
	one_r expr_n = boost::apply_visitor(get_expr_value<one_r>(), args.front());
	if (expr_n.is_null()) {
		value_type result_t = boost::apply_visitor(get_expr_type(), second_arg );
		result_t.collection_.insert(result_t.collection_.begin(), CollectionType::LIST);
		return one_r(result_t);
	}
	integer n = get_value_wrapper<integer>(expr_n.val());
	return boost::apply_visitor(get_elements_visitor<Selector>(n), second_arg );
}

struct first_select {
	static std::list<value> select(size_t n, std::list<value> v) {
		if (n >= v.size()) {
			return v;
		}
		auto iter = v.begin();
		while (n > 0) {
			++iter;
			--n;
		}
		return std::list<value>(v.begin(), iter);
	}
};

expr_result first_cloud(const std::list<expr_result> &args)
{
	return get_elements_wrapper<first_select>(args);
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * last  * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */


struct last_select {
	static std::list<value> select(size_t n, std::list<value> v) {
		if (n >= v.size()) {
			return v;
		}
		auto iter = v.end();
		while (n > 0) {
			--iter;
			--n;
		}
		return std::list<value>(iter, v.end());
	}
};

expr_result last_cloud(const std::list<expr_result> &args)
{
	return get_elements_wrapper<last_select>(args);
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * random  * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

struct random_select {
	static std::list<value> select(size_t n, std::list<value> v) {
		if (n >= v.size()) {
			return v;
		}
		std::vector<value> vals(v.begin(), v.end());
		std::random_shuffle(vals.begin(), vals.end());
		return std::list<value>(vals.begin(), vals.begin() + n);
	}
};

expr_result random_cloud(const std::list<expr_result> &args)
{
	return get_elements_wrapper<random_select>(args);
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * min * * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

template<bool max_>
struct min_max_function_visitor: public boost::static_visitor<one_r>
{
	template<typename V>
	enable_if_same<V, one_r, one_r>
	operator () (const V &expr) const {
		LOG(WARN, "min_max_function_visitor requires col_r or list_r" LG(expr));
		throw std::runtime_error("min_max_function_visitor requires col_r or list_r");
		return one_r(SimpleType::INTEGER);

	}
	template<typename V>
	disable_if_same<V, one_r, one_r>
	operator () (const V &expr) const {
		if (!expr.t.isSimpleType()) {
			LOG(WARN, "min_max_function_visitor na niepoprawnym typie" LG(expr, expr.t));
			throw std::runtime_error("min_max_function_visitor na niepoprawnym typie");
		}
		switch (expr.t.type_) {
		case SimpleType::BOOL:
		case SimpleType::CONTACT:
			LOG(WARN, "min_max_function_visitor na niepoprawnym typie" LG(expr, expr.t));
			throw std::runtime_error("min_max_function_visitor na niepoprawnym typie");
			break;
		default:
			break;
		}
		auto filtered_expr = filter_nulls_and_check_null<V>(expr);
		if (!std::get<0>(filtered_expr)) {
			return one_r(expr.t);
		}
		std::list<value> vals = std::get<1>(filtered_expr);
		if (vals.size() == 0) {
			return one_r(expr.t);
		}
		std::set<value> result(vals.begin(), vals.end());
		if (max_) {
			return one_r(expr.t, *(--result.end()));
		}
		return one_r(expr.t, *(result.begin()));
	}
};

expr_result min_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "min_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("min_cloud invalid function arity");
	}
	return boost::apply_visitor(min_max_function_visitor<false>(), args.front());
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * max * * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

expr_result max_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "max_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("max_cloud invalid function arity");
	}
	return boost::apply_visitor(min_max_function_visitor<true>(), args.front());
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * land  * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

template<typename Visitor>
struct logic_sum_visitor: public boost::static_visitor<one_r>
{
	template<typename V>
	enable_if_same<V, one_r, one_r>
	operator () (const V &expr) const {
		LOG(WARN, "sum_visitor requires col_r or list_r" LG(expr));
		throw std::runtime_error("sum_visitor requires col_r or list_r");
		return one_r(SimpleType::INTEGER);

	}
	template<typename V>
	disable_if_same<V, one_r, one_r>
	operator () (const V &expr) const {
		if (!expr.t.isSimpleType(SimpleType::BOOL)) {
			LOG(WARN, "logic_sum_visitor na niepoprawnym typie" LG(expr, expr.t));
			throw std::runtime_error("logic_sum_visitor na niepoprawnym typie");
		}
		auto filtered_expr = filter_nulls_and_check_null<V>(expr);
		if (!std::get<0>(filtered_expr)) {
			return one_r(SimpleType::BOOL);
		}
		std::list<value> vals = std::get<1>(filtered_expr);
		if (vals.size() == 0) {
			return one_r(SimpleType::BOOL, true);
		}
		value sum = vals.front();
		for (auto iter = ++vals.begin(); iter != vals.end(); ++iter) {
			sum = boost::apply_visitor(Visitor(sum), *iter);
		}
		return one_r(SimpleType::BOOL, sum);
	}
};

expr_result land_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "land_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("land_cloud invalid function arity");
	}
	return boost::apply_visitor(logic_sum_visitor<and_operator>(), args.front());
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * lor * * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

expr_result lor_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "lor_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("lor_cloud invalid function arity");
	}
	return boost::apply_visitor(logic_sum_visitor<or_operator>(), args.front());
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * isNull  * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

struct is_null_function_visitor: public boost::static_visitor<bool>
{
	template<typename T>
	bool operator () (const T &v) const {
		return v.is_null();
	}
};

expr_result isNull_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "isNull_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("isNull_cloud invalid function arity");
	}
	return one_r(SimpleType::BOOL, boost::apply_visitor(is_null_function_visitor(), args.front()));
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * now * * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

expr_result now_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 0) {
		LOG(WARN, "now_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("now_cloud invalid function arity");
	}
	return one_r(SimpleType::TIME, time::now());
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * epoch * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

expr_result epoch_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 0) {
		LOG(WARN, "epoch_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("epoch_cloud invalid function arity");
	}
	return one_r(SimpleType::TIME, time::epoch());	//!< pownien zwracac  2000/01/01 00:00:00.000 CET.
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * size  * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

struct size_function_visitor: public boost::static_visitor<value>
{
	static value_type get_result_type(const value_type& t) {
		if (t.collection_.empty() && t.type_ != SimpleType::STRING) {
			LOG(WARN, "size_function_visitor expected sizeable value" LG(t));
			throw std::runtime_error("size_function_visitor expected sizeable value");
		}
		return SimpleType::INTEGER;
	}

	template<typename T>
	typename boost::enable_if<boost::mpl::or_<
			std::is_same<T, string>,
			std::is_same<T, list<value>>,
			std::is_same<T, set<value>>
		>, value>::type
	operator() (const T &v) const
	{
		LOG("size_function_visitor" LG(v));
		return value(integer(v.size()));
	}

	template<typename T>
	typename boost::disable_if<boost::mpl::or_<
			std::is_same<T, string>,
			std::is_same<T, list<value>>,
			std::is_same<T, set<value>>
		>, value>::type
	operator() (const T &v) const
	{
		LOG(WARN, "size_function_visitor expected sizeable value" LG(v));
		throw std::runtime_error("size_function_visitor expected sizeable value");
		return value(integer(0));
	}
};

expr_result size_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "size_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("size_cloud invalid function arity");
	}
	return boost::apply_visitor(unary_op_expr_result_visitor<size_function_visitor>(), args.front());
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * round * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

struct double_numeric_function_visitor: public boost::static_visitor<value>
{

	static value_type get_result_type(const value_type& t) {
		if (!t.isSimpleType(SimpleType::DOUBLE)) {
			LOG(WARN, "double_numeric_function_visitor expected double value" LG(t));
			throw std::runtime_error("double_numeric_function_visitor expected double value");
		}
		return SimpleType::DOUBLE;
	}

	std::function<double(double)> f;
	double_numeric_function_visitor(std::function<double(double)> f): f(f) {}
	template<typename T>
	enable_if_same<T, double, value>
	operator() (const T &v) const
	{
		return value(f(v));
	}

	template<typename T>
	disable_if_same<T, double, value>
	operator() (const T &v) const
	{
		LOG(WARN, "double_numeric_function_visitor expected double value" LG(v));
		throw std::runtime_error("double_numeric_function_visitor expected double value");
		return value(0.0);
	}
};

expr_result round_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "round_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("round_cloud invalid function arity");
	}
	auto f = [] (double d)-> double {
		return round(d);
	};
	return boost::apply_visitor(unary_op_expr_result_visitor<double_numeric_function_visitor>(f), args.front());
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * floor * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

expr_result floor_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "floor_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("floor_cloud invalid function arity");
	}
	auto f = [] (double d)-> double {
		return floor(d);
	};
	return boost::apply_visitor(unary_op_expr_result_visitor<double_numeric_function_visitor>(f), args.front());
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * ceil  * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

expr_result ceil_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "ceil_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("ceil_cloud invalid function arity");
	}
	auto f = [] (double d)-> double {
		return ceil(d);
	};
	return boost::apply_visitor(unary_op_expr_result_visitor<double_numeric_function_visitor>(f), args.front());
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * to_bool * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */


struct to_boolean_conversion_visitor: public boost::static_visitor<value>
{

	static value_type get_result_type(const value_type& t) {
		if (!t.isSimpleType(SimpleType::STRING)) {
			LOG(WARN, "to_boolean_conversion_visitor expected string value" LG(t));
			throw std::runtime_error("to_boolean_conversion_visitor expected string value");
		}
		return SimpleType::BOOL;
	}

	template<typename T>
	enable_if_same<T, string, value>
	operator() (const T &v) const
	{
		if (v == std::string("true")) {
			return value(true);
		}
		if (v == std::string("false")) {
			return value(false);
		}
		LOG(WARN, "to_boolean_conversion_visitor invalid string to bool conversion" LG(v));
		throw std::runtime_error("to_boolean_conversion_visitor invalid string to bool conversion");
		return value(false);
	}

	template<typename T>
	disable_if_same<T, string, value>
	operator() (const T &v) const
	{
		LOG(WARN, "to_boolean_conversion_visitor expected string value" LG(v));
		throw std::runtime_error("to_boolean_conversion_visitor expected string value");
		return value(false);
	}
};


expr_result to_boolean_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		throw std::runtime_error("to_boolean_cloud invalid function arity");
	}
	return boost::apply_visitor(unary_op_expr_result_visitor<to_boolean_conversion_visitor>(), args.front());
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * to_contact  * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

struct to_contact_conversion_visitor: public boost::static_visitor<value>
{

	static value_type get_result_type(const value_type& t) {
		if (!t.isSimpleType(SimpleType::STRING)) {
			LOG(WARN, "to_contact_conversion_visitor expected string value" LG(t));
			throw std::runtime_error("to_contact_conversion_visitor expected string value");
		}
		return SimpleType::CONTACT;
	}

	template<typename T>
	enable_if_same<T, string, value>
	operator() (const T &v) const
	{
		return value(contact_from_string_throw(v));
	}

	template<typename T>
	disable_if_same<T, string, value>
	operator() (const T &v) const
	{
		LOG(WARN, "to_contact_conversion_visitor expected string value" LG(v));
		throw std::runtime_error("to_contact_conversion_visitor expected string value");
		return value(contact());
	}
};

expr_result to_contact_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "to_contact_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("to_contact_cloud invalid function arity");
	}
	return boost::apply_visitor(unary_op_expr_result_visitor<to_contact_conversion_visitor>(), args.front());
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * to_double * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */


struct to_double_conversion_visitor: public boost::static_visitor<value>
{
	static value_type get_result_type(const value_type& t) {
		if (!t.isSimpleType(SimpleType::INTEGER) && !t.isSimpleType(SimpleType::STRING))
		{
			LOG(WARN, "to_double_conversion_visitor expected integer or string value" LG(t));
			throw std::runtime_error("to_double_conversion_visitor expected integer or string value");
		}
		return SimpleType::DOUBLE;
	}

	template<typename T>
	enable_if_same<T, integer, value>
	operator() (const T &v) const
	{
		return value(static_cast<double>(v));
	}

	template<typename T>
	enable_if_same<T, string, value>
	operator() (const T &v) const
	{
		return value(std::stod(v));
	}

	template<typename T>
	typename boost::disable_if<boost::mpl::or_<
			std::is_same<T, integer>,
			std::is_same<T, string>
		>, value>::type
	operator() (const T &v) const
	{
		LOG(WARN, "to_double_conversion_visitor expected integer or string value" LG(v));
		throw std::runtime_error("to_double_conversion_visitor expected integer or string value");
		return value(0.0);
	}
};

expr_result to_double_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "to_double_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("to_double_cloud invalid function arity");
	}
	return boost::apply_visitor(unary_op_expr_result_visitor<to_double_conversion_visitor>(), args.front());
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * duration  * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

struct to_duration_conversion_visitor: public boost::static_visitor<value>
{

	static value_type get_result_type(const value_type& t) {
		if (!t.isSimpleType(SimpleType::INTEGER) && !t.isSimpleType(SimpleType::STRING))
		{
			LOG(WARN, "to_duration_conversion_visitor expected integer or string value" LG(t));
			throw std::runtime_error("to_duration_conversion_visitor expected integer or string value");
		}
		return SimpleType::DURATION;
	}

	template<typename T>
	enable_if_same<T, integer, value>
	operator() (const T &v) const
	{
		return value(duration(v));
	}

	template<typename T>
	enable_if_same<T, string, value>
	operator() (const T &v) const
	{
		duration d;
		if (!d.from_string(v)) {
			LOG(WARN, "to_duration_conversion_visitor could not convert from string" LG(v));
			throw std::runtime_error("to_duration_conversion_visitor could not convert from string");
		}
		return value(d);
	}

	template<typename T>
	typename boost::disable_if<boost::mpl::or_<
			std::is_same<T, integer>,
			std::is_same<T, string>
		>, value>::type
	operator() (const T &v) const
	{
		LOG(WARN, "to_duration_conversion_visitor expected integer or string value" LG(v));
		throw std::runtime_error("to_duration_conversion_visitor expected integer or string value");
		return value(duration());
	}
};

expr_result to_duration_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "to_duration_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("to_duration_cloud invalid function arity");
	}
	return boost::apply_visitor(unary_op_expr_result_visitor<to_duration_conversion_visitor>(), args.front());
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * to_integer  * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

struct to_integer_conversion_visitor: public boost::static_visitor<value>
{

	static value_type get_result_type(const value_type& t) {
		if (!t.isSimpleType(SimpleType::DOUBLE) && !t.isSimpleType(SimpleType::DURATION) && !t.isSimpleType(SimpleType::STRING))
		{
			LOG(WARN, "to_integer_conversion_visitor expected double, duration or string value" LG(t));
			throw std::runtime_error("to_integer_conversion_visitor expected double, duration or string value");
		}
		return SimpleType::INTEGER;
	}

	template<typename T>
	enable_if_same<T, double, value>
	operator() (const T &v) const
	{
		return value(static_cast<integer>(v));
	}


	template<typename T>
	enable_if_same<T, duration, value>
	operator() (const T &v) const
	{
		return value(v.v);
	}

	template<typename T>
	enable_if_same<T, string, value>
	operator() (const T &v) const
	{
		return value(integer(std::stoll(v)));
	}

	template<typename T>
	typename boost::disable_if<boost::mpl::or_<
			std::is_same<T, double>,
			std::is_same<T, duration>,
			std::is_same<T, string>
		>, value>::type
	operator() (const T &v) const
	{
		LOG(WARN, "to_integer_conversion_visitor expected double, duration or string value" LG(v));
		throw std::runtime_error("to_integer_conversion_visitor expected double, duration or string value");
		return value(integer());
	}
};

expr_result to_integer_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "to_integer_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("to_integer_cloud invalid function arity");
	}
	return boost::apply_visitor(unary_op_expr_result_visitor<to_integer_conversion_visitor>(), args.front());
}


/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * to_list * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

struct to_list_conversion_visitor: public boost::static_visitor<value>
{

	static value_type get_result_type(const value_type& t) {
		if (t.collection_.empty() || t.collection_.front() != CollectionType::SET) {
			LOG(WARN, "to_list_conversion_visitor expected set value" LG(t));
			throw std::runtime_error("to_list_conversion_visitor expected set value");
		}
		value_type new_t = t;
		new_t.collection_.front() = CollectionType::LIST;
		return new_t;
	}

	template<typename T>
	enable_if_same<T, set<value>, value>
	operator() (const T &v) const
	{
		return list<value>(v.begin(), v.end());
	}

	template<typename T>
	disable_if_same<T, set<value>, value>
	operator() (const T &v) const
	{
		LOG(WARN, "to_list_conversion_visitor expected set value" LG(v));
		throw std::runtime_error("to_list_conversion_visitor expected set value");
		return list<value>();
	}
};

expr_result to_list_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "to_list_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("to_list_cloud invalid function arity");
	}
	return boost::apply_visitor(unary_op_expr_result_visitor<to_list_conversion_visitor>(), args.front());
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * to_set  * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

struct to_set_conversion_visitor: public boost::static_visitor<value>
{

	static value_type get_result_type(const value_type& t) {
		if (t.collection_.empty() || t.collection_.front() != CollectionType::LIST) {
			LOG(WARN, "to_set_conversion_visitor expected list value" LG(t));
			throw std::runtime_error("to_set_conversion_visitor expected list value");
		}
		value_type new_t = t;
		new_t.collection_.front() = CollectionType::SET;
		return new_t;
	}

	template<typename T>
	enable_if_same<T, list<value>, value>
	operator() (const T &v) const
	{
		set<value> s;
		for (auto el: v) {
			s.insert(el);
		}
		return s;
	}

	template<typename T>
	disable_if_same<T, list<value>, value>
	operator() (const T &v) const
	{
		LOG(WARN, "to_set_conversion_visitor expected list value" LG(v));
		throw std::runtime_error("to_set_conversion_visitor expected list value");
		return value(set<value>());
	}
};

expr_result to_set_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "to_set_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("to_set_cloud invalid function arity");
	}
	return boost::apply_visitor(unary_op_expr_result_visitor<to_set_conversion_visitor>(), args.front());
}


/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * to_string * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */


struct to_string_conversion_visitor: public boost::static_visitor<value>
{
	static value_type get_result_type(const value_type& t) {
		return SimpleType::STRING;
	}

	template<typename T>
	enable_if_same<T, bool, value>
	operator() (const T &v) const {
		return v ? value("true") :  value("false");
	}

	template<typename T>
	disable_if_same<T, bool, value>
	operator() (const T &v) const {
		std::stringstream ss;
		ss << v;
		return value(ss.str());
	}
};

expr_result to_string_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "to_string_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("to_string_cloud invalid function arity");
	}
	return boost::apply_visitor(unary_op_expr_result_visitor<to_string_conversion_visitor>(), args.front());
}

/* * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * avg * * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * */

struct to_time_conversion_visitor: public boost::static_visitor<value>
{

	static value_type get_result_type(const value_type& t) {
		if (!t.isSimpleType(SimpleType::STRING)) {
			LOG(WARN, "to_time_conversion_visitor expected string value" LG(t));
			throw std::runtime_error("to_time_conversion_visitor expected string value");
		}
		return SimpleType::TIME;
	}
	template<typename T>
	enable_if_same<T, string, value>
	operator() (const T &v) const
	{
		time t;
		if (!t.from_string(v)) {
			LOG(WARN, "to_time_conversion_visitor could not convert from string" LG(v));
			throw std::runtime_error("to_time_conversion_visitor could not convert from string");
		}
		return value(t);
	}

	template<typename T>
	disable_if_same<T, string, value>
	operator() (const T &v) const
	{
		LOG(WARN, "to_time_conversion_visitor expected integer or string value" LG(v));
		throw std::runtime_error("to_time_conversion_visitor expected integer or string value");
		return value(time());
	}
};

expr_result to_time_cloud(const std::list<expr_result> &args)
{
	if (args.size() != 1) {
		LOG(WARN, "to_time_cloud invalid function arity" LG(args.size()));
		throw std::runtime_error("to_time_cloud invalid function arity");
	}
	return boost::apply_visitor(unary_op_expr_result_visitor<to_time_conversion_visitor>(), args.front());
}

} /* namespace function */
} /* namespace cloud */
