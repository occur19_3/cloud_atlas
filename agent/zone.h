#pragma once

#include <map>

#include "attribute.h"
//#define TEST_INTERPRETERA

namespace cloud {
	class Zone;
}
class Program;

std::ostream & operator<<(std::ostream &os, const cloud::Zone &z);

namespace cloud {

using AttributesMap = std::map<std::string, Attribute>;

struct ZMI {
	time last_update_;
	AttributesMap attributes_;
	ZMI() = default;

	void insertBaseAttribute(const Attribute &a);
	void insertAttribute(const Attribute &a);
	std::string getName() const;
	ZmiTimestamp getTimestamp() const;
	std::set<contact> getContacts() const;
	void addContact(const contact &c);
	void removeContact(const contact &c);
};

class Zone {
	static constexpr size_t kMaxContactSetSize = 3;	//!na potrzeby testow jest amal, bo ni echemy wsztskich trzymac
	static const std::list<std::string> BASE_ZMI_QUERIES;
	static const std::list<std::string> BASE_ZMI_THIS_LEVEL_QUERIES;
public:
	friend std::ostream & ::operator<<(std::ostream &os, const ::cloud::Zone &z);
	static Zone* createStartZone(const std::string &owner, size_t level, time now, Zone* father, const zone_path &path);

	Zone(Zone* father);
	~Zone();

	size_t getDepth() const;
	std::string getZoneName() const;
	zone_path getZonePath() const;

	/// Dane o kwerendach trzymamy osobno, w momencie przekalkulowywania, Agent sam uruchamia obliczanie kwerend
	bool installQueryInAllZones(const std::string &query);
	void installQueriesInAllZones(const std::list<std::string> &queries, bool baseZmi = false, bool this_level = false);

	void eraseNonBaseAtrrs();
	void recomputeBaseZmi();
	void purge_old(size_t timeout_sec);

	/// Schodzimy najnizej jak sie da i ustalamy wartosc atrybutow
	void fetcherAttributesUpdate(const std::list<Attribute> &attributes);
	///  zwraca informacje o trzymanych strefach lista zawiera pare< nazwa obecnego poziomu, lista<timestamp, nazwa strefy>
	ZmiFreshness getKeptZoneNames(size_t level) const;
	/// zwraca atrybuty z podanej sciezki, jesli nie znalezlizmy dostaniemy pusta liste
	std::pair<time, std::list<Attribute>> getZoneAttributes(const zone_path &zone);

	/** * * * * Plotkowanie * * * * */

	std::set<contact> getContacts(size_t level) const;
	///zwraca false, jesli nie znalezlismy podanej sciezki lub probujemy zainstalowac cos starszego
	void setSiblingAttributes(const zone_path &path, const ZMI &new_sibling_zmi);
	void addContact(size_t level, const contact &c);
	void removeContact(size_t level, const contact &c);

private:
	bool installQueryInAllZones(Program *p);
	void installQueriesInAllZones(const std::list<Program*> &queries, bool baseZmi, bool this_level);
	bool installQuery(Program *p, bool baseZmi = false, bool this_level = false);

	std::pair<time, std::list<Attribute>> getZoneAttributesHelper(const zone_path &zone, size_t level);
	void setSiblingAttributesHelper(const zone_path &path, size_t level, const ZMI &new_sibling_zmi);

	std::string getThisLevelPathName() const;
	std::string getZoneNamePrefix() const;
	std::string getZoneNameHelper() const;
	std::vector<AttributesMap> getThisLevelAttributes() const;

	Zone* father_ = nullptr;
	Zone* child_ = nullptr;
	ZMI zmi_;
	std::map<std::string, ZMI> sibling_zmi_;	// w sumie wiersze


#ifdef TEST_INTERPRETERA
public:
	Zone() = default;
	std::string installQueryInAllZones_TEST(Program *p);
	void addChild_TEST(Zone *c);

private:
	std::vector<AttributesMap> getChildsAttributes_TEST() const;
	std::string installQuery_TEST(Program *p);

	std::vector<Zone*> childs_TEST_;

	friend Zone *getTestRootZone_TEST();
#endif
};

#ifdef TEST_INTERPRETERA
Zone *getTestRootZone_TEST();
#endif

} /* namespace cloud */

std::ostream & operator<<(std::ostream &os, const cloud::ZMI &z);
std::ostream & operator<<(std::ostream &os, const cloud::Zone &z);
