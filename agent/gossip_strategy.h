#pragma once

#include <stddef.h>

namespace cloud {

class GossipStrategy {
public:
	GossipStrategy(size_t max_level): max_level_(max_level) {}
	virtual ~GossipStrategy() {}

	virtual size_t getCurrentGossipLevel() = 0;

protected:
	const size_t max_level_;
};


class RandomStrategy: public GossipStrategy
{
public:
	RandomStrategy(size_t max_level): GossipStrategy(max_level) {}
	virtual size_t getCurrentGossipLevel() override;
};


class RandomStrategyExp: public GossipStrategy
{
public:
	RandomStrategyExp(size_t max_level): GossipStrategy(max_level) {}
	virtual size_t getCurrentGossipLevel() override;
};



class RoundRobinStrategy: public GossipStrategy
{
public:
	RoundRobinStrategy(size_t max_level): GossipStrategy(max_level), current_(0) {}
	virtual size_t getCurrentGossipLevel() override;

public:
	size_t current_;
};



class RoundRobinStrategyExp: public GossipStrategy
{
public:
	RoundRobinStrategyExp(size_t max_level): GossipStrategy(max_level), current_(new size_t[max_level]) {
		for (size_t i = 0; i < max_level; ++i) current_[i] = 0;
	}
	virtual size_t getCurrentGossipLevel() override;
public:
	size_t *current_;

};
}
