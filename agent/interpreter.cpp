#include "interpreter.h"

#include <iostream>

#include <boost/regex.hpp>

#include "expr_result_visitors.h"
#include "log_utils.h"
#include "function_interpreter.h"
#include "value_visitors.h"
#include "zone.h"

#undef  LOG_DEFAULT_CATEGORY
#define LOG_DEFAULT_CATEGORY PARSER

namespace cloud {

QueryInterpretResult* QueryInterpreter::process_query(Program *p)
{
	try {
		p->accept(this);
		QueryInterpretResult* result = new QueryInterpretResult;
		result->name_ = std::move(query_name_);
		result->attributes_ = std::move(attributes_);
		return result;
	} catch (std::exception &e) {
		LOG(WARN, "parsing error occured " << e.what());
		return nullptr;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * Program * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryInterpreter::visitProgram(Program* t) {} //abstract class

void QueryInterpreter::visitProgramQuery(ProgramQuery *programquery)
{
	visitIdent(programquery->ident_);
	query_name_ = last_ident_;
	LOG("parsing ProgramQuery" LG(query_name_));
	programquery->liststmt_->accept(this);
	for (auto stmt: last_stmt_list_result_) {
		for (auto select: stmt) {
			if (std::get<0>(select).empty()) {
				LOG(WARN, "attributes provided in query must have name");
				throw std::runtime_error("attributes provided in query must have name");
			}
			one_r v = boost::apply_visitor(get_expr_value<one_r>(), std::get<1>(select));
			check_type_compatibility_throw(v.res.val, v.t);
			attributes_.push_back(std::make_tuple(std::get<0>(select), v.t, v.res));
			LOG("pushed back " LG(std::get<0>(select), v.t, v.res.is, v.res.val));
		}
	}
}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * Stmt  * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryInterpreter::visitStmt(Stmt* t) {} //abstract class

void QueryInterpreter::visitListStmt(ListStmt* liststmt)
{
	std::list<stmt_result> stmt_list_result;
	for (ListStmt::iterator i = liststmt->begin() ; i != liststmt->end() ; ++i) {
		(*i)->accept(this);
		stmt_list_result.push_back(last_stmt_result_);
	}
	last_stmt_list_result_ = stmt_list_result;
}

void QueryInterpreter::visitSelStmt(SelStmt *selstmt)
{
	LOG("processing SelStmt");
	selstmt->selectclause_->accept(this);
}

void QueryInterpreter::visitSelOrdStmt(SelOrdStmt *selordstmt)
{
	LOG("processing SelOrdStmt");
	selordstmt->orderclause_->accept(this);
	selordstmt->selectclause_->accept(this);
}

void QueryInterpreter::visitSelWhrStmt(SelWhrStmt *selwhrstmt)
{
	LOG("processing SelWhrStmt");
	selwhrstmt->whereclause_->accept(this);
	selwhrstmt->selectclause_->accept(this);
}

void QueryInterpreter::visitSelWhrOrdStmt(SelWhrOrdStmt *selwhrordstmt)
{
	LOG("processing SelWhrOrdStmt");
	selwhrordstmt->whereclause_->accept(this);
	selwhrordstmt->orderclause_->accept(this);
	selwhrordstmt->selectclause_->accept(this);
}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * SelectClause  * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryInterpreter::visitSelectClause(SelectClause* t) {} //abstract class

void QueryInterpreter::visitSelectClauseRule(SelectClauseRule *selectclauserule)
{
	LOG("processing SelectClauseRule" LG(state_));
	selectclauserule->listselect_->accept(this);
}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * WhereClause * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryInterpreter::visitWhereClause(WhereClause* t) {} //abstract class


void QueryInterpreter::visitWhereClauseRule(WhereClauseRule *whereclauserule)
{
	LOG("parsing visitWhereClauseRule");
	//! zapisujemy stan, bo where operuje na poszcegolnych wierszach
	parse_state saved_state = std::move(state_);	//zapisujemy stare srodowisko

	std::vector<expr_result> where_result;
	for (size_t i = 0; i < std::get<2>(saved_state).size(); ++i) {
		parse_state zone_state = std::make_tuple(false, i, std::get<2>(saved_state));
		state_ = std::move(zone_state);
		LOG("parsing zaraz bedziemy przetwarzali to i owo");
		whereclauserule->expr_->accept(this);
		where_result.push_back(last_expr_result_);
		LOG("processing visitWhereClauseRule");
	}

	if (where_result.size() != std::get<2>(saved_state).size()) {
		LOG("visitWhereClauseRule cos poszlo nie tak rozna ilosc zone'ow i ewaluacji wartosci" LG(where_result.size(), std::get<2>(saved_state).size()));
		throw std::runtime_error("visitWhereClauseRule cos poszlo nie tak rozna ilosc zone'ow i ewaluacji wartosci");
	}
	LOG("parsing visitWhereClauseRule" LG(where_result));
	auto state_iter = std::get<2>(saved_state).begin();
	for (auto whr_iter = where_result.begin(); whr_iter != where_result.end(); ++whr_iter) {
		one_r res = boost::apply_visitor(get_expr_value<one_r>(), *whr_iter);
		bool should_remove = !res.res.is || !get_value_wrapper<bool>(res.res.val);
		if (should_remove) {
			auto rem_iter = state_iter++;
			std::get<2>(saved_state).erase(rem_iter);
		} else {
			++state_iter;
		}
	}
	//!wstawiamy zmoodyfikowane srodowisko
	state_ = std::move(saved_state);
}



/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * OrderClause * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryInterpreter::visitOrderClause(OrderClause* t) {} //abstract class

void QueryInterpreter::visitOrderClauseRule(OrderClauseRule *orderclauserule)
{
	LOG("processing visitOrderClauseRule");
	orderclauserule->listorder_->accept(this);
}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * Order * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryInterpreter::visitOrder(Order* t) {} //abstract class

void QueryInterpreter::visitListOrder(ListOrder* listorder)
{
	for (ListOrder::iterator i = listorder->begin() ; i != listorder->end() ; ++i) {
		(*i)->accept(this);
	}
}

void QueryInterpreter::visitOrderSim(OrderSim *ordersim)
{
	OrderOptNulls orderoptnulls(ordersim->expr_->clone(), new OrderOptDesc, new OrderNullsLast);
	visitOrderOptNulls(&orderoptnulls);
}

void QueryInterpreter::visitOrderNulls(OrderNulls *ordernulls)
{
	OrderOptNulls orderoptnulls(ordernulls->expr_->clone(), new OrderOptDesc, ordernulls->ordernullsopt_->clone());
	visitOrderOptNulls(&orderoptnulls);
}

void QueryInterpreter::visitOrderOptSim(OrderOptSim *orderoptsim)
{
	OrderOptNulls orderoptnulls(orderoptsim->expr_->clone(), orderoptsim->orderopt_->clone(), new OrderNullsLast);
	visitOrderOptNulls(&orderoptnulls);
}

void QueryInterpreter::visitOrderOptNulls(OrderOptNulls *orderoptnulls)
{	
	LOG("parsing visitOrderOptNulls");
//	//! zapisujemy stan, bo where go nie zmienia i wgl
	parse_state saved_state = std::move(state_);	//zapisujemy stare srodowisko

	order_result order_result_;
	size_t i = 0;
	for (; i < std::get<2>(saved_state).size(); ++i) {
		parse_state zone_state = std::make_tuple(false, i, std::get<2>(saved_state));
		state_ = std::move(zone_state);
		orderoptnulls->expr_->accept(this);
		one_r res = boost::apply_visitor(get_expr_value<one_r>(), last_expr_result_);
		order_result_.push_back(std::make_tuple(res.res, i));
	}
//	//! sanity check
	if (order_result_.size() != std::get<2>(state_).size()) {
		LOG("visitOrderOptNulls cos poszlo nie tak rozna ilosc zone'ow i ewaluacji wartosci" LG(order_result_.size(), std::get<2>(state_).size()));
		throw std::runtime_error("visitWhereClauseRule cos poszlo nie tak rozna ilosc zone'ow i ewaluacji wartosci");
	}

	LOG("parsing visitOrderOptNulls po obliczeniu wartosci");
	orderoptnulls->orderopt_->accept(this);
	orderoptnulls->ordernullsopt_->accept(this);
	auto compare_f = [this] (const std::tuple<nullable_value, int> &v1_, const std::tuple<nullable_value, int> &v2_) -> bool {
		nullable_value v1 = std::get<0>(v1_);
		nullable_value v2 = std::get<0>(v2_);
		if (!v1.is) {
			if (!v2.is) {
				return false;
			}
			return last_order_nulls_opt_ == OrderNullsOptType::NULLS_FIRST;
		} else {
			if (!v2.is) {
				return last_order_nulls_opt_ == OrderNullsOptType::NULLS_LAST;
			}
			if (last_order_opt_ == OrderOptType::ASC) {
				return boost::apply_visitor(lth_operator(v2.val), v1.val);
			} else {
				return boost::apply_visitor(gth_operator(v2.val), v1.val);
			}
		}
	};

//	//! sortujemy sobie
	std::stable_sort(order_result_.begin(), order_result_.end(), compare_f);

//	//!przeorganizowanie - troche dla bezdomnych, ale std nie ma fajnej aplikacji permutacji
	std::vector<AttributesMap> reordered_attrs;
	for (auto v: order_result_) {
		reordered_attrs.push_back(std::move( std::get<2>(saved_state).at(std::get<1>(v)) ));
	}
//	//! wrzucamy nowy stan
	state_ = std::make_tuple(std::get<0>(saved_state), std::get<1>(saved_state), reordered_attrs);
}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * OrderOpt  * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryInterpreter::visitOrderOpt(OrderOpt*) {} //abstract class

void QueryInterpreter::visitOrderOptAsc(OrderOptAsc *)
{
	last_order_opt_ = OrderOptType::ASC;
}

void QueryInterpreter::visitOrderOptDesc(OrderOptDesc *)
{
	last_order_opt_ = OrderOptType::DESC;
}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * OrderNullsOpt * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryInterpreter::visitOrderNullsOpt(OrderNullsOpt* ) {} //abstract class

void QueryInterpreter::visitOrderNullsFirst(OrderNullsFirst *)
{
	last_order_nulls_opt_ = OrderNullsOptType::NULLS_FIRST;
}

void QueryInterpreter::visitOrderNullsLast(OrderNullsLast *)
{
	last_order_nulls_opt_ = OrderNullsOptType::NULLS_LAST;
}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * Select  * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryInterpreter::visitSelect(Select*) {} //abstract class

void QueryInterpreter::visitListSelect(ListSelect* listselect)
{
	stmt_result _stmt_result;
	for (ListSelect::iterator i = listselect->begin() ; i != listselect->end() ; ++i)
	{
		(*i)->accept(this);
		_stmt_result.push_back(last_select_result_);
	}
	last_stmt_result_ = _stmt_result;
}

void QueryInterpreter::visitSelectSim(SelectSim *selectsim)
{
	SelectAs selectas(selectsim->selectstart_->clone(), "");
	visitSelectAs(&selectas);
}

void QueryInterpreter::visitSelectAs(SelectAs *selectas)
{
	selectas->selectstart_->accept(this);
	visitIdent(selectas->ident_);
	last_select_result_ = std::make_tuple(last_ident_, last_expr_result_);
}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * SelectStart * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryInterpreter::visitSelectStart(SelectStart*) {} //abstract class

void QueryInterpreter::visitSelectStartSim(SelectStartSim *selectstartsim)
{
	SelectStartOpt selectstartopt(new SelModOptAll, selectstartsim->expr_->clone());
	visitSelectStartOpt(&selectstartopt);
}

void QueryInterpreter::visitSelectStartOpt(SelectStartOpt *selectstartopt)
{
	selectstartopt->selmodopt_->accept(this);
	selectstartopt->expr_->accept(this);
}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * SelModOpt * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryInterpreter::visitSelModOpt(SelModOpt*) {} //abstract class

void QueryInterpreter::visitSelModOptAll(SelModOptAll *)
{
	last_sel_mod_opt_type_ = SelModOptType::ALL;
}

void QueryInterpreter::visitSelModOptDist(SelModOptDist *)
{
	last_sel_mod_opt_type_ = SelModOptType::DISTINCT;
}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * Expr  * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryInterpreter::visitExpr(Expr*) {} //abstract class

void QueryInterpreter::visitListExpr(ListExpr* listexpr)
{
	std::list<expr_result> expr_list_result;
	for (ListExpr::iterator i = listexpr->begin() ; i != listexpr->end() ; ++i)
	{
		(*i)->accept(this);
		expr_list_result.push_back(last_expr_result_);
	}
	last_expr_list_result_ = expr_list_result;
}

void QueryInterpreter::visitEOr(EOr *eor)
{
	eor->expr_1->accept(this);
	expr_result v1 = last_expr_result_;
	eor->expr_2->accept(this);
	expr_result v2 = last_expr_result_;
	last_expr_result_ = apply_bin_op<or_operator>(v1, v2);
}

void QueryInterpreter::visitEAnd(EAnd *eand)
{
	eand->expr_1->accept(this);
	expr_result v1 = last_expr_result_;
	eand->expr_2->accept(this);
	expr_result v2 = last_expr_result_;
	last_expr_result_ = apply_bin_op<and_operator>(v1, v2);
}

void QueryInterpreter::visitENot(ENot *enot)
{
	enot->expr_->accept(this);
	expr_result v1 = last_expr_result_;
	last_expr_result_ = boost::apply_visitor(unary_op_expr_result_visitor<not_value_operator>(), v1);
}

void QueryInterpreter::visitERel(ERel *erel)
{
	erel->expr_1->accept(this);
	expr_result v1 = last_expr_result_;
	erel->relop_->accept(this);
	RelOpType relop = last_rel_op_;
	erel->expr_2->accept(this);
	expr_result v2 = last_expr_result_;
	switch (relop) {
	case RelOpType::LTH:
		last_expr_result_ = apply_bin_op<lth_operator>(v1, v2);
		break;
	case RelOpType::LE:
		last_expr_result_ = apply_bin_op<le_operator>(v1, v2);
		break;
	case RelOpType::GTH:
		last_expr_result_ = apply_bin_op<gth_operator>(v1, v2);
		break;
	case RelOpType::GE:
		last_expr_result_ = apply_bin_op<ge_operator>(v1, v2);
		break;
	case RelOpType::EQU:
		last_expr_result_ = apply_bin_op<equ_operator>(v1, v2);
		break;
	case RelOpType::NE:
		last_expr_result_ = apply_bin_op<ne_operator>(v1, v2);
		break;
	}
}

void QueryInterpreter::visitERegexp(ERegexp *eregexp)
{
	eregexp->expr_->accept(this);
	expr_result v1 = last_expr_result_;
	visitString(eregexp->string_);
	last_expr_result_ = boost::apply_visitor(unary_op_expr_result_visitor<regex_value_operator>(last_string_), v1);
}

void QueryInterpreter::visitEAdd(EAdd *eadd)
{
	eadd->expr_1->accept(this);
	expr_result v1 = last_expr_result_;
	eadd->addop_->accept(this);
	AddOpType op = last_add_op_;
	eadd->expr_2->accept(this);
	expr_result v2 = last_expr_result_;
	LOG("before adding" LG(v1, v2));
	switch (op) {
	case AddOpType::PlusOp:
		last_expr_result_ = apply_bin_op<add_value_operator>(v1, v2);
		break;

	case AddOpType::MinusOp:
		last_expr_result_ = apply_bin_op<minus_value_operator>(v1, v2);
		break;
	}
	LOG("after adding" LG(v1, v2, last_expr_result_));
}

void QueryInterpreter::visitEMul(EMul *emul)
{
	emul->expr_1->accept(this);
	expr_result v1 = last_expr_result_;
	emul->mulop_->accept(this);
	MulOpType mulop = last_mul_op_;
	emul->expr_2->accept(this);
	expr_result v2 = last_expr_result_;
	LOG("before mulop" LG(v1, v2));
	switch (mulop) {
	case MulOpType::TimesOp:
		last_expr_result_ = apply_bin_op<times_value_operator>(v1, v2);
		break;

	case MulOpType::DivideOp:
		last_expr_result_ = apply_bin_op<div_value_operator>(v1, v2);
		break;

	case MulOpType::ModuloOp:
		last_expr_result_ = apply_bin_op<modulo_value_operator>(v1, v2);
		break;
	}
	LOG("after mulop" LG(v1, v2, last_expr_result_));
}

void QueryInterpreter::visitENeg(ENeg *eneg)
{
	eneg->expr_->accept(this);
	expr_result v1 = last_expr_result_;
	last_expr_result_ = boost::apply_visitor(unary_op_expr_result_visitor<negate_value_operator>(), v1);
}

void QueryInterpreter::visitETrue(ETrue *etrue)
{
	last_expr_result_ = one_r(SimpleType::BOOL,  true);
}

void QueryInterpreter::visitEFalse(EFalse *efalse)
{
	last_expr_result_ = one_r(SimpleType::BOOL, false);
}

void QueryInterpreter::visitEInt(EInt *eint)
{
	visitInteger(eint->integer_);
	last_expr_result_ = one_r(SimpleType::INTEGER, last_integer_);
}

void QueryInterpreter::visitEDouble(EDouble *edouble)
{
	visitDouble(edouble->double_);
	last_expr_result_ = one_r(SimpleType::DOUBLE, last_double_);
}

void QueryInterpreter::visitEString(EString *estring)
{
	visitString(estring->string_);
	last_expr_result_ = one_r(SimpleType::STRING, last_string_);
}

void QueryInterpreter::visitEVar(EVar *evar)
{
	visitIdent(evar->ident_);
	std::string var_name = last_ident_;
	LOG("visitEVar szukamy wartosci" LG(var_name, state_));
	//przeszukujemy wszystko nawet w wypadku where'a, ale sprawdzamy, czy bardzo nie wykrzaczyly nam sie atrybuty

	bool found = false;
	std::vector<nullable_value> vals;
	value_type t = SimpleType::BOOL;
	for (const auto &zone_attrs: std::get<2>(state_)) {
		auto iter = zone_attrs.find(var_name);
		if (iter != zone_attrs.end()) {
			found = true;
			vals.push_back(iter->second.value_);
			t = iter->second.type_;
		} else {
			vals.push_back(nullable_value());
		}
	}
	if (!found && std::get<0>(state_)) {
		LOG("provided attribute does not exists" LG(var_name));
		throw std::runtime_error("provided attribute does not exists");
	}
	for (auto iter = vals.begin(); iter != vals.end(); ++iter) {
		if (iter->is) {
			check_type_compatibility_throw(iter->val, t);
		} else {
			iter->val = get_empty_value(t);
		}
	}
	if (std::get<0>(state_)) {
		last_expr_result_ = col_r(t, vals);
	} else {
		last_expr_result_ = one_r(t, vals[std::get<1>(state_)]);
	}
	LOG("znalezlizmy wartosc" LG(std::get<0>(state_), std::get<1>(state_), last_expr_result_));
}

void QueryInterpreter::visitEFun(EFun *efun)
{
	visitIdent(efun->ident_);
	std::string fun_name = last_ident_;
	efun->listexpr_->accept(this);
	last_expr_result_ = process_query_function(fun_name, last_expr_list_result_);
	LOG("processed_function" LG(last_expr_result_));
}

void QueryInterpreter::visitEStmt(EStmt *estmt)
{
	//	zagniezdzone procesujemy w pelnym srodowisku
	LOG("visitEStmt" LG(const_attr_map_));
	parse_state isolated_state = std::make_tuple(true, 0, const_attr_map_);
	parse_state saved_state = std::move(state_);	//zapisujemy stare srodowisko
	state_ = std::move(isolated_state);

	LOG("bedziemy przetwarzali w visitEStmt" LG(state_));
	estmt->stmt_->accept(this);
	LOG("visitEStmt" LG(last_stmt_result_.size()));
	if (last_stmt_result_.size() != 1) {
		throw std::runtime_error("wywolanie zagniezdzonego select'a zwrocilo niepoprawna liczbe wynikow");
	}
	last_expr_result_ = std::move(std::get<1>(last_stmt_result_.front()));

	state_ = std::move(saved_state);	//przywracamy stare srodowisko
}

void QueryInterpreter::visitEBraceEmpty(EBraceEmpty *ebraceempty)
{
	throw std::runtime_error("nie musimy obslugiwac EBraceEmpty");
}

void QueryInterpreter::visitEBracketExpr(EBracketExpr *ebracketexpr)
{
	throw std::runtime_error("nie musimy obslugiwac EBracketExpr");
	ebracketexpr->listexpr_->accept(this);
}

void QueryInterpreter::visitEFunStar(EFunStar *efunstar)
{
	throw std::runtime_error("nie musimy obslugiwac EFunStar");
	visitIdent(efunstar->ident_);
}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * RelOp * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryInterpreter::visitRelOp(RelOp*) {} //abstract class

void QueryInterpreter::visitLTH(LTH *)
{
	last_rel_op_ = RelOpType::LTH;
}

void QueryInterpreter::visitLE(LE *)
{
	last_rel_op_ = RelOpType::LE;
}

void QueryInterpreter::visitGTH(GTH *)
{
	last_rel_op_ = RelOpType::GTH;
}

void QueryInterpreter::visitGE(GE *)
{
	last_rel_op_ = RelOpType::GE;
}

void QueryInterpreter::visitEQU(EQU *)
{
	last_rel_op_ = RelOpType::EQU;
}

void QueryInterpreter::visitNE(NE *)
{
	last_rel_op_ = RelOpType::NE;
}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * AddOp * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryInterpreter::visitAddOp(AddOp*) {} //abstract class

void QueryInterpreter::visitPlusOp(PlusOp *)
{
	last_add_op_ = AddOpType::PlusOp;
}

void QueryInterpreter::visitMinusOp(MinusOp *)
{
	last_add_op_ = AddOpType::MinusOp;
}



/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * MulOp * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryInterpreter::visitMulOp(MulOp*) {} //abstract class

void QueryInterpreter::visitTimesOp(TimesOp *)
{
	last_mul_op_ = MulOpType::TimesOp;
}

void QueryInterpreter::visitDivideOp(DivideOp *)
{
	last_mul_op_ = MulOpType::DivideOp;
}

void QueryInterpreter::visitModuloOp(ModuloOp *)
{
	last_mul_op_ = MulOpType::ModuloOp;
}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * Values  * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryInterpreter::visitChar(Char x)
{
	LOG("parsed char " << x);
	throw std::runtime_error("nie powinnismy sparsowwac pojedynczego chara");
}

void QueryInterpreter::visitDouble(Double x)
{
	last_double_ = x;
}

void QueryInterpreter::visitIdent(Ident x)
{
	last_ident_ = x;
}

void QueryInterpreter::visitInteger(Integer x)
{
	last_integer_ = x;
}

void QueryInterpreter::visitString(String x)
{
	last_string_ = x;
}

} /* namespace cloud */
