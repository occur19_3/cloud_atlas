#include "expr_result.h"

namespace cloud {
namespace detail {

struct print_boost_variant_expr: public boost::static_visitor<void>
{
	std::ostream &os;
	print_boost_variant_expr(std::ostream &os): os(os) {}
	template<typename T>
	void operator() (const T &v) const {
		os << v;
	}
};

}	/* namespace detail */
}	/* namespace cloud */

std::ostream & operator <<(std::ostream & os, const cloud::one_r &v)
{
	os << "one_r(" << v.res << ")";
	return os;
}

std::ostream & operator <<(std::ostream & os, const cloud::col_r &v)
{
	os << "col_r(" << v.res << ")";
	return os;
}

std::ostream & operator <<(std::ostream & os, const cloud::list_r &v)
{
	os << "list_r(" << v.res << ")";
	return os;
}

std::ostream & operator <<(std::ostream & os, const cloud::expr_result &v)
{
	boost::apply_visitor(cloud::detail::print_boost_variant_expr(os), v);
	return os;
}

