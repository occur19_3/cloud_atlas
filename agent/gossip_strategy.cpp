#include "gossip_strategy.h"

#include <cstdlib>

namespace cloud {


size_t RandomStrategy::getCurrentGossipLevel()
{
	return (::rand() % max_level_) + 1;
}

size_t RandomStrategyExp::getCurrentGossipLevel()
{
	size_t r = 0;
	for (; r < max_level_; ++r) {
		if (::rand() % max_level_ != 0) {
			return r + 1;
		}
	}
	return r;
}

size_t RoundRobinStrategy::getCurrentGossipLevel()
{
	return (current_++  % max_level_) + 1;
}

size_t RoundRobinStrategyExp::getCurrentGossipLevel()
{
	size_t i = 0;
	for (; i < max_level_; ++i) {
		++current_[i];
		if (current_[i] % max_level_ != 0) {
			return i + 1;
		}
	}
	return i;
}

}
