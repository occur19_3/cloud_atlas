#include "zone.h"

#include <sstream>

#include "Absyn.H"
#include "log_utils.h"
#include "interpreter.h"
#include "Parser.H"
#include "utils.h"

#undef  LOG_DEFAULT_CATEGORY
#define LOG_DEFAULT_CATEGORY PARSER

namespace cloud {


/* * * * * ZMI * * * * */

static const std::set<std::string> baseZMIAttributes {
	"level",
	"name",
	"owner",
	"timestamp",
	"contacts",
	"cardinality"
};

void ZMI::insertBaseAttribute(const Attribute &a) {
	if (baseZMIAttributes.find(a.name_) != baseZMIAttributes.end()) {
		insertIntoMap(attributes_, a.name_, a);
	} else {
		LOG(WARN, "insertBaseAttribute ktos probuje wstawic zwykly atrybut jako atrybut bazowy" LG(a));
	}
}

void ZMI::insertAttribute(const Attribute &a) {
	if (baseZMIAttributes.find(a.name_) == baseZMIAttributes.end()) {
		insertIntoMap(attributes_, a.name_, a);
	} else {
		LOG(WARN, "insertAttribute ktos probuje zmienic atrybut bazowy" LG(a));
	}
}

std::string ZMI::getName() const
{
	auto iter = attributes_.find("name");
	if (iter == attributes_.end()) {
		LOG(WARN, "nie mozemy znalezc swojego imienia" LG(attributes_));
		return "";
	}
	return get_value_wrapper<std::string>(iter->second.value_.val);
}

ZmiTimestamp ZMI::getTimestamp() const {
	return std::make_pair(getName(), last_update_);
}

std::set<contact> ZMI::getContacts() const
{
	auto iter = attributes_.find("contacts");
	if (iter == attributes_.end()) {
		LOG(ERROR, "nie ma atrybutu contacts");
		return std::set<contact>();
	}
	try {
		if (!iter->second.value_.is) {
			LOG("kontakty sa puste, to jest zle");
			return std::set<contact>();
		}
		auto vs = get_value_wrapper<set<value>>(iter->second.value_.val);
		std::set<contact> result;
		for (auto v: vs) {
			result.insert(get_value_wrapper<contact>(v));
		}
		return result;
	} catch (std::exception &e) {
		LOG(ERROR, "atrybut contacts nie jest zbiorem kontaktow" LG(e.what(), iter->second));
		return std::set<contact>();
	}
}

void ZMI::addContact(const contact &c)
{
	LOG("addContact" LG(c, attributes_));
	auto iter = attributes_.find("contacts");
	if (iter == attributes_.end()) {
		LOG(ERROR, "nie ma atrybutu contacts");
		insertBaseAttribute(Attribute("contacts", set_contact_vt, std::set<value>{ value(c) }));

	} else try {
		auto vs = get_value_wrapper<set<value>>(iter->second.value_.val);
		vs.insert(value(c));
		insertBaseAttribute(Attribute("contacts", set_contact_vt, vs));
	} catch (std::exception &e) {
		LOG(ERROR, "atrybut contacts nie jest zbiorem kontaktow" LG(e.what(), iter->second));
	}
	LOG("addContactAfter" LG(c, attributes_));
}

void ZMI::removeContact(const contact &c)
{
	LOG("removeContact" LG(c, attributes_));
	auto iter = attributes_.find("contacts");
	if (iter == attributes_.end()) {
		LOG(ERROR, "nie ma atrybutu contacts");
	} else try {
		auto vs = get_value_wrapper<set<value>>(iter->second.value_.val);
		vs.erase(value(c));
		insertBaseAttribute(Attribute("contacts", set_contact_vt, vs));
	} catch (std::exception &e) {
		LOG(ERROR, "atrybut contacts nie jest zbiorem kontaktow" LG(e.what(), iter->second));
	}
	LOG("removeContactAfter" LG(c, attributes_));
}

/* * * * * Zone * * * * */

const std::list<std::string> Zone::BASE_ZMI_QUERIES = std::list<std::string>{
	"&cardinality: SELECT sum(cardinality) AS cardinality"
};

const std::list<std::string> Zone::BASE_ZMI_THIS_LEVEL_QUERIES = std::list<std::string>{
	"&owner: SELECT min(owner) AS owner WHERE timestamp == min((SELECT timestamp))",
	"&timestamp: SELECT min(timestamp) AS timestamp",
	"&contacts: SELECT to_set(random(10, unfold(contacts))) AS contacts"
};

Zone* Zone::createStartZone(const std::string &owner, size_t level, time now, Zone* father, const zone_path &path)
{
	if (level >= path.size()) {
		return nullptr;
	}
	Zone* z = new Zone(father);
	z->child_ = createStartZone(owner, level + 1, now, z, path);
	z->zmi_.insertBaseAttribute(Attribute("level", integer_vt, integer(level)));
	if (level == 0) {
		z->zmi_.insertBaseAttribute(Attribute("name", string_vt, nullable_value(false, get_empty_value(string_vt))));
	} else {
		z->zmi_.insertBaseAttribute(Attribute("name", string_vt, path[level]));
	}
	z->zmi_.insertBaseAttribute(Attribute("owner", string_vt, owner));
	z->zmi_.insertBaseAttribute(Attribute("timestamp", time_vt, now));
	z->zmi_.insertBaseAttribute(Attribute("contacts", set_contact_vt, set<value>()));
	z->zmi_.insertBaseAttribute(Attribute("cardinality", integer_vt, integer(1)));
	z->zmi_.last_update_ = now;
	return z;
}

Zone::Zone(Zone* father): father_(father) {}

Zone::~Zone()
{
#ifdef TEST_INTERPRETERA
	for (auto c: childs_TEST_) {
		delete c;
	}
	childs_TEST_.clear();
#else
	delete child_;
#endif
}

size_t Zone::getDepth() const
{
	if (!child_) {
		return 0;
	}
	return 1 + child_->getDepth();
}

std::string Zone::getZoneName() const
{
	if (!father_){
		return "/";
	}
	return father_->getZoneNamePrefix() + zmi_.getName();
}

zone_path Zone::getZonePath() const
{
	zone_path path;
	if (!father_) {
		path.push_back("");
	} else {
		path.push_back(zmi_.getName());
	}
	if (child_) {
		zone_path child_path = child_->getZonePath();
		path.insert(path.end(), child_path.begin(), child_path.end());
	}
	return path;
}

bool Zone::installQueryInAllZones(const std::string &query)
{
	Program* p = pProgram( query.c_str() );
	if (!p) {
		LOG(ERROR, "lol xd czemu wgll przyjeslimy to query" LG(query));
		return false;
	}
	return installQueryInAllZones(p);
}

void Zone::installQueriesInAllZones(const std::list<std::string> &queries, bool baseZmi, bool this_level)
{
	std::list<Program*> queries_p;
	for (auto s: queries) {
		Program* p = pProgram( s.c_str() );
		if (!p) {
			LOG(ERROR, "lol xd czemu wgll przyjeslimy to query" LG(s));
		} else {
			queries_p.push_back(p);
		}
	}
	installQueriesInAllZones(queries_p, baseZmi, this_level);
}

void Zone::eraseNonBaseAtrrs()
{
	LOG("eraseNonBaseAtrrs" LG(zmi_));
	if (child_) {
		for (auto iter = zmi_.attributes_.begin(); iter != zmi_.attributes_.end();) {
			auto curr = iter++;
			if (baseZMIAttributes.find(curr->first) == baseZMIAttributes.end()) {
				zmi_.attributes_.erase(curr);
			}
		}
		LOG("eraseNonBaseAtrrs after" LG(zmi_));
		child_->eraseNonBaseAtrrs();
	}
}

void Zone::recomputeBaseZmi() {
	installQueriesInAllZones(BASE_ZMI_QUERIES, true);
	installQueriesInAllZones(BASE_ZMI_THIS_LEVEL_QUERIES, true, true);
}

void Zone::purge_old(size_t timeout_sec)
{
	LOG("purge_old" LG(sibling_zmi_));
	for (auto iter = sibling_zmi_.begin(); iter != sibling_zmi_.end();) {
		auto curr = iter++;
		if (time::now().v - curr->second.last_update_.v > static_cast<std::int64_t>(timeout_sec)) {
			LOG("purge_old erasing curr" LG(time::now(), time::now().v, curr->second.last_update_, curr->second.last_update_.v, static_cast<std::int64_t>(timeout_sec), curr->second));
			sibling_zmi_.erase(curr);
		}
	}
	LOG("purge_old after" LG(sibling_zmi_));
	if (child_) {
		child_->purge_old(timeout_sec);
	}
}

void Zone::fetcherAttributesUpdate(const std::list<Attribute> &attributes)
{
	if (child_) {
		child_->fetcherAttributesUpdate(attributes);
	} else {
		zmi_.last_update_ = time::now();
		for (auto a: attributes) {
			zmi_.insertAttribute(a);
		}
	}
}

ZmiFreshness Zone::getKeptZoneNames(size_t level) const
{
	std::list< std::pair<std::string, std::map<std::string, time>> > result;
	std::map<std::string, time> this_level_timestamps { zmi_.getTimestamp() };
	for (auto z: sibling_zmi_) {
		this_level_timestamps.insert(z.second.getTimestamp());
	}
	result.push_back(std::make_pair(zmi_.getName(), this_level_timestamps));
	if (child_ && level) {
		result.splice(result.end(), child_->getKeptZoneNames(level - 1));
	}
	return result;
}

std::pair<time, std::list<Attribute>> Zone::getZoneAttributes(const zone_path &zone)
{
	if (zone.empty() || !zone.front().empty()) {
		LOG(WARN, "nieprawidlowa sciezka" LG(zone));
		return std::make_pair(time(), std::list<Attribute>());
	}
	return getZoneAttributesHelper(zone, 0);
}

std::set<contact> Zone::getContacts(size_t level) const
{
	if (level == 0) {
		return zmi_.getContacts();
	}
	if (child_ == nullptr) {
		LOG(ERROR, "nie ma dziecka a powinno byc");
		return std::set<contact>();
	}
	return child_->getContacts(level - 1);
}

void Zone::setSiblingAttributes(const zone_path &path, const ZMI &new_sibling_zmi)
{
	/// Mozemy zainstalowac cokolwiek dopiero na 1 poziomie
	if (path.size() < 2 || !path.front().empty() || !child_) {
		LOG("setSiblingAttributes niefajna sciezka" LG(path, new_sibling_zmi));
		return;
	}
	LOG("setSiblingAttributes" LG(path, new_sibling_zmi));
	child_->setSiblingAttributesHelper(path, 1, new_sibling_zmi);
}

void Zone::addContact(size_t level, const contact &c)
{
	LOG("addContact" LG(level,c));
	zmi_.addContact(c);
	if (level > 0 && child_) {
		child_->addContact(level - 1, c);
	}
}

void Zone::removeContact(size_t level, const contact &c)
{
	LOG("removeContact" LG(level,c));
	zmi_.removeContact(c);
	if (level > 0 && child_) {
		child_->removeContact(level - 1, c);
	}
}

bool Zone::installQueryInAllZones(Program *p)
{
	bool result = true;
	if (child_ != nullptr) {
		LOG("probujemy zainstalowac u dziecka a potem u siebie");
		result = child_->installQueryInAllZones(p->clone()) && installQuery(p);
	} else {
		LOG("to koniec juz nic nie instalujemy");
	}
	delete p;
	return result;
}

void Zone::installQueriesInAllZones(const std::list<Program*> &queries, bool baseZmi, bool this_level)
{
	if (child_ != nullptr) {
		LOG("probujemy zainstalowac u dziecka a potem u siebie");
		std::list<Program*> new_queries;
		for (auto q: queries) {
			new_queries.push_back(q->clone());
		}
		child_->installQueriesInAllZones(new_queries, baseZmi, this_level);
		for (auto q: queries) {
			installQuery(q, baseZmi, this_level);
		}
	} else {
		LOG("to koniec juz nic nie instalujemy");
	}
	for (auto q: queries) {
		delete q;
	}
}

bool Zone::installQuery(Program *p, bool baseZmi, bool this_level)
{
	LOG("installing Query in" LG(baseZmi, this_level, getZoneName(), zmi_.attributes_.size(), zmi_.attributes_));
	bool result = true;
	std::vector<AttributesMap> interpreter_attributes = child_->getThisLevelAttributes();
	LOG("installing query" LG(interpreter_attributes));
	if (this_level) {
		LOG("installing this_level query" LG(interpreter_attributes));
		interpreter_attributes = getThisLevelAttributes();
	}
	LOG("installing query" LG(interpreter_attributes));
	cloud::QueryInterpreter interpreter(interpreter_attributes);
	QueryInterpretResult* res = interpreter.process_query(p);
	if (res) {
		for (auto v: res->attributes_) {
			Attribute new_attr(std::get<0>(v), std::get<1>(v), std::get<2>(v));
			if (baseZmi) {
				zmi_.insertBaseAttribute(new_attr);
			} else {
				zmi_.insertAttribute(new_attr);
			}
		}
		zmi_.last_update_ = time::now();		
		LOG("po instalacji query" LG(zmi_));
		result = true;
	} else {
		LOG("nie udalo sie ogarnac query");
		result = false;
	}
	delete res;
	return result;
}

std::pair<time, std::list<Attribute>> Zone::getZoneAttributesHelper(const zone_path &zone, size_t level)
{
	if (level > zone.size()) {
		LOG(ERROR, "robimy cos zle chcemy zbyt gleboka rzecz" LG(zone, level));
		return std::make_pair(time(), std::list<Attribute> ());
	}
	if (zone.size() - 1 == level) {
		std::string name = zone[level];
		time last_update;
		std::list<Attribute> attributes;
		if (zmi_.getName() == name) {
			LOG("getZoneAttributesHelper chodzi o mnie");
			last_update = zmi_.last_update_;
			for (auto a: zmi_.attributes_) {
				attributes.push_back(a.second);
			}
		} else {
			LOG("getZoneAttributesHelper chodzi o brata");
			auto iter = sibling_zmi_.find(name);
			if (iter == sibling_zmi_.end()) {
				LOG("nie mam takiego brat #bsnt" LG(name, zone));
				return std::make_pair(time(), std::list<Attribute> ());
			}
			last_update = iter->second.last_update_;
			for (auto a: iter->second.attributes_) {
				attributes.push_back(a.second);
			}
		}
		LOG("getZoneAttributesHelper" LG(zone, level, last_update, attributes));
		return std::make_pair(last_update, attributes);
	}
	if (!child_) {
		LOG(ERROR, "robimy cos zle, bo juz nie mamy dziecka a chcemy" LG(zone));
		return std::make_pair(time(), std::list<Attribute> ());
	}
	if (zmi_.getName() != zone[level]) {
		LOG(ERROR, "robimy cos zle, bo sciezka nazw stref sie nie zgadza" LG(zone));
		return std::make_pair(time(), std::list<Attribute> ());
	}
	return child_->getZoneAttributesHelper(zone, level + 1);
}

void Zone::setSiblingAttributesHelper(const zone_path &path, size_t level, const ZMI &new_sibling_zmi)
{
	LOG("setSiblingAttributesHelper" LG(path, level, new_sibling_zmi));
	if (level > path.size()) {
		LOG(ERROR, "robimy cos zle chcemy zbyt gleboka rzecz" LG(path, level));
		return;
	}
	if (path.size() - 1 == level) {
		std::string name = path[level];
		if (zmi_.getName() == name) {
			LOG("getZoneAttributesHelper chodzi o mnie, co nie powinno miec miejsca, bo ja sam sobie obliczam atrybuty");
			return;
		}

		auto iter = sibling_zmi_.find(name);
		if (iter == sibling_zmi_.end()) {
			LOG("wstawiamy nowa strefe" LG(name, level, new_sibling_zmi));
			sibling_zmi_.insert(std::make_pair(name, new_sibling_zmi));
			return;
		}
		if (iter->second.last_update_ < new_sibling_zmi.last_update_) {
			LOG("podmieniamy na nowa strefe" LG(name, level, iter->second, new_sibling_zmi));
			insertIntoMap(sibling_zmi_, name, new_sibling_zmi);
			return;
		}
		return;
	}
	if (!child_) {
		LOG(ERROR, "robimy cos zle, bo juz nie mamy dziecka a chcemy" LG(level, path));
		return;
	}
	if (zmi_.getName() != path[level]) {
		LOG(ERROR, "robimy cos zle, bo sciezka nazw stref sie nie zgadza" LG(level, path, zmi_.getName(), path[level]));
		return;
	}
	child_->setSiblingAttributesHelper(path, level + 1, new_sibling_zmi);
}

std::string Zone::getZoneNamePrefix() const
{
	return std::string("/") + getZoneNameHelper();
}

std::string Zone::getZoneNameHelper() const
{
	if (!father_){
		return "";
	}
	return father_->getZoneNameHelper() + zmi_.getName() + "/";
}

std::vector<AttributesMap> Zone::getThisLevelAttributes() const
{
	std::vector<AttributesMap> result{zmi_.attributes_};
	for (auto zmi: sibling_zmi_) {
		result.push_back(zmi.second.attributes_);
	}
	return result;
}

#ifdef TEST_INTERPRETERA

std::string Zone::installQueryInAllZones_TEST(Program *p)
{
	LOG("probujemy zainstalowac u dzieci jesli je mamy" LG(childs_TEST_.size()));
	std::stringstream ss;
	for (auto c: childs_TEST_) {
		ss << c->installQueryInAllZones_TEST(p->clone());
	}
	if (childs_TEST_.size() > 0) {
		LOG("instalujemy u siebie");
		ss <<installQuery_TEST(p);
	}
	delete p;
	return ss.str();
}

void Zone::addChild_TEST(Zone *c)
{
	childs_TEST_.push_back(c);
}

std::vector<AttributesMap> Zone::getChildsAttributes_TEST() const
{
	std::vector<AttributesMap> result;
	for (auto c: childs_TEST_) {
		result.push_back(c->attributes_);
	}
	return result;
}

std::string Zone::installQuery_TEST(Program *p)
{
	LOG("installing Query in" LG(getZoneName(), attributes_.size(), attributes_));
	std::stringstream ss;
	cloud::QueryInterpreter interpreter(getChildsAttributes_TEST());
	QueryInterpretResult* res =  interpreter.process_query(p);
	if (res) {
		LOG("Instalujemy atrybuty" LG(res->attributes_, attributes_));
		for (auto v: res->attributes_) {
			Attribute new_attr(std::get<0>(v), std::get<1>(v), std::get<2>(v));
			auto iter = attributes_.find(new_attr.name_);
			if (iter == attributes_.end()) {
				insertIntoMap(attributes_, new_attr.name_, new_attr);
			} else {
				iter->second = new_attr;
			}
			ss << getZoneName() << ": " << new_attr.name_ << ": " << new_attr.value_ << std::endl;
		}
		LOG("Po zainstalowaniu atrybutow" LG(attributes_));
	} else {
		LOG("Nie udalo sie przetworzyc query");
	}
	delete res;
	return ss.str();
}

template<typename T>
std::pair<std::string, Attribute> attr_pair(const std::string &name, const value_type &t, const T &v)
{
	return std::make_pair(name, Attribute(name, t, v));
}

Zone *getTestRootZone_TEST()
{
	AttributesMap uw_khaki13_a {
		//BaseZMI
		attr_pair("level", integer_vt, integer(2)),
		attr_pair("name", string_vt, string("khaki13")),
		attr_pair("owner", string_vt, string("/uw/khaki13")),
		attr_pair("timestamp", time_vt, time::from_string_throw("2012/11/09 21:03:00.000")),
		attr_pair("contacts", set_contact_vt, set<value>{ contact(localhost, 2222), contact(localhost, 1111) }),
		attr_pair("cardinality", integer_vt, integer(1)),

		attr_pair("members", set_contact_vt, set<value>{}),
		attr_pair("creation", time_vt, nullable_type<value>(false, time())),
		attr_pair("cpu_usage", double_vt, 0.1),
		attr_pair("num_cores", integer_vt, nullable_type<value>(false, integer(0))),
		attr_pair("has_ups", bool_vt, true),
		attr_pair("some_names", list_string_vt, list<value>{}),
		attr_pair("expiry", duration_vt, nullable_type<value>(false, duration()))
	};

	AttributesMap uw_khaki31_a {
		//BaseZMI
		attr_pair("level", integer_vt, integer(2)),
		attr_pair("name", string_vt, string("khaki31")),
		attr_pair("owner", string_vt, string("/uw/khaki13")),
		attr_pair("timestamp", time_vt, time::from_string_throw("2012/11/09 20:03:00.000")),
		attr_pair("contacts", set_contact_vt, set<value>{ contact(localhost, 2222) }),
		attr_pair("cardinality", integer_vt, integer(1)),

		attr_pair("members", set_contact_vt, set<value>{ contact(localhost, 2222) }),
		attr_pair("creation", time_vt, time::from_string_throw("2011/11/09 20:12:13.123")),
		attr_pair("cpu_usage", double_vt, nullable_type<value>(false, 0.0)),
		attr_pair("num_cores", integer_vt,  integer(3)),
		attr_pair("has_ups", bool_vt, false),
		attr_pair("some_names", list_string_vt, list<value>{string("agatka"), string("beatka"), string("celina")}),
		attr_pair("expiry", duration_vt, duration::from_string_throw("-13 11:00:00.000"))
	};

	AttributesMap uw_violet07_a {
		//BaseZMI
		attr_pair("level", integer_vt, integer(2)),
		attr_pair("name", string_vt, string("violet07")),
		attr_pair("owner", string_vt, string("/uw/violet07")),
		attr_pair("timestamp", time_vt, time::from_string_throw("2012/11/09 18:00:00.000")),
		attr_pair("contacts", set_contact_vt, set<value>{ contact(localhost, 1111), contact(localhost, 2222), contact(localhost, 3333) }),
		attr_pair("cardinality", integer_vt, integer(1)),

		attr_pair("members", set_contact_vt, set<value>{ contact(localhost, 1111) }),
		attr_pair("creation", time_vt, time::from_string_throw("2011/11/09 20:8:13.123")),
		attr_pair("cpu_usage", double_vt, 0.9),
		attr_pair("num_cores", integer_vt, integer(3)),
		attr_pair("has_ups", bool_vt, nullable_type<value>(false, false)),
		attr_pair("some_names", list_string_vt, list<value>{string("tola"), string("tosia")}),
		attr_pair("expiry", duration_vt, duration::from_string_throw("+13 12:00:00.000"))
	};

	AttributesMap uw_a {
		//BaseZMI
		attr_pair("level", integer_vt, integer(1)),
		attr_pair("name", string_vt, string("uw")),
		attr_pair("owner", string_vt, string("/uw/violet07")),
		attr_pair("timestamp", time_vt, time::from_string_throw("2012/11/09 20:08:13.123")),
		attr_pair("contacts", set_contact_vt, set<value>{}),
		attr_pair("cardinality", integer_vt,  integer(0))
	};

	AttributesMap pjwstk_whatever01_a {
		//BaseZMI
		attr_pair("level", integer_vt, integer(2)),
		attr_pair("name", string_vt, string("whatever01")),
		attr_pair("owner", string_vt, string("/pjwstk/whatever01")),
		attr_pair("timestamp", time_vt, time::from_string_throw("2012/11/09 21:12:00.000")),
		attr_pair("contacts", set_contact_vt, set<value>{ contact(localhost, 8800) }),
		attr_pair("cardinality", integer_vt, integer(1)),

		attr_pair("members", set_contact_vt, set<value>{ contact(localhost, 8800) }),
		attr_pair("creation", time_vt, time::from_string_throw("2012/10/18 07:03:00.000")),
		attr_pair("cpu_usage", double_vt, 0.1),
		attr_pair("num_cores", integer_vt, integer(7)),
		attr_pair("php_modules", list_string_vt, list<value>{string("rewrite")})
	};

	AttributesMap pjwstk_whatever02_a {
		//BaseZMI
		attr_pair("level", integer_vt, integer(2)),
		attr_pair("name", string_vt, string("whatever02")),
		attr_pair("owner", string_vt, string("/pjwstk/whatever02")),
		attr_pair("timestamp", time_vt, time::from_string_throw("2012/11/09 21:13:00.000")),
		attr_pair("contacts", set_contact_vt, std::set<value>{ contact(localhost, 9900) }),
		attr_pair("cardinality", integer_vt, integer(1)),

		attr_pair("members", set_contact_vt, set<value>{ contact(localhost, 9900) }),
		attr_pair("creation", time_vt, time::from_string_throw("2012/10/18 07:04:00.000")),
		attr_pair("cpu_usage", double_vt, 0.4),
		attr_pair("num_cores", integer_vt, integer(13)),
		attr_pair("php_modules", list_string_vt, list<value>{string("odbc")})
	};

	AttributesMap pjwstk_a {
		//BaseZMI
		attr_pair("level", integer_vt, integer(1)),
		attr_pair("name", string_vt, string("pjwstk")),
		attr_pair("owner", string_vt, string("/pjwstk/whatever01")),
		attr_pair("timestamp", time_vt, time::from_string_throw("2012/11/09 20:08:13.123")),
		attr_pair("contacts", set_contact_vt, std::set<value>{}),
		attr_pair("cardinality", integer_vt, integer(0))
	};

	AttributesMap root_a {
		//BaseZMI
		attr_pair("level", integer_vt, integer(1)),
		attr_pair("name", string_vt, nullable_type<value>(false, std::string())),
		attr_pair("owner", string_vt, string("/uw/violet07")),
		attr_pair("timestamp", time_vt, time::from_string_throw("2012/11/09 20:10:17.342")),
		attr_pair("contacts", set_contact_vt, std::set<value>{}),
		attr_pair("cardinality", integer_vt, integer(0))
	};

	LOG(TRACE, "jak wyglada wypisanie mapy" LG(uw_a, pjwstk_whatever02_a));
	Zone *uw_khaki13, *uw_khaki31, *uw_violet07, *pjwstk_whatever01, *pjwstk_whatever02, *uw, *pjwstk, *root;

	uw_khaki13 = new Zone;
	uw_khaki13->attributes_ = uw_khaki13_a;

	uw_khaki31 = new Zone;
	uw_khaki31->attributes_ = uw_khaki31_a;

	uw_violet07 = new Zone;
	uw_violet07->attributes_ = uw_violet07_a;


	uw = new Zone(uw_khaki13);
	uw->attributes_ = uw_a;
	uw->addChild_TEST(uw_khaki13);
	uw->addChild_TEST(uw_khaki31);
	uw->addChild_TEST(uw_violet07);
	uw_khaki13->father_ = uw;
	uw_khaki31->father_ = uw;
	uw_violet07->father_ = uw;


	pjwstk_whatever01 = new Zone;
	pjwstk_whatever01->attributes_ = pjwstk_whatever01_a;

	pjwstk_whatever02 = new Zone;
	pjwstk_whatever02->attributes_ = pjwstk_whatever02_a;

	pjwstk = new Zone(pjwstk_whatever01);
	pjwstk->attributes_ = pjwstk_a;
	pjwstk->addChild_TEST(pjwstk_whatever01);
	pjwstk->addChild_TEST(pjwstk_whatever02);
	pjwstk_whatever01->father_ = pjwstk;
	pjwstk_whatever02->father_ = pjwstk;

	root = new Zone(uw);
	root->attributes_ = root_a;
	root->addChild_TEST(uw);
	root->addChild_TEST(pjwstk);
	uw->father_ = root;
	pjwstk->father_ = root;
	return root;
}

#endif

} /* namespace cloud */


std::ostream & operator<<(std::ostream &os, const cloud::ZMI &z)
{
	os << "ZMI[" << z.last_update_ << "," << z.attributes_ << "]";
	return os;
}

std::ostream & operator<<(std::ostream &os, const cloud::Zone &z)
{	
	os << z.getZoneName() << std::endl;
	for (auto a: z.zmi_.attributes_) {
		os << "    " << a.second << std::endl;
	}
#ifndef TEST_INTERPRETERA
	std::string prefix = z.getZoneNamePrefix();
	for (auto s_zmi: z.sibling_zmi_) {
		os << prefix << s_zmi.first << std::endl;
		for (auto a: s_zmi.second.attributes_) {
			os << "    " << a.second << std::endl;
		}
	}
	if (z.child_) {
		os << z.child_;
	}
#else
	for (auto c: z.childs_TEST_) {
		os << *c;
	}
#endif
	return os;
}
