#include "agent.h"

#include <boost/asio/placeholders.hpp>
#include <boost/bind/bind.hpp>
#include <boost/make_shared.hpp>

#include "gossip_strategy.h"
#include "log_utils.h"
#include "interpreter.h"
#include "utils.h"
#include "zone.h"

#undef  LOG_DEFAULT_CATEGORY
#define LOG_DEFAULT_CATEGORY AGENT

namespace cloud {

GossipStrategyType parseGossipStrategy(const std::string &str);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * GossipTransaction * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * */


void GossipTransaction::calculateTimeDiff()
{
	LOG("calculateTimeDiff" LG(my_rcv_timestamp, my_snd_timestamp, their_snd_timestamp, their_rcv_timestamp));
	std::int64_t rtt = (my_rcv_timestamp - my_snd_timestamp) - (their_snd_timestamp - their_rcv_timestamp);
	rtt /= 2;
	time_diff_ = my_snd_timestamp + rtt - their_rcv_timestamp;
	LOG("calculateTimeDiff after" LG(rtt, time_diff_, my_rcv_timestamp, my_snd_timestamp, their_snd_timestamp, their_rcv_timestamp));
}

void GossipTransaction::setRequestedZones(const ZmiFreshness &my_freshness, const ZmiFreshness &their_freshness)
{
	if (my_freshness.size() < 2 || their_freshness.size() < 2 || level_ == 0) {
		LOG(ERROR, "musimy przynajmniej na poziomie 1 plotkowac" LG(level_, my_freshness, their_freshness));
		return;
	}
#ifndef PRODUCTION
	if (level_ > my_freshness.size() || level_ > their_freshness.size()) {
		LOG(WARN, "poziom plotkoeania za duzy" LG(level_, my_freshness, their_freshness));
	}
#endif

	auto make_vec = [] (const zone_path &zp, const std::string name) {
		zone_path r(zp.begin(), zp.end());
		r.push_back(name);
		return r;
	};

	requested_zones_.clear();
	zone_path prefix{""};
	auto my_iter = ++my_freshness.begin();
	auto their_iter = ++their_freshness.begin();
	for (size_t i = 1; i <= level_ && my_iter != my_freshness.end() && their_iter != their_freshness.end(); ++i, ++my_iter, ++their_iter) {
		std::string my_name = my_iter->first;
		for (auto tz: their_iter->second) {
			if (tz.first != my_name) {
				auto find_iter = my_iter->second.find(tz.first);
				if (find_iter == my_iter->second.end() ||
						find_iter->second.v < tz.second.v + time_diff_ ) {
					LOG("pushing back" LG(prefix, tz.first, make_vec(prefix, tz.first)));
					requested_zones_.insert(make_vec(prefix, tz.first));
				}
			}
		}
		prefix.push_back(my_name);
	}
	LOG("after setting requeste zones" LG(requested_zones_, level_, my_freshness, their_freshness, *this));
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * Agent * * * * * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * */


Agent::Agent(boost::asio::io_service &io_serv):
	io_serv_(io_serv),
	network_(io_serv, [this] (NetworkManager::RcvData && p) { packet_arrived((std::move(p))); }),
	recompute_timer_(io_serv, boost::bind(&Agent::start_recompute, this, boost::asio::placeholders::error)),
	gossip_timer_(io_serv, boost::bind(&Agent::start_gossip, this, boost::asio::placeholders::error)),
	purge_timer_(io_serv, boost::bind(&Agent::start_purge, this, boost::asio::placeholders::error))
{}

Agent::~Agent()
{
	delete my_root_zone_;
}

bool Agent::start(const std::string &host, std::uint16_t port, const std::string &zone, const std::string &public_key_file, std::uint32_t recompute_period,
				  std::uint32_t gossip_period, const std::string &gossip_strategy, std::uint32_t purge_period, const std::string &contact_s)
{
	LOG("starting agent" LG(port, zone, public_key_file, recompute_period, gossip_period, gossip_strategy, purge_period, contact_s));

	zone_ = zone;

	if (!my_addr_.fromString(host, port)) {
		LOG(ERROR, "invalid host provided" LG(host));
		return false;
	}

	if (zone_.empty() || recompute_period == 0 || gossip_period == 0 || purge_period == 0) {
		LOG(ERROR, "invalid agent parameters provided" LG(zone, recompute_period, gossip_period, gossip_strategy, purge_period));
		return false;
	}

	fallback_contacts_ = contact::setFromString(contact_s);
	if (!contact_s.empty() && fallback_contacts_.empty()) {
		LOG(ERROR, "invalid fallback contacts provided" LG(contact_s));
		return false;
	}

	if (!network_.start(port)) {
		LOG(ERROR, "cannot start network" LG(port));
		return false;
	}

	if (!load_public_key(public_key_file)) {
		LOG(ERROR, "problem with loading query signer public key" LG(public_key_file));
		return false;
	}

	if (!initialize_zone_data(zone_)) {
		LOG(ERROR, "invalid zone name" LG(zone_));
		return false;
	}

	if (!set_gossip_strategy(gossip_strategy)) {
		LOG(ERROR, "invalid gossip strategy" LG(gossip_strategy));
		return false;
	}

	purge_timer_.rerun_time_ms_ = purge_period * 1000;
	purge_timer_.repeat();

	recompute_timer_.rerun_time_ms_ = recompute_period * 1000;
	recompute_timer_.repeat();

	gossip_timer_.rerun_time_ms_ = gossip_period * 1000;
	gossip_timer_.repeat();

	io_serv_.run();
	LOG("finishing");
	return true;
}

void Agent::stop()
{
	LOG("stopping");
	network_.stop();
	io_serv_.stop();
	LOG("stopped");
}

bool Agent::load_public_key(const std::string &public_key_file)
{
	return cloud::read_from_file(public_key_file, pk_);
}

bool Agent::initialize_zone_data(const std::string &zone_name)
{
	zone_path path = parseZoneName(zone_name);
	if (path.empty()) {
		return false;
	}
	my_root_zone_ = Zone::createStartZone(zone_name, 0, time::now(), nullptr, path);
	return my_root_zone_;
}

bool Agent::set_gossip_strategy(const std::string &gossip_strategy)
{
	switch (parseGossipStrategy(gossip_strategy)) {
	case GossipStrategyType::random:
		gossip_strategy_ = new RandomStrategy(my_root_zone_->getDepth());
		return true;
	case GossipStrategyType::random_exp:
		gossip_strategy_ = new RandomStrategyExp(my_root_zone_->getDepth());
		return true;
	case GossipStrategyType::round_robin:
		gossip_strategy_ = new RoundRobinStrategy(my_root_zone_->getDepth());
		return true;
	case GossipStrategyType::round_robin_exp:
		gossip_strategy_ = new RoundRobinStrategyExp(my_root_zone_->getDepth());
		return true;
	default:
		return false;
	}
}


/* * * * * * * * * * Processing Packets * * * * * * * * * */


void Agent::packet_arrived(NetworkManager::RcvData &&packet)
{
	LOG("packet_arrived " LG(packet));
	try {
		switch (packet.type) {
		case PacketType::FetcherData:
		{
			Packet_FetcherData p = deserializePacket<Packet_FetcherData>(packet.data);
			LOG("przetwarzam pakiet" LG(p));
			std::list<Attribute> attributes;
			for (auto a: p.attributes_) {
				attributes.push_back(convertToCloudAttribute(a));
			}
			processFetcherData(attributes);
		}
			break;

		case PacketType::SetFallbackContactsRequest:
		{
			Packet_SetFallbackContactsRequest p = deserializePacket<Packet_SetFallbackContactsRequest>(packet.data);
			LOG("przetwarzam pakiet" LG(p));
			processSetFallbackContactsRequest(packet, p.id_, p.contacts_);
		}
			break;

		case PacketType::GetKeptZoneNamesRequest:
		{
			Packet_GetKeptZoneNamesRequest p = deserializePacket<Packet_GetKeptZoneNamesRequest>(packet.data);
			LOG("przetwarzam pakiet" LG(p));
			processGetKeptZoneNamesRequest(packet, p.id_);
		}
			break;


		case PacketType::GetZoneAttributesRequest:
		{
			Packet_GetZoneAttributesRequest p = deserializePacket<Packet_GetZoneAttributesRequest>(packet.data);
			LOG("przetwarzam pakiet" LG(p));
			processGetZoneAttributesRequest(packet, p.id_, p.requested_zones_);
		}
			break;

		case PacketType::GetZoneAttributesResponse:
		{
			Packet_GetZoneAttributesResponse p = deserializePacket<Packet_GetZoneAttributesResponse>(packet.data);
			LOG("przetwarzam pakiet" LG(p));
			std::list<Attribute> attributes;
			for (auto a: p.attributes_) {
				attributes.push_back(convertToCloudAttribute(a));
			}
			processGetZoneAttributesResponse(packet, p.id_, p.zone_, p.timestamp_, attributes);
		}
			break;

		case PacketType::SetSingletonZoneAttributeRequest:
		{
			Packet_SetSingletonZoneAttributeRequest p = deserializePacket<Packet_SetSingletonZoneAttributeRequest>(packet.data);
			LOG("przetwarzam pakiet" LG(p));
			std::list<Attribute> attributes;
			for (auto a: p.attributes_) {
				attributes.push_back(convertToCloudAttribute(a));
			}
			processSetSingletonZoneAttributeRequest(packet, p.id_, attributes);
		}
			break;

		case PacketType::InstallQueryRequest:
		{
			Packet_InstallQueryRequest p = deserializePacket<Packet_InstallQueryRequest>(packet.data);
			LOG("przetwarzam pakiet" LG(p));
			processInstallQueryRequest(packet, p.id_, p.query_);
		}
			break;

		case PacketType::StartGossip:
		{
			Packet_StartGossip p = deserializePacket<Packet_StartGossip>(packet.data);
			LOG("przetwarzam pakiet" LG(p));
			processStartGossip(packet, p.id_, p.zones_, p.queries_);
		}
			break;

		case PacketType::GossipReply:
		{
			Packet_GossipReply p = deserializePacket<Packet_GossipReply>(packet.data);
			LOG("przetwarzam pakiet" LG(p));
			processGossipReply(packet, p.id_, p.last_my_rcv_timestamp_, p.last_their_snd_timestamp_, p.zones_, p.queries_);
		}
			break;

		case PacketType::GossipZoneAttributesRequest:
		{
			Packet_GossipZoneAttributesRequest p = deserializePacket<Packet_GossipZoneAttributesRequest>(packet.data);
			LOG("przetwarzam pakiet" LG(p));
			processGossipZoneAttributesRequest(packet, p.id_, p.last_my_rcv_timestamp_, p.last_their_snd_timestamp_, p.requested_zones_);
		}
			break;

		default:
			LOG("unknown packet" LG(packet));
			break;
		}
	} catch (std::exception &e) {
		LOG(WARN, "packet_processing error" LG(e.what()));
	}

	delete[] packet.data.data_;
}

void Agent::processFetcherData(const std::list<Attribute> &attributes)
{
	my_root_zone_->fetcherAttributesUpdate(attributes);
}

void Agent::processSetFallbackContactsRequest(const NetworkManager::RcvData &rcv_data, std::int64_t id, const std::set<contact> &new_contacts)
{
	LOG("processSetFallbackContactsRequest" LG(id, new_contacts, fallback_contacts_));
	fallback_contacts_ = new_contacts;
	Packet_SetFallbackContactsResponse res(id, true);
	PacketData pd = serializePacketSetTypeAndAddTimeStampOffset(res);
	LOG("processSetFallbackContactsRequest sending response" LG(rcv_data, res));
	network_.push_to_udp_send_queue(rcv_data.c, std::move(pd));
}

void Agent::processGetKeptZoneNamesRequest(const NetworkManager::RcvData &rcv_data, std::int64_t id)
{
	LOG("processGetKeptZoneNamesRequest" LG(id));
	Packet_GetKeptZoneNamesResponse res(id, my_root_zone_->getKeptZoneNames(my_root_zone_->getDepth()), getQueryWrappers());
	PacketData pd = serializePacketSetTypeAndAddTimeStampOffset(res);
	LOG("processGetKeptZoneNamesRequest sending response" LG(rcv_data, res));
	network_.push_to_udp_send_queue(rcv_data.c, std::move(pd));
}


void Agent::processGetZoneAttributesRequest(const NetworkManager::RcvData &rcv_data, std::int64_t id, const std::set<zone_path> &requested_zones)
{
	LOG("processGetZoneAttributesRequest" LG(id, requested_zones));
	for (auto zone: requested_zones) {
		auto zone_attrs = my_root_zone_->getZoneAttributes(zone);
		std::list<serialization::Attribute> attrs;
		std::transform(zone_attrs.second.begin(), zone_attrs.second.end(), std::back_inserter(attrs), [] (const Attribute &a) { return convertToSerializableAttribute(a); });
		Packet_GetZoneAttributesResponse res(id, zone, zone_attrs.first, attrs);
		PacketData pd = serializePacketSetTypeAndAddTimeStampOffset(res);
		LOG("processGetZoneAttributesRequest sending response" LG(zone, rcv_data, res));
		network_.push_to_udp_send_queue(rcv_data.c, std::move(pd));
	}
}

void Agent::processGetZoneAttributesResponse(const NetworkManager::RcvData &rcv_data, std::int64_t id, const zone_path &zone, time timestamp, const std::list<Attribute> &attributes)
{
	LOG("processGetZoneAttributesResponse" LG(id, zone, timestamp, attributes));
	auto iter = gossip_transactions_.find(id);
	if (iter != gossip_transactions_.end()) {
		GossipTransaction &gtr = iter->second;
		gtr.requested_zones_.erase(zone);
		gtr.retries = 0;
		ZMI zmi;
		zmi.last_update_ = time(timestamp.v + gtr.time_diff_);
		for (auto a: attributes) {
			insertIntoMap(zmi.attributes_, a.name_, a);
		}
		my_root_zone_->setSiblingAttributes(zone, zmi);
		if (gtr.requested_zones_.empty()) {
			gossip_transactions_.erase(iter);
		}
	}
}

void Agent::processSetSingletonZoneAttributeRequest(const NetworkManager::RcvData &rcv_data, std::int64_t id, const std::list<Attribute> &attributes)
{
	LOG("processSetSingletonZoneAttributeRequest" LG(id, attributes));
	my_root_zone_->fetcherAttributesUpdate(attributes);
	Packet_SetSingletonZoneAttributeResponse res(id, true);
	PacketData pd = serializePacketSetTypeAndAddTimeStampOffset(res);
	LOG("processSetSingletonZoneAttributeRequest sending response" LG(rcv_data, res));
	network_.push_to_udp_send_queue(rcv_data.c, std::move(pd));
}

void Agent::processInstallQueryRequest(const NetworkManager::RcvData &rcv_data, std::int64_t id, const serialization::query_wrapper &query)
{
	LOG("processInstallQueryRequest" LG(id, query));
	bool result = tryInsertQuery(query);
	Packet_InstallQueryResponse res(id, result);
	PacketData pd = serializePacketSetTypeAndAddTimeStampOffset(res);
	LOG("processInstallQueryRequest sending response" LG(rcv_data, res));
	network_.push_to_udp_send_queue(rcv_data.c, std::move(pd));
}

void Agent::processStartGossip(const NetworkManager::RcvData &rcv_data, std::int64_t id, const ZmiFreshness &zones, const std::list<serialization::query_wrapper> &queries)
{
	LOG("processStartGossip" LG(id, zones, queries));
	auto iter = gossip_transactions_.find(id);
	if (iter != gossip_transactions_.end()) {
		//jesli dostalismy ten pakiet, a mamy transakcje, to jest to prośba o retransmisje GossipReply
		LOG("processStartGossip Id found resending GossipReply" LG(id));
		sendGossipReply(rcv_data, iter->second);
		return;
	}
	zone_path sender_path = getZonePath(zones);
	size_t gossip_level = get_eq_level(my_root_zone_->getZonePath(), sender_path);
	if (gossip_level < 1) {
		LOG("nie mamy jak za brdzo plotkowac, bo wyliczylismy zly poziom" LG(id, gossip_level, my_root_zone_->getZonePath(), sender_path));
		return;
	}

	updateQueries(queries);
	GossipTransaction gtr;
	gtr.state_ = GossipTransaction::state::RECEIVED_START_R;
	gtr.id_ = id;
	gtr.level_ = gossip_level;
	gtr.address_ = rcv_data.c;
	gtr.their_freshness_ = zones;
	gtr.rcv_data_my_rcv_timestamp = rcv_data.my_rcv_timestamp;
	gtr.rcv_data_their_snd_timestamp = rcv_data.their_snd_timestamp;
	my_root_zone_->addContact(gtr.level_, gtr.address_);
	sendGossipReply(rcv_data, gtr);	
	gtr.timer_ = boost::make_shared<RepeatTimer>(io_serv_, [this, id] (boost::system::error_code ec) {
		gossipTimeout(ec, id);
	});
	gtr.timer_->rerun_time_ms_ = 1000;
	LOG("timer timeout" LG(gtr.timer_->rerun_time_ms_));
	gtr.timer_->repeat();
	insertIntoMap(gossip_transactions_, gtr.id_, std::move(gtr));
}

void Agent::processGossipReply(const NetworkManager::RcvData &rcv_data, std::int64_t id, std::int64_t last_my_rcv_timestamp, std::int64_t last_their_snd_timestamp,
						const ZmiFreshness &zones, const std::list<serialization::query_wrapper> &queries)
{
	LOG("processGossipReply" LG(id, last_my_rcv_timestamp, last_their_snd_timestamp, zones, queries));
	auto iter = gossip_transactions_.find(id);
	if (iter == gossip_transactions_.end()) {
		LOG("nie mamy tej transakcji u siebie, jest to retransmisja, ale my nic nie potrzebujemy juz, wiec wykonujemy retransmijse bez zadan" LG(id));
		GossipTransaction gtr;
		gtr.id_ = id;
		sendGossipZoneAttributesRequest(rcv_data, gtr);
		return;
	}
	GossipTransaction &gtr = iter->second;
	switch (gtr.state_) {
	case GossipTransaction::CONNECTING_I:
	{
		LOG("dostalismy processGossipReply w stanie CONNECTING_I" LG(gtr));
		zone_path sender_path = getZonePath(zones);
		size_t gossip_level = get_eq_level(my_root_zone_->getZonePath(), sender_path);
		if (gossip_level < 1) {
			LOG(WARN, "ktos plotkuje z nami na zlym poziomie" LG(my_root_zone_->getZonePath(), sender_path, gtr));
			return;
		}
		gtr.timer_->stop();
		gtr.retries = 0;
		gtr.state_ = GossipTransaction::state::WAITING_FOR_ATTRIBUTES;
		gtr.level_ = gossip_level;
		my_root_zone_->addContact(gtr.level_, gtr.address_);
		updateQueries(queries);

		gtr.my_rcv_timestamp = rcv_data.my_rcv_timestamp;
		gtr.their_snd_timestamp = rcv_data.their_snd_timestamp;
		gtr.my_snd_timestamp = last_their_snd_timestamp;
		gtr.their_rcv_timestamp = last_my_rcv_timestamp;
		gtr.calculateTimeDiff();
		gtr.setRequestedZones(my_root_zone_->getKeptZoneNames(gtr.level_), zones);

		gtr.rcv_data_my_rcv_timestamp = rcv_data.my_rcv_timestamp;
		gtr.rcv_data_their_snd_timestamp = rcv_data.their_snd_timestamp;
		LOG("timer timeout" LG(gtr.timer_->rerun_time_ms_));
		gtr.timer_->repeat();
		sendGossipZoneAttributesRequest(rcv_data, gtr);
		if (gtr.requested_zones_.size() == 0) {
			gossip_transactions_.erase(iter);
		}
	}
		break;
	case GossipTransaction::RECEIVED_START_R:
		LOG("Nie powinnismy dostac pakietu processGossipReply" LG(gtr));
		break;

	case GossipTransaction::WAITING_FOR_ATTRIBUTES:
		LOG("Juz dostalismy ten pakiet, wiec jest to rposba o retransmisje naszej prosby:)" LG(gtr));
		sendGossipZoneAttributesRequest(rcv_data, gtr);
		break;
	default:
		LOG(WARN, "tu nie powinnismy byc ");
		break;
	}
}

void Agent::processGossipZoneAttributesRequest(const NetworkManager::RcvData &rcv_data, std::int64_t id,  std::int64_t last_my_rcv_timestamp, std::int64_t last_their_snd_timestamp, const std::set<zone_path> &requested_zone)
{
	LOG("processGossipZoneAttributesRequest" LG(id, requested_zone));
	auto iter = gossip_transactions_.find(id);
	if (iter != gossip_transactions_.end()) {
		GossipTransaction &gtr = iter->second;
		LOG("przetwarzamy" LG(gtr));
		if (gtr.state_ == GossipTransaction::state::RECEIVED_START_R) {
			LOG("wszysyko na gicie juz przetwarzamy");
			gtr.timer_->stop();
			gtr.retries = 0;
			gtr.state_ = GossipTransaction::state::WAITING_FOR_ATTRIBUTES;
			gtr.my_rcv_timestamp = rcv_data.my_rcv_timestamp;
			gtr.their_snd_timestamp = rcv_data.their_snd_timestamp;
			gtr.my_snd_timestamp = last_their_snd_timestamp;
			gtr.their_rcv_timestamp = last_my_rcv_timestamp;
			gtr.calculateTimeDiff();
			gtr.setRequestedZones(my_root_zone_->getKeptZoneNames(gtr.level_), gtr.their_freshness_);
			if (gtr.requested_zones_.size() > 0) {
				LOG("timer timeout" LG(gtr.timer_->rerun_time_ms_));
				gtr.timer_->repeat();
				sendGetZoneAttributesRequest(gtr);
			} else {
				gossip_transactions_.erase(iter);
			}
		}
	} else {
		LOG("nie znalezlismy tej transakcji" LG(id, gossip_transactions_));
	}
	processGetZoneAttributesRequest(rcv_data, id, requested_zone);
}



/* * * * * * * * * * Timer Handlers * * * * * * * * * */


void Agent::start_recompute(boost::system::error_code ec)
{
	LOG("start_recompute" LG(ec.message()));
	if (!ec) {
		recompute_queries();
		recompute_timer_.repeat();
	}
}

void Agent::start_gossip(boost::system::error_code ec)
{
	LOG("****************************" LG(ec.message()));
	LOG("******* start_gossip *******" LG(ec.message()));
	LOG("****************************" LG(ec.message()));
	if (!ec) {
		try {
			size_t level = gossip_strategy_->getCurrentGossipLevel();
			std::set<contact> gossip_contacts = my_root_zone_->getContacts(level);
			if (gossip_contacts.empty()) {
				level = my_root_zone_->getDepth();
				gossip_contacts = fallback_contacts_;
			}
			LOG("selected contacts" LG(level, gossip_contacts, fallback_contacts_, my_root_zone_->getDepth()));
			if (!gossip_contacts.empty()) {
				LOG("plotkujemy");
				auto id = getRandomId();
				GossipTransaction gtr;
				gtr.state_ = GossipTransaction::state::CONNECTING_I;
				gtr.id_ = id;

				gtr.level_ = level;
				gtr.address_ = getRandomFromSet(gossip_contacts);
				sendStartGossip(gtr);
				gtr.timer_ = boost::make_shared<RepeatTimer>(io_serv_, [this, id] (boost::system::error_code ec) {
					gossipTimeout(ec, id);
				});
				gtr.timer_->rerun_time_ms_ = 1000;
				LOG("timer timeout" LG(gtr.timer_->rerun_time_ms_));
				gtr.timer_->repeat();
				insertIntoMap(gossip_transactions_, gtr.id_, std::move(gtr));
			} else  {
				LOG(WARN, "Nie ammy z kim plotkowac");
			}

		} catch (std::exception &e) {
			LOG("start_gossip error" LG(e.what()));
		}
		gossip_timer_.repeat();	//z miejsca rozpoczynamy oczekiwanie na kolejna runde, moze byc tak, ze mamy kilka swoich plotkowan uruchomionych
	}
}

void Agent::start_purge(boost::system::error_code ec)
{
	LOG("start_purge" LG(ec.message()));
	if (!ec) {
		my_root_zone_->purge_old(purge_timer_.rerun_time_ms_);
		for (auto iter = queries_.begin(); iter != queries_.end();) {
			auto curr = iter++;
			if (curr->second.validity_ < time::now()) {
				queries_.erase(curr);
			}
		}
		purge_timer_.repeat();
	}
}


/* * * * * * * * * * Gossip Functions * * * * * * * * * */


void Agent::sendStartGossip(const GossipTransaction &gtr)
{
	LOG("sendStartGossip" LG(gtr));
	Packet_StartGossip req(gtr.id_, my_root_zone_->getKeptZoneNames(gtr.level_), getQueryWrappers());
	PacketData pd = serializePacketSetTypeAndAddTimeStampOffset(req);
	LOG("sendStartGossip sending packet" LG(req, gtr));
	network_.push_to_udp_send_queue(gtr.address_, std::move(pd));
}

void Agent::sendGossipReply(const NetworkManager::RcvData &rcv_data, const GossipTransaction &gtr)
{
	LOG("sendGossipReply" LG(gtr));
	Packet_GossipReply res(gtr.id_, rcv_data.my_rcv_timestamp, rcv_data.their_snd_timestamp, my_root_zone_->getKeptZoneNames(gtr.level_), getQueryWrappers());
	PacketData pd = serializePacketSetTypeAndAddTimeStampOffset(res);
	LOG("sendGossipReply sending packet" LG(rcv_data, res, gtr));
	network_.push_to_udp_send_queue(gtr.address_, std::move(pd));
}

void Agent::sendGossipZoneAttributesRequest(const NetworkManager::RcvData &rcv_data, const GossipTransaction &gtr)
{
	LOG("sendGossipZoneAttributesRequest" LG(rcv_data, gtr));
	Packet_GossipZoneAttributesRequest req(gtr.id_, rcv_data.my_rcv_timestamp, rcv_data.their_snd_timestamp, gtr.requested_zones_);
	PacketData pd = serializePacketSetTypeAndAddTimeStampOffset(req);
	LOG("sendGossipZoneAttributesRequest sending packet" LG(rcv_data, req, gtr));
	network_.push_to_udp_send_queue(gtr.address_, std::move(pd));
}

void Agent::sendGetZoneAttributesRequest(const GossipTransaction &gtr)
{
	LOG("sendGetZoneAttributesRequest" LG(gtr));
	Packet_GetZoneAttributesRequest req(gtr.id_, gtr.requested_zones_);
	PacketData pd = serializePacketSetTypeAndAddTimeStampOffset(req);
	LOG("sendGetZoneAttributesRequest sending packet" LG(req, gtr));
	network_.push_to_udp_send_queue(gtr.address_, std::move(pd));
}

void Agent::gossipTimeout(boost::system::error_code ec, std::int64_t id)
{
	LOG("gossipTimeout" LG(ec.message(), id));
	if (!ec) {
		auto iter = gossip_transactions_.find(id);
		if (iter != gossip_transactions_.end()) {
			LOG("gossipTimeout" LG(ec.message(), id));
			GossipTransaction & gtr = iter->second;
			if (++gtr.retries > GossipTransaction::kMaxConnectionRetries) {
				my_root_zone_->removeContact(my_root_zone_->getDepth(), gtr.address_);
				io_serv_.post([this, id] () {
					gossip_transactions_.erase(id);
				});
				return;
			}
			switch (gtr.state_) {
			case GossipTransaction::CONNECTING_I:
				sendStartGossip(gtr);
				break;
			case GossipTransaction::RECEIVED_START_R:
			{
				NetworkManager::RcvData rcv_data;
				rcv_data.my_rcv_timestamp = gtr.rcv_data_my_rcv_timestamp;
				rcv_data.their_snd_timestamp = gtr.rcv_data_their_snd_timestamp;
				sendGossipReply(rcv_data, gtr);
			}
				break;

			case GossipTransaction::WAITING_FOR_ATTRIBUTES:
				sendGetZoneAttributesRequest(gtr);

				break;
			default:
				LOG(WARN, "tu nie powinnismy byc ");
				break;
			}
			LOG("timer timeout" LG(gtr.timer_->rerun_time_ms_));
			gtr.timer_->repeat();
		}
	}
}


/* * * * * * * * * * Query Functions * * * * * * * * * */


bool Agent::tryInsertQuery(const serialization::query_wrapper &query, bool with_install)
{
	LOG("tryInsertQuery" LG(query));
	if (!verifySignature(query.query_, query.install_, query.validity_, query.signer_timestamp_, query.signature_.toSignature(), pk_)) {
		LOG("problem z weryfikacją query");
		return false;
	}
	if (query.validity_ < time::now()) {
		LOG("otrzymalismy za stare" LG(query.validity_, time::now()));
		return false;
	}

	auto iter = queries_.find(query.query_);
	if (iter != queries_.end() && iter->second.signer_timestamp_ > query.signer_timestamp_) {
		LOG("mamy query z nowszym timestampem signera" LG(iter->second.signer_timestamp_, query.signer_timestamp_));
		return false;
	}
	QueryData qd;
	qd.query_ = query.query_;
	qd.install_ = query.install_;
	qd.validity_ = query.validity_;
	qd.signer_timestamp_ = query.signer_timestamp_;
	qd.signature_ = query.signature_.toSignature();
	if (qd.install_ && with_install) {
		my_root_zone_->installQueryInAllZones(query.query_);
	}
	LOG(TRACE, "queries before insert" LG(queries_));
	insertIntoMap(queries_, query.query_, std::move(qd));
	LOG(TRACE, "queries after insert" LG(queries_));
	return true;
}

void Agent::updateQueries(const std::list<serialization::query_wrapper> &their_queries)
{
	for (auto q: their_queries) {
		tryInsertQuery(q, false);
	}
}

void Agent::recompute_queries()
{
	std::list<std::string> queries_s;
	for (auto q: queries_) {
		if (q.second.install_ && q.second.validity_ > time::now()) {
			queries_s.push_back(q.first);
		}
	}
	my_root_zone_->eraseNonBaseAtrrs();
	my_root_zone_->recomputeBaseZmi();
	my_root_zone_->removeContact(my_root_zone_->getDepth(), my_addr_);
	my_root_zone_->installQueriesInAllZones(queries_s);
}

std::list<serialization::query_wrapper> Agent::getQueryWrappers() const
{
	std::list<serialization::query_wrapper> queries;
	for (auto q: queries_) {
		serialization::query_wrapper query_w(q.first, q.second.install_, q.second.validity_, q.second.signer_timestamp_, q.second.signature_);
		queries.push_back(query_w);
	}
	return queries;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * GossipStrategyType  * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * */

GossipStrategyType parseGossipStrategy(const std::string &str)
{
#define CLOUD_ENUM(X) \
	if (str == #X) { \
		return GossipStrategyType::X; \
	} else

	GOSSIP_STRATEGY_LIST
#undef CLOUD_ENUM
	{
		return GossipStrategyType::LastUnknown;
	}
}

} /* namespace cloud */

std::ostream & operator << (std::ostream & os, ::cloud::GossipStrategyType v)
{
#define CLOUD_ENUM(X) \
	case ::cloud::GossipStrategyType::X: \
		os << #X; \
		break;

	switch (v) {
		GOSSIP_STRATEGY_LIST
	default:
		os << "";
		break;
	}
	return os;
#undef CLOUD_ENUM
}

std::ostream & operator << (std::ostream & os, const ::cloud::QueryData &v)
{
	os << "QueryData[" << v.query_ << "," << v.install_ << "," << v.validity_ << "," << v.signer_timestamp_ << "]";
	return os;
}

std::ostream & operator << (std::ostream & os, const ::cloud::GossipTransaction &v)
{
	os << "GossipTransaction[" <<v.id_ << "," << v.their_zone_ << "," << v.level_ << "," << v.address_ << "," << v.state_ << "," << v.requested_zones_ << "]";
	return os;
}

std::ostream & operator << (std::ostream & os, const ::cloud::GossipTransaction::state &v)
{
	switch (v) {
	case ::cloud::GossipTransaction::state::CONNECTING_I:
		os << "CONNECTING_I";
		break;

	case ::cloud::GossipTransaction::state::RECEIVED_START_R:
		os << "RECEIVED_START_R";
		break;

	case ::cloud::GossipTransaction::state::WAITING_FOR_ATTRIBUTES:
		os << "WAITING_FOR_ATTRIBUTES";
		break;

	default:
		break;
	}
	return os;
}
