#pragma once

#include <list>

#include "expr_result.h"

namespace cloud {

expr_result process_query_function(const std::string &name, const std::list<expr_result> &args);

} /* namespace cloud */
