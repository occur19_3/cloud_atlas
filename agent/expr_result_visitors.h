#pragma once

#include "expr_result.h"

namespace cloud {

/* * * * * * * * * * get_expr_value * * * * * * * * * */

template<typename T>
struct get_expr_value: public boost::static_visitor<T>
{
	template<typename V>
	enable_if_same<V, T, T>
	operator () (const V &v) const {
		return v;
	}

	template<typename V>
	disable_if_same<V, T, T>
	operator () (const V &v) const {
		throw std::runtime_error("expected T value");
	}
};

struct get_expr_type: public boost::static_visitor<value_type>
{
	template<typename V>
	value_type operator () (const V &v) const {
		return v.t;
	}
};

/* * * * * * * * * * unary_expr_result_visitor * * * * * * * * * */

/**
 *	operacja mapujaca:
 *		one_r<T> -> one_r<R>
 *		col_r<T> -> col_r<R>
 *		list_r<T> -> list_r<R>
 *
 *	not, neg, regex, type_conversions, size, round, floor, ceil
 *
 */

template<typename Visitor>
struct unary_op_expr_result_visitor: public boost::static_visitor<expr_result>
{
	Visitor visitor_;

	template<typename... Args>
	unary_op_expr_result_visitor(Args... args): visitor_(args...) {}

	template<typename V>
	enable_if_same<V, one_r, expr_result>
	operator () (const V &expr) const {
		value_type result_t = Visitor::get_result_type(expr.t);
		if (expr.is_null()) {
			return one_r(result_t, false, get_empty_value(result_t));
		}
		return one_r(result_t, boost::apply_visitor(visitor_, expr.val()));
	}

	template<typename V>
	disable_if_same<V, one_r, expr_result>
	operator () (const V &expr) const {
		value_type result_t = Visitor::get_result_type(expr.t);
		if (expr.is_null()) {
			return V(result_t);
		}
		decltype(expr.res.val) res;
		for (auto el: expr.val()) {
			if (!el.is) {
				res.push_back(nullable_value(false, get_empty_value(result_t)));
			} else {
				res.push_back(boost::apply_visitor(visitor_, el.val));
			}
		}
		return V(result_t, res);
	}
};


///* * * * * * * * * * binary_op_expr_result_visitor * * * * * * * * * */

/**
 *	operacja mapujaca:
 *		one_r<T> one_r<T> -> one_r<R>
 *		col_r<T> -> col_r<R>
 *		list_r<T> -> list_r<R>
 *
 *	+, -, *, /, %, rel
 */
template<typename Visitor>
struct binary_op_expr_result_visitor;

template<typename Visitor>
expr_result apply_bin_op(const expr_result &e1, const expr_result &e2)
{
	LOG("apply_bin_op" LG(e1, e2));
	return boost::apply_visitor(binary_op_expr_result_visitor<Visitor>(e2), e1);
}

template<typename expr_type, typename Visitor>
struct binary_op_specific_expr_result_visitor;

template<typename Visitor>
struct binary_op_expr_result_visitor: public boost::static_visitor<expr_result>
{
	expr_result e2;
	binary_op_expr_result_visitor(expr_result e2): e2(e2) {}

	template<typename T>
	expr_result operator () (const T &v1) const
	{
		value_type result_t = Visitor::get_result_type(v1.t, boost::apply_visitor(get_expr_type(), e2));
		LOG("binary_op_expr_result_visitor" LG(v1, e2, v1.t, result_t));
		return boost::apply_visitor(binary_op_specific_expr_result_visitor<T, Visitor>(result_t, v1), e2);
	}
};

template<typename Visitor>
struct binary_op_specific_expr_result_visitor<one_r, Visitor>: public boost::static_visitor<expr_result>
{
	value_type result_t;
	one_r v1;
	binary_op_specific_expr_result_visitor<one_r, Visitor>(value_type result_t, one_r v1): result_t(result_t), v1(v1) {}

	template<typename V>
	enable_if_same<V, one_r, expr_result>
	operator () (const V &v2) const {
		if (v1.is_null() || v2.is_null()) {
			return one_r(result_t, false, get_empty_value(result_t));
		}
		value result = boost::apply_visitor(Visitor(v2.val()), v1.val());	//!< to ma sens
		return one_r(result_t, result);
	}

	template<typename V>
	disable_if_same<V, one_r, expr_result>
	operator () (const V &v2) const {
		if (v1.is_null() || v2.is_null()) {
			return V(result_t);
		}
		decltype(v2.res.val) res;
		for (auto el2: v2.res.val) {
			if (!el2.is) {
				res.push_back(nullable_value(false, get_empty_value(result_t)));
			} else {
				value v = boost::apply_visitor(Visitor(el2.val), v1.val());
				res.push_back(nullable_value(v));
			}
		}
		return V(result_t, res);
	}
};

template<typename Visitor>
struct binary_op_specific_expr_result_visitor<col_r, Visitor>: public boost::static_visitor<expr_result>
{
	value_type result_t;
	col_r v1;
	binary_op_specific_expr_result_visitor<col_r, Visitor>(value_type result_t, col_r v1): result_t(result_t), v1(v1) {}

	expr_result operator () (const one_r &v2) const {
		if (v1.is_null() || v2.is_null()) {
			return col_r(result_t);
		}
		std::vector<nullable_value> res;
		for (auto el1: v1.res.val) {
			if (!el1.is) {
				res.push_back(nullable_value(false, get_empty_value(result_t)));
			} else {
				value v = boost::apply_visitor(Visitor(v2.val()), el1.val);
				res.push_back(nullable_value(v));
			}
		}
		return col_r(result_t, res);
	}

	expr_result operator () (const col_r &v2) const {
		if (v1.is_null() || v2.is_null()) {
			return col_r(result_t);
		}
		if (v1.val().size() != v2.val().size()) {
			throw std::runtime_error("columns have different sizes, something went really wrong");
		}
		std::vector<nullable_value> res;
		for (size_t i = 0; i < v1.val().size(); ++i) {
			auto el1 = v1.val()[i];
			auto el2 = v2.val()[i];
			if (!el1.is || !el2.is) {
				res.push_back(nullable_value(false, get_empty_value(result_t)));
			} else {
				value v = boost::apply_visitor(Visitor(el2.val), el1.val);
				res.push_back(nullable_value(v));
			}
		}
		return col_r(result_t, res);
	}

	expr_result operator () (const list_r &v2) const {
		LOG(WARN, "binary_op_specific_expr_result_visitor col_r expected one_t or col_r type" LG(v1, v2));
		throw std::runtime_error("binary_op_specific_expr_result_visitor col_r expected one_t or col_r type");
		return col_r(result_t, std::vector<nullable_value>());
	}
};

template<typename Visitor>
struct binary_op_specific_expr_result_visitor<list_r, Visitor>: public boost::static_visitor<expr_result>
{
	value_type result_t;
	list_r v1;
	binary_op_specific_expr_result_visitor<list_r, Visitor>(value_type result_t, list_r v1): result_t(result_t), v1(v1) {}

	template<typename V>
	enable_if_same<V, one_r, expr_result>
	operator () (const V &v2) const {
		if (v1.is_null() || v2.is_null()) {
			return list_r(result_t);
		}
		std::list<nullable_value> res;
		for (auto el1: v1.res.val) {
			if (!el1.is) {
				res.push_back(nullable_value(false, get_empty_value(result_t)));
			} else {
				value v = boost::apply_visitor(Visitor(v2.val()), el1.val);
				res.push_back(nullable_value(v));
			}
		}
		return list_r(result_t, res);
	}

	template<typename V>
	disable_if_same<V, one_r, expr_result>
	operator () (const V &v2) const {
		LOG(WARN, "binary_op_specific_expr_result_visitor list_r expected one_t type" LG(v1, v2));
		throw std::runtime_error("binary_op_specific_expr_result_visitor list_r expected one_r type");
		return list_r(result_t);
	}
};

} /* namespace cloud */

