#pragma once

#include <boost/regex.hpp>

#include "log_utils.h"
#include "value.h"

#undef  LOG_DEFAULT_CATEGORY
#define LOG_DEFAULT_CATEGORY PARSER

namespace cloud {


/* * * * * * * * * * not_value_operator * * * * * * * * * */


struct not_value_operator: public boost::static_visitor<value>
{
	static value_type get_result_type(const value_type& t) {
		if (!t.collection_.empty() || t.type_ != SimpleType::BOOL) {
			LOG(WARN, "not_value_operator expected bool value" LG(t));
			throw std::runtime_error("not_value_operator expected bool value");
		}
		return SimpleType::BOOL;
	}

	template<typename T>
	enable_if_same<T, bool, value>
	operator() (const T &v) const
	{
		return value(!v);
	}

	template<typename T>
	disable_if_same<T, bool, value>
	operator() (const T &v) const
	{
		LOG(WARN, "not_value_operator expected bool value" LG(v));
		throw std::runtime_error("not_value_operator expected bool value");
		return value(false);
	}
};


/* * * * * * * * * * negate_value_operator * * * * * * * * * */


struct negate_value_operator: public boost::static_visitor<value>
{
	static value_type get_result_type(const value_type& t) {
		if (!t.collection_.empty()) {
			LOG(WARN, "negate_value_operator expected numeric value" LG(t));
			throw std::runtime_error("negate_value_operator expected numeric value");
		}
		switch (t.type_) {
		case SimpleType::DOUBLE:
			return SimpleType::DOUBLE;
		case SimpleType::DURATION:
			return SimpleType::DURATION;
		case SimpleType::INTEGER:
			return SimpleType::INTEGER;
		default:
			LOG(WARN, "negate_value_operator expected numeric value" LG(t));
			throw std::runtime_error("negate_value_operator expected numeric value");
			break;
		}
		return SimpleType::BOOL;
	}

	template<typename T>
	typename boost::enable_if<boost::mpl::or_<
			std::is_same<T, double>,
			std::is_same<T, duration>,
			std::is_same<T, integer>
		>, value>::type
	operator() (const T &v) const
	{
		return value(-v);
	}

	template<typename T>
	typename boost::disable_if<boost::mpl::or_<
			std::is_same<T, double>,
			std::is_same<T, duration>,
			std::is_same<T, integer>
		>, value>::type
	operator() (const T &v) const
	{
		LOG(WARN, "negate_value_operator expected numeric value" LG(v));
		throw std::runtime_error("negate_value_operator expected numeric value");
		return value();
	}
};


/* * * * * * * * * * regex_value_operator * * * * * * * * * */


struct regex_value_operator: public boost::static_visitor<value>
{
	static value_type get_result_type(const value_type& t) {
		if (!t.collection_.empty() || t.type_ != SimpleType::STRING) {
			LOG(WARN, "regex_value_operator expected bool value" LG(t));
			throw std::runtime_error("regex_value_operator expected bool value");
		}
		return SimpleType::BOOL;
	}

	std::string regex_;
	regex_value_operator(const std::string &regex): regex_(regex) {}

	template<typename T>
	enable_if_same<T, std::string, value>
	operator() (const T &v) const
	{
		bool result = boost::regex_match(v, boost::regex(regex_));
		return value(result);
	}

	template<typename T>
	disable_if_same<T, std::string, value>
	operator() (const T &v) const
	{
		LOG(WARN, "regex_value_operator expected std::string value" LG(v));
		throw std::runtime_error("regex_value_operator expected std::string value");
		return value(false);
	}
};


/* * * * * * * * * * add_value_operator * * * * * * * * * */


struct logic_and_operator {
	static bool op (bool b1, bool b2) { return b1 && b2; }
};

struct logic_or_operator {
	static bool op (bool b1, bool b2) { return b1 || b2; }
};

template<typename Operator>
struct logic_value_operator: public boost::static_visitor<value>
{
	static value_type get_result_type(const value_type &t1, const value_type &t2) {
		if (!t1.isSimpleType(SimpleType::BOOL) || !t2.isSimpleType(SimpleType::BOOL)) {
			LOG(WARN, "logic_value_operator expected bool value" LG(t1, t2));
			throw std::runtime_error("logic_value_operator expected bool value");
		}
		return SimpleType::BOOL;
	}

	value v2;
	logic_value_operator(const value &v2): v2(v2) {}

	template<typename T>
	enable_if_same<T, bool, value>
	operator() (const T &v1) const
	{
		return Operator::op(v1, get_value_wrapper<bool>(v2));
	}

	template<typename T>
	disable_if_same<T, bool, value>
	operator() (const T &v1) const
	{
		LOG(WARN, "logic_value_operator expected boolean value" LG(v1, v2));
		throw std::runtime_error("logic_value_operator expected boolean value");
		return value();
	}
};

using and_operator = logic_value_operator<logic_and_operator>;
using or_operator = logic_value_operator<logic_or_operator>;


/* * * * * * * * * * relop_value_operator * * * * * * * * * */


template<typename Operator>
struct relop_value_operator: public boost::static_visitor<bool>
{
	static value_type get_result_type(const value_type &t1, const value_type &t2) {
		if (!t1.isSimpleType() || !t2.isSimpleType()) {
			LOG(WARN, "relop_value_operator expected simple value" LG(t1, t2));
			throw std::runtime_error("relop_value_operator expected simple value");
		}
		if (t1.type_ != t2.type_) {
			LOG(WARN, "relop_value_operator expected types must be equal" LG(t1, t2));
			throw std::runtime_error("relop_value_operator expected types must be equal");
		}
		switch (t1.type_) {
		case SimpleType::BOOL:
		case SimpleType::CONTACT:
			LOG(WARN, "relop_value_operator expected types not comparable" LG(t1, t2));
			throw std::runtime_error("relop_value_operator expected types not comparable");
			break;

		default:
			break;
		}

		return SimpleType::BOOL;
	}

	value v2;
	relop_value_operator(const value &v2): v2(v2) {}

	template<typename T>
	typename boost::enable_if<boost::mpl::or_<
//			std::is_same<T, bool>,
			std::is_same<T, double>,
			std::is_same<T, duration>,
			std::is_same<T, integer>,
			std::is_same<T, std::string>,
			std::is_same<T, time>
		>, bool>::type
	operator() (const T &v1) const
	{
		return Operator::op(v1, get_value_wrapper<T>(v2));
	}

	template<typename T>
	typename boost::disable_if<boost::mpl::or_<
//			std::is_same<T, bool>,
			std::is_same<T, double>,
			std::is_same<T, duration>,
			std::is_same<T, integer>,
			std::is_same<T, std::string>,
			std::is_same<T, time>
		>, bool>::type
	operator() (const T &v1) const
	{
		LOG(WARN, "relop_value_operator expected comparable value" LG(v1, v2));
		throw std::runtime_error("relop_value_operator expected comparable value");
		return false;
	}
};

struct rel_lth_operator {
	template<typename T>
	static bool op (T v1, T v2) { return v1 < v2; }
};

struct rel_le_operator {
	template<typename T>
	static bool op (T v1, T v2) { return v1 <= v2; }
};

struct rel_gth_operator {
	template<typename T>
	static bool op (T v1, T v2) { return v1 > v2; }
};

struct rel_ge_operator {
	template<typename T>
	static bool op (T v1, T v2) { return v1 >= v2; }
};

struct rel_equ_operator {
	template<typename T>
	static bool op (T v1, T v2) { return v1 == v2; }
};

struct rel_ne_operator {
	template<typename T>
	static bool op (T v1, T v2) { return v1 != v2; }
};

using lth_operator = relop_value_operator<rel_lth_operator>;
using le_operator = relop_value_operator<rel_le_operator>;
using gth_operator = relop_value_operator<rel_gth_operator>;
using ge_operator = relop_value_operator<rel_ge_operator>;
using equ_operator = relop_value_operator<rel_equ_operator>;
using ne_operator = relop_value_operator<rel_ne_operator>;


/* * * * * * * * * * add_value_operator * * * * * * * * * */


struct add_value_operator: public boost::static_visitor<value>
{
	static value_type get_result_type(const value_type &t1, const value_type &t2) {
		if (!t1.isSimpleType()) {
			if (t1 != t2) {
				LOG(WARN, "add_value_operator collection types must be equal" LG(t1, t2));
				throw std::runtime_error("add_value_operator collection types must be equal");
			}
		} else {
			if (!t2.isSimpleType()) {
				LOG(WARN, "add_value_operator cannot add single to collection" LG(t1, t2));
				throw std::runtime_error("add_value_operator cannot add single to collection");
			}

			switch (t1.type_) {
			case SimpleType::BOOL:
			case SimpleType::CONTACT:
				LOG(WARN, "add_value_operator expected types not addable" LG(t1, t2));
				throw std::runtime_error("add_value_operator expected types not addable");
				break;

			case SimpleType::TIME:
				if (t2.type_ != SimpleType::DURATION) {
					LOG(WARN, "add_value_operator expected types not addable" LG(t1, t2));
					throw std::runtime_error("add_value_operator expected types not addable");
				}
				break;

			default:
				if (t1.type_ != t2.type_) {
					LOG(WARN, "add_value_operator expected types must be equal" LG(t1, t2));
					throw std::runtime_error("add_value_operator expected types must be equal");
				}
				break;
			}
		}
		return t1;
	}


	value v2;
	add_value_operator(const value &v2): v2(v2) {}

	template<typename T>
	typename boost::enable_if<boost::mpl::or_<
			std::is_same<T, double>,
			std::is_same<T, duration>,
			std::is_same<T, integer>,
			std::is_same<T, std::string>
		>, value>::type
	operator() (const T &v1) const
	{
		return v1 + get_value_wrapper<T>(v2);
	}

	template<typename T>
	enable_if_same<T, time, value>
	operator() (const T &v1) const
	{
		return v1 + get_value_wrapper<duration>(v2);
	}

	template<typename T>
	enable_if_same<T, std::list<value>, value>
	operator() (const T &v1) const
	{
		std::list<value> l1 = v1;
		std::list<value> l2 = get_value_wrapper<std::list<value>>(v2);
		l1.splice(l1.end(), l2);
		return l1;
	}

	template<typename T>
	enable_if_same<T, std::set<value>, value>
	operator() (const T &v1) const
	{
		std::set<value> s2 = get_value_wrapper<std::set<value>>(v2);
		s2.insert(v1.begin(), v1.end());
		return s2;
	}

	template<typename T>
	typename boost::disable_if<
		boost::mpl::or_<
			boost::mpl::or_<
				std::is_same<T, double>,
				std::is_same<T, duration>,
				std::is_same<T, integer>,
				std::is_same<T, std::string>
			>,
			boost::mpl::or_<
				std::is_same<T, std::list<value> >,
				std::is_same<T, std::set<value> >,
				std::is_same<T, time>
			>
		>, value>::type
	operator() (const T &v1) const
	{
		LOG(WARN, "add_value_operator expected addable value" LG(v1, v2));
		throw std::runtime_error("add_value_operator expected addable value");
		return value();
	}
};

/* * * * * * * * * * minus_value_operator * * * * * * * * * */


struct time_minus_value_operator: public boost::static_visitor<value>
{
	time v1;
	time_minus_value_operator(time v1): v1(v1) {}

	template<typename T>
	typename boost::enable_if<boost::mpl::or_<
			std::is_same<T, duration>,
			std::is_same<T, time>
		>, value>::type
	operator() (const T &v2) const
	{
		return v1 - v2;
	}
	template<typename T>
	typename boost::disable_if<boost::mpl::or_<
			std::is_same<T, duration>,
			std::is_same<T, time>
		>, value>::type
	operator() (const T &v2) const
	{
		LOG("time_minus_value_operator expected addable value" LG(v1, v2));
		throw std::runtime_error("time_minus_value_operator expected addable value");
		return value();
	}
};

struct minus_value_operator: public boost::static_visitor<value>
{
	static value_type get_result_type(const value_type &t1, const value_type &t2) {
		if (!t1.isSimpleType() || !t2.isSimpleType()) {
			LOG(WARN, "minus_value_operator collections cannot be subtracted" LG(t1, t2));
			throw std::runtime_error("minus_value_operatorcannot be subtracted");
		}
		switch (t1.type_) {
		case SimpleType::BOOL:
		case SimpleType::CONTACT:
		case SimpleType::STRING:
			LOG(WARN, "minus_value_operator expected types not addable" LG(t1, t2));
			throw std::runtime_error("minus_value_operator expected types not addable");
			break;

		case SimpleType::TIME:
			if (t2.type_ == SimpleType::DURATION ) {
				return SimpleType::TIME;
			} else
			if (t2.type_ == SimpleType::TIME) {
				return SimpleType::DURATION;
			} else {
				LOG(WARN, "minus_value_operator expected types not addable" LG(t1, t2));
				throw std::runtime_error("minus_value_operator expected types not addable");
			}
			break;

		default:
			if (t1.type_ != t2.type_) {
				LOG(WARN, "minus_value_operator expected types must be equal" LG(t1, t2));
				throw std::runtime_error("minus_value_operator expected types must be equal");
			}
			break;
		}
		return t1;
	}

	value v2;
	minus_value_operator(const value &v2): v2(v2) {}

	template<typename T>
	typename boost::enable_if<boost::mpl::or_<
			std::is_same<T, double>,
			std::is_same<T, duration>,
			std::is_same<T, integer>
		>, value>::type
	operator() (const T &v1) const
	{
		return v1 - get_value_wrapper<T>(v2);
	}

	template<typename T>
	enable_if_same<T, time, value>
	operator() (const T &v1) const
	{
		return boost::apply_visitor(time_minus_value_operator(v1), v2);
	}

	template<typename T>
	typename boost::disable_if<boost::mpl::or_<
			std::is_same<T, double>,
			std::is_same<T, duration>,
			std::is_same<T, integer>,
			std::is_same<T, time>
		>, value>::type
	operator() (const T &v1) const
	{
		LOG(WARN, "minus_value_operator expected minusable value" LG(v1, v2));
		throw std::runtime_error("minus_value_operator expected minusable value");
		return value();
	}
};


/* * * * * * * * * * mul_value_operator * * * * * * * * * */


struct modulo_value_operator: public boost::static_visitor<value>
{
	static value_type get_result_type(const value_type &t1, const value_type &t2) {
		if (!t1.isSimpleType(SimpleType::INTEGER) || !t2.isSimpleType(SimpleType::INTEGER)) {
			LOG(WARN, "modulo_value_operator expected integer" LG(t1, t2));
			throw std::runtime_error("modulo_value_operator expected integer");
		}
		return SimpleType::INTEGER;
	}

	value v2;
	modulo_value_operator(value v2): v2(v2) {}

	template<typename T>
	enable_if_same<T, integer, value>
	operator() (const T &v1) const
	{
		auto i2 = get_value_wrapper<integer>(v2);
		if (i2 == 0) {
			LOG(WARN, "modulo_value_operator modulo by zero" LG(v1, v2));
			throw std::runtime_error("modulo_value_operator modulo by zero");
		}
		return v1 % i2;
	}

	template<typename T>
	disable_if_same<T, integer, value>
	operator() (const T &v1) const
	{
		LOG(WARN, "modulo_value_operator expected int value" LG(v1, v2));
		throw std::runtime_error("modulo_value_operator expected int value");
		return value();
	}
};

struct div_value_operator: public boost::static_visitor<value>
{
	static value_type get_result_type(const value_type &t1, const value_type &t2) {
		if (!t1.isSimpleType() || !t2.isSimpleType()) {
			LOG(WARN, "div_value_operator collections cannot be divided" LG(t1, t2));
			throw std::runtime_error("div_value_operator cannot be divided");
		}

		switch (t1.type_) {
		case SimpleType::DOUBLE:
		case SimpleType::INTEGER:
			if (t1.type_ != t2.type_) {
				LOG(WARN, "div_value_operator double integer types must be equal" LG(t1, t2));
				throw std::runtime_error("div_value_operator double integer types must be equal");
			}
			return SimpleType::DOUBLE;
			break;

		case SimpleType::DURATION:
			if (t2.type_ != SimpleType::INTEGER ) {
				LOG(WARN, "div_value_operator duraiton is dividable only by int" LG(t1, t2));
				throw std::runtime_error("div_value_operator is dividable only by int");
			}
			return SimpleType::DURATION;
			break;

		default:
			LOG(WARN, "div_value_operator cannot be divided" LG(t1, t2));
			throw std::runtime_error("div_value_operator cannot be divided");
			break;
		}
		return t1;
	}

	value v2;
	div_value_operator(const value &v2): v2(v2) {}

	template<typename T>
	enable_if_same<T, double, value>
	operator() (const T &v1) const
	{
		auto i2 = get_value_wrapper<double>(v2);
		if (i2 == 0) {
			LOG(WARN, "div_value_operator double divison by zero" LG(v1, v2));
			throw std::runtime_error("div_value_operator double divison by zero");
		}
		return v1 / i2;
	}

	template<typename T>
	enable_if_same<T, integer, value>
	operator() (const T &v1) const
	{
		auto i2 = get_value_wrapper<integer>(v2);
		if (i2 == 0) {
			LOG(WARN, "div_value_operator integer divison by zero" LG(v1, v2));
			throw std::runtime_error("div_value_operator integer divison by zero");
		}
		return static_cast<double>(v1) / i2;
	}

	template<typename T>
	enable_if_same<T, duration, value>
	operator() (const T &v1) const
	{
		auto i2 = get_value_wrapper<integer>(v2);
		if (i2 == 0) {
			LOG(WARN, "div_value_operator duration divison by zero" LG(v1, v2));
			throw std::runtime_error("div_value_operator duration divison by zero");
		}
		return v1 / i2;
	}

	template<typename T>
	typename boost::disable_if<boost::mpl::or_<
			std::is_same<T, double>,
			std::is_same<T, duration>,
			std::is_same<T, integer>
		>, value>::type
	operator() (const T &v1) const
	{
		LOG(WARN, "div_value_operator expected dividable value" LG(v1, v2));
		throw std::runtime_error("div_value_operator expected dividable value");
		return value();
	}
};

struct times_value_operator: public boost::static_visitor<value>
{
	static value_type get_result_type(const value_type &t1, const value_type &t2) {
		if (!t1.isSimpleType() || !t2.isSimpleType()) {
			LOG(WARN, "times_value_operator collections cannot be divided" LG(t1, t2));
			throw std::runtime_error("times_value_operator cannot be divided");
		}

		switch (t1.type_) {
		case SimpleType::DOUBLE:
		case SimpleType::INTEGER:
			if (t1.type_ != t2.type_) {
				LOG(WARN, "times_value_operator double integer types must be equal" LG(t1, t2));
				throw std::runtime_error("times_value_operator double integer types must be equal");
			}
			return t1;
			break;

		case SimpleType::DURATION:
			if (t2.type_ != SimpleType::INTEGER ) {
				LOG(WARN, "times_value_operator duraiton is multiplable only by int" LG(t1, t2));
				throw std::runtime_error("times_value_operator is multiplable only by int");
			}
			return SimpleType::DURATION;
			break;

		default:
			LOG(WARN, "times_value_operator cannot be multiplablied" LG(t1, t2));
			throw std::runtime_error("times_value_operator cannot be multiplablied");
			break;
		}
		return t1;
	}


	value v2;
	times_value_operator(const value &v2): v2(v2) {}

	template<typename T>
	typename boost::enable_if<boost::mpl::or_<
			std::is_same<T, double>,
			std::is_same<T, integer>
	>, value>::type
	operator() (const T &v1) const
	{
		return v1 * get_value_wrapper<T>(v2);
	}

	template<typename T>
	enable_if_same<T, duration, value>
	operator() (const T &v1) const
	{
		return v1 * get_value_wrapper<integer>(v2);
	}

	template<typename T>
	typename boost::disable_if<boost::mpl::or_<
			std::is_same<T, double>,
			std::is_same<T, duration>,
			std::is_same<T, integer>
		>, value>::type
	operator() (const T &v1) const
	{
		LOG(WARN, "times_value_operator expected timesable value" LG(v1, v2));
		throw std::runtime_error("times_value_operator expected timesable value");
		return value();
	}
};

}
