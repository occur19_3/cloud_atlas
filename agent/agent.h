#pragma once

#include <ostream>

#include "crypto.h"
#include "network.h"
#include "utils.h"
#include "value_types.h"

namespace cloud {

class Zone;
class GossipStrategy;

struct QueryData {
	std::string query_;
	bool install_;
	time validity_;
	time signer_timestamp_;
	Signature signature_;
};

struct GossipTransaction {
	static constexpr std::int64_t kMaxConnectionRetries = 3;

	enum state {
		CONNECTING_I,
		RECEIVED_START_R,
		WAITING_FOR_ATTRIBUTES
	} state_;
	std::int64_t id_;

	std::int64_t time_diff_;
	std::int64_t my_snd_timestamp = 0;
	std::int64_t my_rcv_timestamp = 0;
	std::int64_t their_snd_timestamp = 0;
	std::int64_t their_rcv_timestamp = 0;


	std::int64_t rcv_data_my_rcv_timestamp = 0;
	std::int64_t rcv_data_their_snd_timestamp = 0;

	size_t level_;
	contact address_;
	zone_path their_zone_;

	std::set<zone_path> requested_zones_;

	ZmiFreshness their_freshness_;

	std::int64_t retries = 0;
	boost::shared_ptr<RepeatTimer> timer_;

	void calculateTimeDiff();
	void setRequestedZones(const ZmiFreshness &my_freshness, const ZmiFreshness &their_freshness);
};

enum class GossipStrategyType: std::uint8_t;

class Agent {
public:
	Agent(boost::asio::io_service &io_serv);
	~Agent();

	bool start(const std::string &host, std::uint16_t port, const std::string &zone, const std::string &public_key_file, std::uint32_t recompute_period,
			std::uint32_t gossip_period, const std::string &gossip_strategy, std::uint32_t purge_period, const std::string &contact_s);
	void stop();

private:
	bool load_public_key(const std::string &public_key_file);
	bool initialize_zone_data(const std::string &zone_name);
	bool set_gossip_strategy(const std::string &gossip_strategy);

	void packet_arrived(NetworkManager::RcvData &&packet);
	void processFetcherData(const std::list<Attribute> &attributes);
	void processSetFallbackContactsRequest(const NetworkManager::RcvData &rcv_data, std::int64_t id, const std::set<contact> &new_contacts);
	void processGetKeptZoneNamesRequest(const NetworkManager::RcvData &rcv_data, std::int64_t id);
	void processGetZoneAttributesRequest(const NetworkManager::RcvData &rcv_data, std::int64_t id, const std::set<zone_path> &requested_zones);
	void processGetZoneAttributesResponse(const NetworkManager::RcvData &rcv_data, std::int64_t id, const zone_path &zone, time timestamp, const std::list<Attribute> &attributes);
	void processSetSingletonZoneAttributeRequest(const NetworkManager::RcvData &rcv_data, std::int64_t id, const std::list<Attribute> &attributes);
	void processInstallQueryRequest(const NetworkManager::RcvData &rcv_data, std::int64_t id, const serialization::query_wrapper &query);
	void processStartGossip(const NetworkManager::RcvData &rcv_data, std::int64_t id, const ZmiFreshness &zones, const std::list<serialization::query_wrapper> &queries);
	void processGossipReply(const NetworkManager::RcvData &rcv_data, std::int64_t id, std::int64_t last_my_rcv_timestamp, std::int64_t last_their_snd_timestamp,
							const ZmiFreshness &zones, const std::list<serialization::query_wrapper> &queries);
	void processGossipZoneAttributesRequest(const NetworkManager::RcvData &rcv_data, std::int64_t id,  std::int64_t last_my_rcv_timestamp, std::int64_t last_their_snd_timestamp,
											const std::set<zone_path> &requested_zone);

	void start_recompute(boost::system::error_code ec);
	void start_gossip(boost::system::error_code ec);
	void start_purge(boost::system::error_code ec);

	void sendStartGossip(const GossipTransaction &gtr);
	void sendGossipReply(const NetworkManager::RcvData &rcv_data, const GossipTransaction &gtr);
	void sendGossipZoneAttributesRequest(const NetworkManager::RcvData &rcv_data, const GossipTransaction &gtr);
	void sendGetZoneAttributesRequest(const GossipTransaction &gtr);

	void gossipTimeout(boost::system::error_code ec, std::int64_t id);	//!< wywolywany, jrsli nie otrzmyamyo odpowiedzi na pakiet plotkowania w sensownym czase

	/** Nowe query wstawiamy wtw timestamp signer'a jest nowszy
	 *	query_signer dba o to, zeby nie nadpisywac query tzn. aby zmienic, musimy najpierw odinstalowac stare
	 *	zakladamy, ze validity jest w miare sensownie ustalane tzn. roznica miedzy czasem agenta nie jest jakas bardzo zla
	 */
	bool tryInsertQuery(const serialization::query_wrapper &query, bool with_install = true);
	void updateQueries(const std::list<serialization::query_wrapper> &their_queries);
	void recompute_queries();	//!< czysci wszystko i od nowa liczy atrybuty

	std::list<serialization::query_wrapper> getQueryWrappers() const;

	boost::asio::io_service &io_serv_;
	NetworkManager network_;

	// data

	contact my_addr_;
	std::string zone_;
	PublicKey pk_;

	std::set<contact> fallback_contacts_;
	Zone* my_root_zone_ = nullptr;
	std::map<std::string, QueryData> queries_;

	GossipStrategy* gossip_strategy_ = nullptr;
	std::map<std::int64_t, GossipTransaction> gossip_transactions_;

	// timers

	RepeatTimer recompute_timer_;
	RepeatTimer gossip_timer_;
	RepeatTimer purge_timer_;
};

#define GOSSIP_STRATEGY_LIST \
	CLOUD_ENUM(random) \
	CLOUD_ENUM(random_exp) \
	CLOUD_ENUM(round_robin) \
	CLOUD_ENUM(round_robin_exp)

enum class GossipStrategyType: std::uint8_t {
#define CLOUD_ENUM(X) X,
	GOSSIP_STRATEGY_LIST
	LastUnknown
#undef CLOUD_ENUM
};

GossipStrategyType parseGossipStrategy(const std::string &str);

} /* namespace cloud */

std::ostream & operator << (std::ostream & os, ::cloud::GossipStrategyType v);
std::ostream & operator << (std::ostream & os, const ::cloud::QueryData &v);
std::ostream & operator << (std::ostream & os, const ::cloud::GossipTransaction &v);
std::ostream & operator << (std::ostream & os, const ::cloud::GossipTransaction::state &v);
