#pragma once

#include <stdexcept>
#include <list>
#include <vector>

#ifdef CLOUD_COMPILE_VARIANT
#include <boost/variant/apply_visitor.hpp>
#include <boost/variant/static_visitor.hpp>
#include <boost/variant/variant.hpp>
#endif

#include "value.h"


namespace cloud {

/* * * * * * * * * * result_type * * * * * * * * * */

template<typename T>
struct result_type {
	value_type t;
	nullable_type<T> res;

	result_type(const value_type &t): t(t) {}
	result_type(const value_type &t, T res): t(t), res(res) {}

	result_type(const value_type &t, bool v, T res):  t(t), res(v, res) {}

	result_type(const value_type &t, nullable_type<T> res):  t(t), res(res) {}

	//convenience stuff
	bool is_null() const { return !res.is; }
	T& val(){ return res.val; }
	const T& val() const  { return res.val; }
};

//create null

using one_r = result_type<value>;
using col_r = result_type<std::vector<nullable_value>>;
using list_r = result_type<std::list<nullable_value>>;


/* * * * * * * * * * expr_result * * * * * * * * * */

using expr_result = boost::variant<one_r, col_r, list_r>;

}	/* namespace cloud */

std::ostream & operator <<(std::ostream & os, const cloud::one_r &v);
std::ostream & operator <<(std::ostream & os, const cloud::col_r &v);
std::ostream & operator <<(std::ostream & os, const cloud::list_r &v);
std::ostream & operator <<(std::ostream & os, const cloud::expr_result &v);

