#include <iostream>
#include <string>

#include <boost/program_options.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/address_v4.hpp>
#include <boost/asio/signal_set.hpp>

#include "agent.h"
#include "log_utils.h"
#include "value_types.h"

#undef  LOG_DEFAULT_CATEGORY
#define LOG_DEFAULT_CATEGORY AGENT

int main(int argc, char* argv[])
{
	std::string config_file;

	std::string host;
	std::uint16_t port;
	std::string zone;
	std::string public_key_file;
	std::uint32_t recompute_period;
	std::uint32_t gossip_period;
	std::string gossip_strategy;
	std::uint32_t purge_period;
	std::string fallback_contacts;

	boost::program_options::options_description desc("CloudAtlas Agent options");
	desc.add_options()
		("help", "Shows help message")
		("config,c", boost::program_options::value<std::string>(&config_file), "agent configuration file")

		("host", boost::program_options::value<std::string>(&host)->default_value(cloud::kDefaultCloudHost), "listen host")
		("port", boost::program_options::value<std::uint16_t>(&port)->default_value(cloud::kDefaultCloudAgentPort), "listen port")
		("zone,z", boost::program_options::value<std::string>(&zone), "zone path")
		("public-key-file", boost::program_options::value<std::string>(&public_key_file)->default_value("public_key.key"), "associated QuerySigner public_key")
		("recompute-period,r", boost::program_options::value<std::uint32_t>(&recompute_period)->default_value(5), "value recompute period")
		("gossip-period,g", boost::program_options::value<std::uint32_t>(&gossip_period)->default_value(5), "gossip period")
		("gossip-strategy,s", boost::program_options::value<std::string>(&gossip_strategy)->default_value("random"), "gossip strategy period")
		("purge-period,D", boost::program_options::value<std::uint32_t>(&purge_period)->default_value(60), "value purge period")
		("fallback-contacts,f", boost::program_options::value<std::string>(&fallback_contacts), "start fallback contacts - can be empty")
	;

	try {
		boost::program_options::variables_map vm;
		boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(desc).run(), vm);
		boost::program_options::notify(vm);
		if (vm.count("help")) {
			std::cout << desc;
			return 0;
		}

	} catch (std::exception &e) {
		std::cout << "Invalid arguments provided: " << e.what() << std::endl;
		std::cout << "Check --help for usage" << std::endl;
		return 1;
	}

	boost::asio::io_service io_serv;
	cloud::Agent agent(io_serv);
	boost::asio::signal_set signals_set(io_serv, SIGINT);
	signals_set.async_wait([&agent](const boost::system::error_code& ec, int n) {
		LOG("Received signal" LG(ec, n));
		if (!ec) {

			agent.stop();
		}
	});
	return agent.start(host, port, zone, public_key_file, recompute_period, gossip_period, gossip_strategy, purge_period, fallback_contacts) ? 0 : 1;
}
