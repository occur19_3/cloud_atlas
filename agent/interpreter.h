#pragma once

#include "Absyn.H"
#include "expr_result.h"
#include "value.h"
#include "zone.h"

namespace cloud {

class Zone;

enum class OrderOptType {
	ASC,
	DESC
};

enum class OrderNullsOptType {
	NULLS_FIRST,
	NULLS_LAST
};

enum class SelModOptType {
	ALL,
	DISTINCT
};

enum class RelOpType {
	LTH,
	LE,
	GTH,
	GE,
	EQU,
	NE
};

enum class AddOpType {
	PlusOp,
	MinusOp
};

enum class MulOpType {
	TimesOp,
	DivideOp,
	ModuloOp
};

using query_result_type = std::list<std::tuple<std::string, value_type, nullable_value>>;

struct QueryInterpretResult {
	std::string name_;
	query_result_type attributes_;
};
class QueryInterpreter : public Visitor
{
public:
	QueryInterpreter(const std::vector<AttributesMap> &attrs): Visitor(), const_attr_map_(attrs), state_(true, 0, attrs) {}

	QueryInterpretResult* process_query(Program *p);

	//Program
	void visitProgram(Program* p) override;
	void visitProgramQuery(ProgramQuery* p) override;

	//Stmt
	void visitStmt(Stmt* p) override;
	void visitListStmt(ListStmt* p) override;
	void visitSelStmt(SelStmt* p) override;
	void visitSelOrdStmt(SelOrdStmt* p) override;
	void visitSelWhrStmt(SelWhrStmt* p) override;
	void visitSelWhrOrdStmt(SelWhrOrdStmt* p) override;

	//SelectClause
	void visitSelectClause(SelectClause* p) override;
	void visitSelectClauseRule(SelectClauseRule* p) override;

	//WhereClause
	void visitWhereClause(WhereClause* p) override;
	void visitWhereClauseRule(WhereClauseRule* p) override;

	//OrderClause
	void visitOrderClause(OrderClause* p) override;
	void visitOrderClauseRule(OrderClauseRule* p) override;

	//Order
	void visitOrder(Order* p) override;
	void visitListOrder(ListOrder* p) override;
	void visitOrderSim(OrderSim* p) override;
	void visitOrderNulls(OrderNulls* p) override;
	void visitOrderOptSim(OrderOptSim* p) override;
	void visitOrderOptNulls(OrderOptNulls* p) override;

	//OrderOpt
	void visitOrderOpt(OrderOpt* p) override;
	void visitOrderOptAsc(OrderOptAsc* p) override;
	void visitOrderOptDesc(OrderOptDesc* p) override;

	//OrderNullsOpr
	void visitOrderNullsOpt(OrderNullsOpt* p) override;
	void visitOrderNullsFirst(OrderNullsFirst* p) override;
	void visitOrderNullsLast(OrderNullsLast* p) override;

	//Select
	void visitSelect(Select* p) override;
	void visitListSelect(ListSelect* p) override;
	void visitSelectSim(SelectSim* p) override;
	void visitSelectAs(SelectAs* p) override;

	//SelectStart
	void visitSelectStart(SelectStart* p) override;
	void visitSelectStartSim(SelectStartSim* p) override;
	void visitSelectStartOpt(SelectStartOpt* p) override;

	//SelModOpt
	void visitSelModOpt(SelModOpt* p) override;
	void visitSelModOptAll(SelModOptAll* p) override;
	void visitSelModOptDist(SelModOptDist* p) override;

	//Expr
	void visitExpr(Expr* p) override;
	void visitListExpr(ListExpr* p) override;

	void visitEOr(EOr* p) override;
	void visitEAnd(EAnd* p) override;
	void visitENot(ENot* p) override;
	void visitERel(ERel* p) override;
	void visitERegexp(ERegexp* p) override;
	void visitEAdd(EAdd* p) override;
	void visitEMul(EMul* p) override;
	void visitENeg(ENeg* p) override;


	void visitETrue(ETrue* p) override;
	void visitEFalse(EFalse* p) override;
	void visitEInt(EInt* p) override;
	void visitEDouble(EDouble* p) override;
	void visitEString(EString* p) override;
	void visitEVar(EVar* p) override;

	void visitEFun(EFun* p) override;
	void visitEStmt(EStmt* p) override;

	void visitEBraceEmpty(EBraceEmpty* p) override;
	void visitEBracketExpr(EBracketExpr* p) override;
	void visitEFunStar(EFunStar* p) override;

	//RelOp
	void visitRelOp(RelOp* p) override;
	void visitLTH(LTH* p) override;
	void visitLE(LE* p) override;
	void visitGTH(GTH* p) override;
	void visitGE(GE* p) override;
	void visitEQU(EQU* p) override;
	void visitNE(NE* p) override;

	//AddOp
	void visitAddOp(AddOp* p) override;
	void visitPlusOp(PlusOp* p) override;
	void visitMinusOp(MinusOp* p) override;

	//MulOp
	void visitMulOp(MulOp* p) override;
	void visitTimesOp(TimesOp* p) override;
	void visitDivideOp(DivideOp* p) override;
	void visitModuloOp(ModuloOp* p) override;

	//BaseValues
	void visitChar(Char x) override;
	void visitDouble(Double x) override;
	void visitIdent(Ident x) override;
	void visitInteger(Integer x) override;
	void visitString(String x) override;

private:
	//Note kolumny to sa atrybuty, a wiersze to poszcegolni bracia
	using select_result = std::tuple<std::string, expr_result>;
	using stmt_result = std::list<select_result>;
	using order_result = std::vector<std::tuple<nullable_value, int>>;	//na koncu numer tego pola tutaj
	using parse_state = std::tuple<bool, size_t, std::vector<AttributesMap>>;//!< lista map atrybutow z obecnego poziomu - instalowana kwerenda instaluje atrybuty u gory

	//interpret state
	parse_state  save_interpret_state() { return state_; }
	void restore_interpret_state(parse_state state) { state_ = state; }

	const std::vector<AttributesMap> const_attr_map_;
	parse_state state_;

	//interpret result
	std::string query_name_;						//!< nazwa obecnie instalowanej kwerendy
	query_result_type attributes_;


	// middle values

	std::list<stmt_result> last_stmt_list_result_;	//!< wizytacja ostatniej
	stmt_result last_stmt_result_;					//!< w zasadzie lista select_resultow
	select_result last_select_result_ = std::make_tuple("", one_r(SimpleType::BOOL, false, false));

	std::list<expr_result> last_expr_list_result_;
	expr_result last_expr_result_= one_r(SimpleType::BOOL, false, false);

	OrderOptType last_order_opt_;
	OrderNullsOptType last_order_nulls_opt_;

	SelModOptType last_sel_mod_opt_type_;

	RelOpType last_rel_op_;
	AddOpType last_add_op_;
	MulOpType last_mul_op_;

	std::int64_t last_integer_;
	double last_double_;
	std::string last_ident_;
	std::string last_string_;
};


} /* namespace cloud */
