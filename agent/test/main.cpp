#include <fstream>
#include <iostream>

#include "agent.h"
#include "log_utils.h"
#include "packets.h"
#include "Parser.H"
#include "interpreter.h"
#include "value_visitors.h"
#include "zone.h"

#undef  LOG_DEFAULT_CATEGORY
#define LOG_DEFAULT_CATEGORY PARSER

template<typename Packet>
void test_serialization(Packet packet)
{
	LOG("test_serialization: " << packet);
	cloud::PacketData pd = cloud::serializePacketSetTypeAndAddTimeStampOffset<Packet>(packet);
	LOG("deserializing" LG(pd.size_));
	cloud::PacketData offset_included_pd;
	offset_included_pd.data_ = pd.data_ + cloud::PacketData::kPacketOffset;
	offset_included_pd.size_ = pd.size_ - cloud::PacketData::kPacketOffset;
	Packet deserialized = cloud::deserializePacket<Packet>(offset_included_pd);
	LOG("deserialized: " << deserialized);
	delete[] pd.data_;
}


int main(int argc, char* argv[])
{
#ifdef TEST_INTERPRETERA
	std::istream *is;
	if (argc > 1) {
		is = new std::ifstream(argv[1]);
	} else {
		is = &(std::cin);
	}
	cloud::Zone *z = cloud::getTestRootZone_TEST();
	for (std::string line; std::getline(*is, line); ) {
		LOG("instaljemy" LG(line));
		Program* p = pProgram( line.c_str() );
		if (p) {
			std::cout << z->installQueryInAllZones_TEST(p);

		} else {
			LOG("unable to parse query");
		}
	}
	delete z;
#endif
	std::set<cloud::value> l{true, 2.0, cloud::contact(0x7f000001, 9696)};

	std::map<std::string, int> aa{ {"a", 2}, {"b", 3} };

	boost::asio::io_service io_serv;
	cloud::Agent a(io_serv);

	cloud::value v = l;
	cloud::time t(1000);
	cloud::duration d(1000);
	cloud::duration nd(-11000);
	cloud::contact contact(cloud::localhost, 1111);
	LOG("time " << cloud::time::epoch() << ", " << v << " now: " << cloud::time::now());
	LOG("time " << t << " " << t.from_string(t.to_string()) << " " << t);
	LOG("d " << d << " " << d.from_string(d.to_string()) << " " << d);
	LOG("nd " << nd << " " << nd.from_string(nd.to_string()) << " " << nd);
	LOG("contact " << contact << " after conv " << cloud::contact_from_string_throw(cloud::contact_to_string(contact)));
	try {
		{
			cloud::value b = true;
			cloud::value b1 = false;
			auto new_b = boost::apply_visitor(cloud::not_value_operator(), b);
			LOG("normal visitor " << cloud::get_value_wrapper<bool>(new_b));
			auto op_b = boost::apply_visitor(cloud::or_operator(b), b1);

			LOG("logic_value_operator visitor " << cloud::get_value_wrapper<bool>(op_b) << "," << cloud::get_value_wrapper<bool>(b));
		}
	} catch (std::exception &e) {
		LOG("nie udalo sie oszukac " << e.what());
	}

	std::string str("aaaaaaaa");
	CryptoPP::SecByteBlock block;
	block.Assign(reinterpret_cast<const unsigned char*>(str.data()), str.size());
	cloud::serialization::signature_wrapper sw(block);
	LOG("are signatures equal " << (sw.toSignature() == block));

	test_serialization(cloud::Packet_QuerySignRequest(0, true, cloud::time::now(), "query"));
	test_serialization(cloud::Packet_QuerySignResponse(0, cloud::time::now(), cloud::time::now(), cloud::serialization::signature_wrapper(block)));


	test_serialization(cloud::Packet_SetFallbackContactsRequest(0, std::set<cloud::contact>{}));
	test_serialization(cloud::Packet_SetFallbackContactsRequest(0, std::set<cloud::contact>{contact, contact, contact}));

	test_serialization(cloud::Packet_GetKeptZoneNamesResponse(0, cloud::ZmiFreshness{}, std::list<cloud::serialization::query_wrapper>{}));
	test_serialization(cloud::Packet_GetKeptZoneNamesResponse(2,
		cloud::ZmiFreshness{
			{"zone", std::map<std::string, cloud::time> { {"zone", cloud::time::now()} } }
		},
		std::list<cloud::serialization::query_wrapper>{
			cloud::serialization::query_wrapper("query", true, cloud::time::now(), cloud::time::now(), sw)
		}
	));

	try {
	test_serialization(cloud::Packet_FetcherData(std::list<cloud::serialization::Attribute>{}));
	test_serialization(cloud::Packet_FetcherData(std::list<cloud::serialization::Attribute>{
		cloud::serialization::Attribute("name", cloud::bool_vt, nullptr),
		cloud::serialization::Attribute("name", cloud::bool_vt, new cloud::serialization::ValueBool(true)),
		cloud::serialization::Attribute("name", cloud::bool_vt, new cloud::serialization::ValueContact(contact)),
		cloud::serialization::Attribute("name", cloud::bool_vt, new cloud::serialization::ValueDuration(d)),
		cloud::serialization::Attribute("name", cloud::bool_vt, new cloud::serialization::ValueDouble(0.0)),
		cloud::serialization::Attribute("name", cloud::bool_vt, new cloud::serialization::ValueInteger(1)),
		cloud::serialization::Attribute("name", cloud::bool_vt, new cloud::serialization::ValueString("false")),
		cloud::serialization::Attribute("name", cloud::bool_vt, new cloud::serialization::ValueTime(t)),
		cloud::serialization::Attribute("name", cloud::bool_vt, new cloud::serialization::ValueList(
				std::list<cloud::serialization::Value*>{new cloud::serialization::ValueTime(t), new cloud::serialization::ValueTime(t)}
			)),
		cloud::serialization::Attribute("name", cloud::bool_vt, new cloud::serialization::ValueSet(
				std::list<cloud::serialization::Value*>{new cloud::serialization::ValueTime(t), new cloud::serialization::ValueTime(t)}
			))
	}));
	} catch (std::exception &e) {
		LOG("deserializing errororo " << e.what());
	}
	{
		cloud::PacketData pd;
		try {
			pd = cloud::serializePacketSetTypeAndAddTimeStampOffset<cloud::Packet_QuerySignRequest>(cloud::Packet_QuerySignRequest(0, true, cloud::time::now(), "query"));
			LOG("deserializing" LG(pd.size_));
			cloud::Packet_FetcherData deserialized = cloud::deserializePacket<cloud::Packet_FetcherData>(pd);
			LOG("deserialized: " << deserialized);
		} catch (std::exception &e) {
			LOG("deserializing errororo " << e.what());
		}
		delete [] pd.data_;
	}
	return 0;
}
