#include "serialization.h"

#include <boost/serialization/export.hpp>

namespace cloud {
namespace serialization {

#define GENERATE_CLOUD_SERIALIZATION_SIMPLE_CLEAR(Name) CAT(Value, Name)::~CAT(Value, Name)() {}
//#define GENERATE_CLOUD_SERIALIZATION_SIMPLE_CLEAR(Name) void CAT(Value, Name)::clear() { LOG("deserializing Value" << #Name); }

GENERATE_CLOUD_SERIALIZATION_SIMPLE_CLEAR(Bool)
GENERATE_CLOUD_SERIALIZATION_SIMPLE_CLEAR(Contact)
GENERATE_CLOUD_SERIALIZATION_SIMPLE_CLEAR(Double)
GENERATE_CLOUD_SERIALIZATION_SIMPLE_CLEAR(Duration)
GENERATE_CLOUD_SERIALIZATION_SIMPLE_CLEAR(Integer)
GENERATE_CLOUD_SERIALIZATION_SIMPLE_CLEAR(String)
GENERATE_CLOUD_SERIALIZATION_SIMPLE_CLEAR(Time)

ValueList::~ValueList() {
	for (auto v: value_) {
		delete v;
	}
	value_.clear();
}

ValueSet::~ValueSet() {
	for (auto v: value_) {
		delete v;
	}
	value_.clear();
}

} /* namespace serialization */


Attribute convertToCloudAttribute(const serialization::Attribute &attribute)
{
	return Attribute(attribute.name_, attribute.type_, convertToCloudValue(attribute.type_, attribute.value_));
}

serialization::Attribute convertToSerializableAttribute(const Attribute &attribute)
{
	return serialization::Attribute(attribute.name_, attribute.type_, convertToSerializableValue(attribute.value_));
}

nullable_value convertToCloudValue(const value_type &vt, serialization::Value* val)
{
	if (!val) {
		return nullable_value(false, get_empty_value(vt));
	}
	if (vt.isSimpleType()) {
		switch (vt.type_) {
#define GENERATE_CLOUD_SERIALIZATION_TO_SIMPLE_CLOUD_VALUE_CONVERSION(Name, Enum) \
		case cloud::SimpleType::Enum: \
		{ \
			serialization:: CAT(Value, Name) * dyn_val = dynamic_cast<serialization:: CAT(Value, Name) *>(val); \
			if (!dyn_val) { \
				LOG("nie udalo sie scastowac do" << #Name); \
				throw std::runtime_error("nie udalo sie scastowac"); \
			} else { \
				return nullable_value(dyn_val->value_); \
			} \
		} \
			break \

		GENERATE_CLOUD_SERIALIZATION_TO_SIMPLE_CLOUD_VALUE_CONVERSION(Bool, BOOL);
		GENERATE_CLOUD_SERIALIZATION_TO_SIMPLE_CLOUD_VALUE_CONVERSION(Contact, CONTACT);
		GENERATE_CLOUD_SERIALIZATION_TO_SIMPLE_CLOUD_VALUE_CONVERSION(Double, DOUBLE);
		GENERATE_CLOUD_SERIALIZATION_TO_SIMPLE_CLOUD_VALUE_CONVERSION(Duration, DURATION);
		GENERATE_CLOUD_SERIALIZATION_TO_SIMPLE_CLOUD_VALUE_CONVERSION(Integer, INTEGER);
		GENERATE_CLOUD_SERIALIZATION_TO_SIMPLE_CLOUD_VALUE_CONVERSION(String, STRING);
		GENERATE_CLOUD_SERIALIZATION_TO_SIMPLE_CLOUD_VALUE_CONVERSION(Time, TIME);
		}
	} else {
		switch (vt.collection_.front()) {
		case CollectionType::LIST:
		{
			serialization::ValueList * dyn_val = dynamic_cast<serialization::ValueList*>(val);
			if (!dyn_val) {
				LOG("nie udalo sie scastowac do ValueList");
				throw std::runtime_error("nie udalo sie scastowac");
			} else {
				std::list<value> lv;
				for (auto v: dyn_val->value_) {
					auto sub_val = convertToCloudValue(vt.strip_collection_layer(), v);
					if (sub_val.is_null()) {
						LOG("wewnetrzne wartosci listy nie moga byc puste ValueList");
						throw std::runtime_error("wewnetrzne wartosci listy nie moga byc puste ValueList");
					} else {
						lv.push_back(sub_val.val);
					}
				}
				return nullable_value(lv);
			}
		}
			break;

		case CollectionType::SET:
		{
			serialization::ValueSet * dyn_val = dynamic_cast<serialization::ValueSet*>(val);
			if (!dyn_val) {
				LOG("nie udalo sie scastowac do ValueSet");
				throw std::runtime_error("nie udalo sie scastowac ValueSet");
			} else {
				std::set<value> sv;
				for (auto v: dyn_val->value_) {
					auto sub_val = convertToCloudValue(vt.strip_collection_layer(), v);
					if (sub_val.is_null()) {
						LOG("wewnetrzne wartosci listy nie moga byc puste ValueSet");
						throw std::runtime_error("wewnetrzne wartosci listy nie moga byc puste ValueSet");
					} else {
						sv.insert(sub_val.val);
					}
				}
				if (sv.size() != dyn_val->value_.size()) {
					LOG("w zbiorze byly duplikaty ValueSet");
					throw std::runtime_error("zbiorze byly duplikaty ValueSet");
				}
				return nullable_value(sv);
			}
		}
			break;
		}
	}
	return nullable_value();
}

struct convert_to_serializable_value_visitor: boost::static_visitor<serialization::Value*>
{
	serialization::Value* operator() (const bool &v) const {
		return new serialization::ValueBool(v);
	}

	serialization::Value* operator() (const contact &v) const {
		return new serialization::ValueContact(v);
	}

	serialization::Value* operator() (const double &v) const {
		return new serialization::ValueDouble(v);
	}

	serialization::Value* operator() (const duration &v) const {
		return new serialization::ValueDuration(v);
	}

	serialization::Value* operator() (const integer &v) const {
		return new serialization::ValueInteger(v);
	}
	serialization::Value* operator() (const string &v) const {
		return new serialization::ValueString(v);
	}

	serialization::Value* operator() (const time &v) const {
		return new serialization::ValueTime(v);
	}

	serialization::Value* operator() (const list<value> &l) const {
		std::list<serialization::Value*> lv;
		for (auto v: l) {
			lv.push_back(boost::apply_visitor(convert_to_serializable_value_visitor(), v));
		}
		return new serialization::ValueList(lv);
	}

	serialization::Value* operator() (const set<value> &s) const {
		std::list<serialization::Value*> sv;
		for (auto v: s) {
			sv.push_back(boost::apply_visitor(convert_to_serializable_value_visitor(), v));
		}
		return new serialization::ValueSet(sv);
	}
};

serialization::Value* convertToSerializableValue(const nullable_value &val)
{
	if (val.is_null()) {
		return nullptr;
	}
	return boost::apply_visitor(convert_to_serializable_value_visitor(), val.val);
}

} /* namespace cloud */


std::ostream & operator<< (std::ostream &os, const cloud::serialization::serialization_value_type &v)
{
	switch (v) {
	case cloud::serialization::serialization_value_type::Bool:
		os << "bool";
		break;
	case cloud::serialization::serialization_value_type::Contact:
		os << "contact";
		break;
	case cloud::serialization::serialization_value_type::Double:
		os << "double";
		break;
	case cloud::serialization::serialization_value_type::Duration:
		os << "duration";
		break;
	case cloud::serialization::serialization_value_type::Integer:
		os << "integer";
		break;
	case cloud::serialization::serialization_value_type::String:
		os << "string";
		break;
	case cloud::serialization::serialization_value_type::Time:
		os << "time";
		break;
	case cloud::serialization::serialization_value_type::List:
		os << "list";
		break;
	case cloud::serialization::serialization_value_type::Set:
		os << "set";
		break;
	}
	return os;
}

BOOST_CLASS_EXPORT_GUID(cloud::serialization::ValueBool, "cloud::serialization::ValueBool")
BOOST_CLASS_EXPORT_GUID(cloud::serialization::ValueContact, "cloud::serialization::ValueContact")
BOOST_CLASS_EXPORT_GUID(cloud::serialization::ValueDouble, "cloud::serialization::ValueDouble")
BOOST_CLASS_EXPORT_GUID(cloud::serialization::ValueDuration, "cloud::serialization::ValueDuration")
BOOST_CLASS_EXPORT_GUID(cloud::serialization::ValueInteger, "cloud::serialization::ValueInteger")
BOOST_CLASS_EXPORT_GUID(cloud::serialization::ValueString, "cloud::serialization::ValueString")
BOOST_CLASS_EXPORT_GUID(cloud::serialization::ValueTime, "cloud::serialization::ValueTime")
BOOST_CLASS_EXPORT_GUID(cloud::serialization::ValueList, "cloud::serialization::ValueList")
BOOST_CLASS_EXPORT_GUID(cloud::serialization::ValueSet, "cloud::serialization::ValueSet")

std::ostream & operator<< (std::ostream &os, const cloud::serialization::Value &v)
{
	v.write_to_ostream(os);
	return os;
}

std::ostream & operator<< (std::ostream &os, const cloud::serialization::Value* v)
{
	if (v == nullptr) {
		os << "nullptr";
	} else {
		v->write_to_ostream(os);
	}
	return os;
}

std::ostream & operator<< (std::ostream &os, const cloud::serialization::Attribute &v)
{
	os << v.name_ << ": " << v.type_ << " = " << v.value_;
	return os;
}


std::ostream & operator<< (std::ostream &os, const cloud::serialization::query_wrapper &v)
{
	os << "query[" << v.query_<< "," << v.install_ << "," << v.validity_ << "," << v.signer_timestamp_ << "," << v.signature_.value_.size() << "]";
	return os;
}
