#include "packets.h"

#include <ostream>

#include "log_utils.h"


std::ostream & operator << (std::ostream &os, const cloud::Packet_FetcherData &p)
{
	os << "Packet_FetcherData[" << p.attributes_ << "]";
	return os;
}

std::ostream & operator << (std::ostream &os, const cloud::Packet_QuerySignRequest &p)
{
	os << "Packet_QuerySignRequest[" << p.id_ << "," << p.install_ << "," << p.validity_ << "," << p.query_ << "]";
	return os;
}

std::ostream & operator << (std::ostream &os, const cloud::Packet_QuerySignResponse &p)
{
	os << "Packet_QuerySignResponse[" << p.id_ << "," << p.validity_ << "," << p.signer_timestamp_ << "," << p.signature_.value_.size() << "]";
	return os;
}

std::ostream & operator << (std::ostream &os, const cloud::Packet_SetFallbackContactsRequest &p)
{
	os << "Packet_SetFallbackContactsRequest[" << p.id_ << "," << p.contacts_ << "]";
	return os;
}

std::ostream & operator << (std::ostream &os, const cloud::Packet_SetFallbackContactsResponse &p)
{
	os << "Packet_SetFallbackContactsResponse[" << p.id_ << "," << p.result_ << "]";
	return os;
}

std::ostream & operator << (std::ostream &os, const cloud::Packet_GetKeptZoneNamesRequest &p)
{
	os << "Packet_GetKeptZoneNamesRequest[" << p.id_ << "]";
	return os;
}

std::ostream & operator << (std::ostream &os, const cloud::Packet_GetKeptZoneNamesResponse &p)
{
	os << "Packet_GetKeptZoneNamesResponse[" << p.id_ << "," << p.zones_ << "," << p.queries_ << "]";
	return os;
}

std::ostream & operator << (std::ostream &os, const cloud::Packet_GetZoneAttributesRequest &p)
{
	os << "Packet_GetZoneAttributesRequest[" << p.id_  << "," << p.requested_zones_ << "]";
	return os;
}

std::ostream & operator << (std::ostream &os, const cloud::Packet_GetZoneAttributesResponse &p)
{
	os << "Packet_GetZoneAttributesResponse[" << p.id_ << "," << p.zone_ << "," << p.timestamp_ << "," << p.attributes_ << "]";
	return os;
}

std::ostream & operator << (std::ostream &os, const cloud::Packet_SetSingletonZoneAttributeRequest &p)
{
	os << "Packet_SetSingletonZoneAttributeRequest[" << p.id_ << "," << p.attributes_ << "]";
	return os;
}

std::ostream & operator << (std::ostream &os, const cloud::Packet_SetSingletonZoneAttributeResponse &p)
{
	os << "Packet_SetSingletonZoneAttributeResponse[" << p.id_ << "," << p.result_ << "]";
	return os;
}

std::ostream & operator << (std::ostream &os, const cloud::Packet_InstallQueryRequest &p)
{
	os << "Packet_InstallQueryRequest[" << p.id_ << "," << p.query_ << "]";
	return os;
}

std::ostream & operator << (std::ostream &os, const cloud::Packet_InstallQueryResponse &p)
{
	os << "Packet_InstallQueryResponse[" << p.id_ << "," << p.result_<< "]";
	return os;
}

std::ostream & operator<< (std::ostream &os, const cloud::Packet_StartGossip &p)
{
	os << "Packet_StartGossip["<< p.id_ << "," << p.zones_ << "," << p.queries_ << "]";
	return os;
}

std::ostream & operator<< (std::ostream &os, const cloud::Packet_GossipReply &p)
{
	os << "Packet_GossipReply["<< p.id_ << "," << p.last_my_rcv_timestamp_ << "," << p.last_their_snd_timestamp_ << "," << p.zones_ << "," << p.queries_ << "]";
	return os;
}

std::ostream & operator<< (std::ostream &os, const cloud::Packet_GossipZoneAttributesRequest &p)
{
	os << "Packet_GetZoneAttributesRequest[" << p.id_  << "," << p.last_my_rcv_timestamp_ << "," << p.last_their_snd_timestamp_<< "," << p.requested_zones_ << "]";
	return os;
}

std::ostream & operator << (std::ostream &os, const cloud::PacketType &p)
{
	switch (p) {
	case cloud::PacketType::FetcherData:
		os << "FetcherData";
		break;

	case cloud::PacketType::QuerySignRequest:
		os << "QuerySignRequest";
		break;

	case cloud::PacketType::QuerySignResponse:
		os << "QuerySignResponse";
		break;

	case cloud::PacketType::SetFallbackContactsRequest:
		os << "SetFallbackContactsRequest";
		break;

	case cloud::PacketType::SetFallbackContactsResponse:
		os << "SetFallbackContactsResponse";
		break;

	case cloud::PacketType::GetKeptZoneNamesRequest:
		os << "GetKeptZoneNamesRequest";
		break;

	case cloud::PacketType::GetKeptZoneNamesResponse:
		os << "GetKeptZoneNamesResponse";
		break;

	case cloud::PacketType::GetZoneAttributesRequest:
		os << "GetZoneAttributesRequest";
		break;

	case cloud::PacketType::GetZoneAttributesResponse:
		os << "GetZoneAttributesResponse";
		break;

	case cloud::PacketType::SetSingletonZoneAttributeRequest:
		os << "SetSingletonZoneAttributeRequest";
		break;

	case cloud::PacketType::SetSingletonZoneAttributeResponse:
		os << "SetSingletonZoneAttributeResponse";
		break;

	case cloud::PacketType::InstallQueryRequest:
		os << "InstallQueryRequest";
		break;

	case cloud::PacketType::InstallQueryResponse:
		os << "InstallQueryResponse";
		break;

	case cloud::PacketType::StartGossip:
		os << "StartGossip";
		break;

	case cloud::PacketType::GossipReply:
		os << "GossipReply";
		break;

	case cloud::PacketType::GossipZoneAttributesRequest:
		os << "GossipZoneAttributesRequest";
		break;
	};
	return os;
}
