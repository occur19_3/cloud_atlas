#include "client.h"

#include "log_utils.h"
#include "Parser.H"

#undef  LOG_DEFAULT_CATEGORY
#define LOG_DEFAULT_CATEGORY CLIENT

namespace cloud {

Client::Client(boost::asio::io_service &io_serv): io_serv_(io_serv), network_(io_serv, [this] (NetworkManager::RcvData && p) { packet_arrived((std::move(p))); })
{}

bool Client::start(std::uint16_t port, const std::string &public_key_file, const std::string &agent_host, std::uint16_t agent_port, const std::string &query_signer_host, std::uint16_t query_signer_port)
{
	LOG("starting client" LG(port, public_key_file, agent_host, agent_port, query_signer_host, query_signer_port));
	if (!agent_.fromString(agent_host, agent_port) || !agent_.isSet()) {
		LOG(ERROR, "Invalid agent host address or port" LG(agent_host, agent_port));
		return false;
	}
	if (!query_signer_.fromString(query_signer_host, query_signer_port)|| !query_signer_.isSet()) {
		LOG(ERROR, "Invalid agent host address or port" LG(query_signer_host, query_signer_port));
		return false;
	}
	if (!network_.start(port)) {
		LOG(ERROR, "cannot start network" LG(port));
		return false;
	}
	if (!load_public_key(public_key_file)) {
		LOG(ERROR, "problem with loading query signer public key" LG(public_key_file));
		return false;
	}

	io_serv_.run();
	LOG("finishing");
	return true;
}

void Client::stop()
{
	LOG("stopping");
	network_.stop();
	io_serv_.stop();
	LOG("stopped");
}

std::int64_t Client::startInstallQuery(const std::string &query, bool install, time validity, const std::function<void(std::int64_t, bool, std::string&&, std::list<QueryInfo>&&)> &callback)
{
	LOG("startInstallQuery" LG(query, install, validity));
	auto id = getRandomId();
	io_serv_.post([this, id, query, install, validity, callback] () {
		InstallQueryData data;
		data.state_ = InstallQueryData::state::WAITING_FOR_SIGNER;
		data.id_ = id;
		data.query_ = query;
		data.install_ = install;
		data.validity_ = validity;
		data.callback_ = callback;

		Packet_QuerySignRequest req(data.id_, install, validity, query);
		PacketData pd = serializePacketSetTypeAndAddTimeStampOffset(req);
		LOG("startInstallQuery sending packet" LG(req));
		network_.push_to_udp_send_queue(query_signer_, std::move(pd));
		insertIntoMap(query_installs_, data.id_, std::move(data));
		LOG("" LG(query_installs_));
	});
	return id;
}

std::int64_t Client::startSetFallBackContacts(const std::set<contact> &contacts, const std::function<void(std::int64_t, bool)> &callback)
{
	LOG("startSetFallBackContacts" LG(contacts));
	auto id = getRandomId();
	io_serv_.post([this, id, contacts, callback] () {
		SetFallbackContactsData data;
		data.id_ = id;
		data.callback_ = callback;

		Packet_SetFallbackContactsRequest req(id, contacts);
		PacketData pd = serializePacketSetTypeAndAddTimeStampOffset(req);
		LOG("startSetFallBackContacts sending packet" LG(req));
		network_.push_to_udp_send_queue(agent_, std::move(pd));
		insertIntoMap(set_fallback_contacts_requests_, data.id_, std::move(data));
		LOG("" LG(set_fallback_contacts_requests_));
	});
	return id;
}

std::int64_t Client::updateZoneData(const zone_path& path, const std::function<void(std::int64_t, ZoneData&&)> &callback)
{
	LOG("updateZoneData" LG(path));
	auto id = getRandomId();
	io_serv_.post([this, id, path, callback] () {
		UpdateZoneData data;
		data.state_ = UpdateZoneData::state::WAITING_FOR_KEPT_ATTRIBUTES;
		data.id_ = id;
		data.path_ = path;
		data.callback_ = callback;

		Packet_GetKeptZoneNamesRequest req(id);
		PacketData pd = serializePacketSetTypeAndAddTimeStampOffset(req);
		LOG("updateZoneData sending packet" LG(req));
		network_.push_to_udp_send_queue(agent_, std::move(pd));
		insertIntoMap(update_zone_data_requests_, data.id_, std::move(data));
		LOG("" LG(update_zone_data_requests_));
	});
	return id;
}

std::int64_t Client::getAttributeValue(const zone_path &path, const std::string &name, const std::function<void(std::int64_t, std::list<Attribute>)> &callback)
{
	LOG("getAttributeValue" LG(path, name));
	auto id = getRandomId();
	io_serv_.post([this, id, path, name, callback] () {
		GetAttributeValueData data;
		data.id_ = id;
		data.name_ = name;
		data.callback_ = callback;

		Packet_GetZoneAttributesRequest req(id, std::set<zone_path>{path});
		PacketData pd = serializePacketSetTypeAndAddTimeStampOffset(req);
		LOG("getAttributeValue sending packet" LG(req));
		network_.push_to_udp_send_queue(agent_, std::move(pd));
		insertIntoMap(get_attribute_value_requests_, data.id_, std::move(data));
		LOG("" LG(get_attribute_value_requests_));
	});
	return id;
}

std::int64_t Client::setAttributeValue(const zone_path &path, const Attribute &attr, const std::function<void(std::int64_t, bool result)> &callback)
{
	LOG("setAttributeValue" LG(path, attr));
	auto id = getRandomId();
	io_serv_.post([this, id, path, attr, callback] () {
		SetAttributeValueData data;
		data.id_ = id;
		data.callback_ = callback;

		Packet_SetSingletonZoneAttributeRequest req(id, std::list<serialization::Attribute>{convertToSerializableAttribute(attr)} );
		PacketData pd = serializePacketSetTypeAndAddTimeStampOffset(req);
		LOG("setAttributeValue sending packet" LG(req));
		network_.push_to_udp_send_queue(agent_, std::move(pd));
		insertIntoMap(set_attribute_value_requests_, data.id_, std::move(data));
		LOG("" LG(set_attribute_value_requests_));
	});
	return id;
}

bool Client::load_public_key(const std::string &public_key_file)
{
	return cloud::read_from_file(public_key_file, pk_);
}

void Client::packet_arrived(NetworkManager::RcvData &&packet)
{	LOG("packet_arrived " LG(packet));
	try {
		switch (packet.type) {
		case PacketType::QuerySignResponse:
		{
			Packet_QuerySignResponse p = deserializePacket<Packet_QuerySignResponse>(packet.data);
			processQuerySignResponse(packet, p.id_, p.validity_, p.signer_timestamp_, p.signature_.toSignature());
		}
			break;

		case PacketType::SetFallbackContactsResponse:
		{
			Packet_SetFallbackContactsResponse p = deserializePacket<Packet_SetFallbackContactsResponse>(packet.data);
			LOG("przetwarzam pakiet" LG(p));
			processSetFallbackContactsResponse(packet, p.id_, p.result_);
		}
			break;

		case PacketType::GetKeptZoneNamesResponse:
		{
			Packet_GetKeptZoneNamesResponse p = deserializePacket<Packet_GetKeptZoneNamesResponse>(packet.data);
			LOG("przetwarzam pakiet" LG(p));
			processGetKeptZoneNamesResponse(packet, p.id_, p.zones_, p.queries_);
		}
			break;

		case PacketType::GetZoneAttributesResponse:
		{
			Packet_GetZoneAttributesResponse p = deserializePacket<Packet_GetZoneAttributesResponse>(packet.data);
			LOG("przetwarzam pakiet" LG(p));
			std::list<Attribute> attributes;
			for (auto a: p.attributes_) {
				attributes.push_back(convertToCloudAttribute(a));
			}
			processGetZoneAttributesResponse(packet, p.id_, p.zone_, p.timestamp_, attributes);
		}
			break;

		case PacketType::SetSingletonZoneAttributeResponse:
		{
			Packet_SetSingletonZoneAttributeResponse p = deserializePacket<Packet_SetSingletonZoneAttributeResponse>(packet.data);
			LOG("przetwarzam pakiet" LG(p));
			processSetSingletonZoneAttributeResponse(packet, p.id_, p.result_);
		}
			break;

		case PacketType::InstallQueryResponse:
		{
			Packet_InstallQueryResponse p = deserializePacket<Packet_InstallQueryResponse>(packet.data);
			LOG("przetwarzam pakiet" LG(p));
			processInstallQueryResponse(packet, p.id_, p.result_);
		}
			break;

		default:
			LOG("unknown packet" LG(packet));
			break;
		}
	} catch (std::exception &e) {
		LOG(WARN, "packet_processing error" LG(e.what()));
	}
	delete[] packet.data.data_;
}

void Client::processQuerySignResponse(const NetworkManager::RcvData &rcv_data, std::int64_t id, time validity, time timestamp, const Signature &signature)
{
	LOG("processQuerySignResponse" LG(rcv_data, id));
	auto iter = query_installs_.find(id);
	LOG(TRACE, "co sie wydarzylo" LG(*iter, query_installs_, iter->second.id_));
	if (iter == query_installs_.end()) {
		LOG("nie moge znalez instalacji query");
		LOG("" LG(query_installs_));
		return;
	}
	if (iter->second.state_ != InstallQueryData::state::WAITING_FOR_SIGNER) {
		LOG("juz otrzymalizmy podpis dla tego query");
		return;
	}
	iter->second.signature_ = signature;
	iter->second.validity_ = validity;
	iter->second.timestamp_ = timestamp;

	bool result = verifySignature(iter->second.query_, iter->second.install_, iter->second.validity_, iter->second.timestamp_, iter->second.signature_, pk_);
	if (!result) {
		LOG("nie dostalismy podpisu smuteczeg :<<<");
		iter->second.callback_(id, false, "QuerySigner nie podpisal query", std::move(std::list<QueryInfo>()));
		query_installs_.erase(iter);
		return;
	}

	iter->second.state_ = InstallQueryData::state::WAITING_FOR_AGENT;

	serialization::query_wrapper query_w(iter->second.query_, iter->second.install_, iter->second.validity_, iter->second.timestamp_, iter->second.signature_);
	LOG(TRACE, "co sie wydarzylo" LG(*iter, query_installs_, iter->second.id_));
	Packet_InstallQueryRequest req(iter->second.id_, query_w);
	PacketData pd = serializePacketSetTypeAndAddTimeStampOffset<Packet_InstallQueryRequest>(req);
	LOG("Probujemy teraz zainstalowac query u klienta" LG(req));
	network_.push_to_udp_send_queue(agent_, std::move(pd));
}

void Client::processSetFallbackContactsResponse(const NetworkManager::RcvData &, std::int64_t id, bool result)
{
	LOG("processSetFallbackContactsResponse" LG(id, result));
	auto iter = set_fallback_contacts_requests_.find(id);
	if (iter == set_fallback_contacts_requests_.end()) {
		LOG("nie moge znalez instalacji query" LG(id, set_fallback_contacts_requests_));
		return;
	}
	iter->second.callback_(id, result);
	set_fallback_contacts_requests_.erase(iter);
}

void Client::processGetKeptZoneNamesResponse(const NetworkManager::RcvData &rcv_data, std::int64_t id, const ZmiFreshness &zones, const std::list<serialization::query_wrapper> &queries)
{	
	LOG("processGetKeptZoneNamesResponse" LG(id, zones, queries));
	auto qiter = query_installs_.find(id);
	if (qiter != query_installs_.end()) {
		LOG("processGetKeptZoneNamesResponse dotyczy query_installs_" LG(id, *qiter));
		if (qiter->second.state_ != InstallQueryData::state::WAITING_FOR_QUERIES) {
			LOG("processGetKeptZoneNamesResponse nie czekamy na ten pakiet" LG(id, *qiter));
			return;
		}

		std::list<QueryInfo> query_infos;
		for (auto q: queries) {
			query_infos.push_back(std::make_tuple(q.query_, q.install_, q.validity_));
		}
		qiter->second.callback_(id, true, "Query Install Success", std::move(query_infos));
		query_installs_.erase(qiter);
		return;
	}
	auto uiter = update_zone_data_requests_.find(id);
	if (uiter != update_zone_data_requests_.end()) {
		LOG("processGetKeptZoneNamesResponse dotyczy update_zone_data_requests_" LG(id, *uiter));
		if (uiter->second.state_ != UpdateZoneData::state::WAITING_FOR_KEPT_ATTRIBUTES) {
			LOG("processGetKeptZoneNamesResponse nie czekamy na ten pakiet" LG(id, *uiter));
			return;
		}
		uiter->second.state_ = UpdateZoneData::state::WAITING_FOR_ATTRIBUTES;
		uiter->second.data_.kept_zones_ = zones;
		uiter->second.data_.path_ = getZonePath(zones);
		if (uiter->second.path_.empty()) {
			uiter->second.requested_zones_ = getAllPaths(uiter->second.data_.path_);
		} else {
			uiter->second.requested_zones_ = getAllPaths(uiter->second.path_);
		}
		if (uiter->second.requested_zones_.empty()) {
			uiter->second.callback_(id, std::move(uiter->second.data_));
			update_zone_data_requests_.erase(uiter);
			return;
		}

		Packet_GetZoneAttributesRequest req(id, uiter->second.requested_zones_);
		PacketData pd = serializePacketSetTypeAndAddTimeStampOffset(req);
		LOG("getAttributeValue sending packet" LG(req));
		network_.push_to_udp_send_queue(agent_, std::move(pd));
		return;
	}
	LOG("nie znalazlem id do obslugi processGetKeptZoneNamesResponse" LG(id));
}

void Client::processGetZoneAttributesResponse(const NetworkManager::RcvData &rcv_data, std::int64_t id, const zone_path &zone, time timestamp, const std::list<Attribute> &attributes)
{	
	LOG("processGetZoneAttributesResponse" LG(id, zone, timestamp, attributes));
	auto giter = get_attribute_value_requests_.find(id);
	if (giter != get_attribute_value_requests_.end()) {
		LOG("processGetZoneAttributesResponse dotyczy get_attribute_value_requests_" LG(id, *giter));
		for (auto a: attributes) {
			if (a.name_ == giter->second.name_) {
				giter->second.callback_(id, std::list<Attribute>{a});
				get_attribute_value_requests_.erase(giter);
				return;
			}
		}
		giter->second.callback_(id, std::list<Attribute>());
		get_attribute_value_requests_.erase(giter);
		return;
	}

	auto uiter = update_zone_data_requests_.find(id);
	if (uiter != update_zone_data_requests_.end()) {
		LOG("processGetZoneAttributesResponse dotyczy update_zone_data_requests_" LG(id, *uiter));
		if (uiter->second.state_ != UpdateZoneData::state::WAITING_FOR_ATTRIBUTES) {
			LOG("processGetKeptZoneNamesResponse nie czekamy na ten pakiet" LG(id, *uiter));
			return;
		}
		auto iter = uiter->second.requested_zones_.find(zone);
		if (iter == uiter->second.requested_zones_.end()) {
			LOG("processGetKeptZoneNamesResponse nie czekamy na atrybuty z tej strerfy" LG(id, zone));
			return;
		}
		uiter->second.requested_zones_.erase(iter);
		uiter->second.data_.attrs_.push_back(std::make_pair(zone, attributes));
		if (uiter->second.requested_zones_.empty()) {
			uiter->second.callback_(id, std::move(uiter->second.data_));
			update_zone_data_requests_.erase(uiter);
		}
		return;
	}
	LOG("nie znalazlem id do obslugi processGetKeptZoneNamesResponse" LG(id));

}

void Client::processSetSingletonZoneAttributeResponse(const NetworkManager::RcvData &rcv_data, std::int64_t id, bool result)
{	
	LOG("processSetSingletonZoneAttributeResponse" LG(id, result));
	auto iter = set_attribute_value_requests_.find(id);
	if (iter == set_attribute_value_requests_.end()) {
		LOG("nie moge znalez instalacji query" LG(id, set_attribute_value_requests_));
		return;
	}
	iter->second.callback_(id, result);
	set_attribute_value_requests_.erase(iter);
}

void Client::processInstallQueryResponse(const NetworkManager::RcvData &rcv_data, std::int64_t id, bool result)
{
	LOG("processInstallQueryResponse" LG(rcv_data, id, result));
	auto iter = query_installs_.find(id);
	if (iter == query_installs_.end()) {
		LOG("nie moge znalez instalacji query" LG(id, query_installs_));
		return;
	}
	if (iter->second.state_ != InstallQueryData::state::WAITING_FOR_AGENT) {
		LOG("nie czekamy na odpowiedz na instalacje");
		return;
	}

	if (!result) {
		iter->second.callback_(id, false, "Nie udalo sie nam zainstalowac query u agenta", std::move(std::list<QueryInfo>()));
		query_installs_.erase(iter);
		return;
	}
	iter->second.state_ = InstallQueryData::state::WAITING_FOR_QUERIES;

	Packet_GetKeptZoneNamesRequest req(id);
	PacketData pd = serializePacketSetTypeAndAddTimeStampOffset(req);
	LOG("updateZoneData sending packet" LG(req));
	network_.push_to_udp_send_queue(agent_, std::move(pd));
}

} /* namespace cloud */


std::ostream & operator <<(std::ostream & os, const cloud::InstallQueryData &v)
{
	os << "InstallQueryData[" << v.id_ << ", " << v.query_ << "," << static_cast<int>(v.state_) << "]";
	return os;
}

std::ostream & operator <<(std::ostream & os, const cloud::SetFallbackContactsData &v)
{
	os << "SetFallbackContactsData[" << v.id_ << "]";
	return os;
}

std::ostream & operator <<(std::ostream & os, const cloud::ZoneData &v)
{
	return os;
}

std::ostream & operator <<(std::ostream & os, const cloud::UpdateZoneData &v)
{
	return os;
}

std::ostream & operator <<(std::ostream & os, const cloud::GetAttributeValueData &v)
{
	return os;
}

std::ostream & operator <<(std::ostream & os, const cloud::SetAttributeValueData &v)
{
	return os;
}
