#include "clientmainwindow.h"
#include "ui_clientmainwindow.h"

#include <set>
#include <sstream>

#ifdef CLOUD_COMPILE_VARIANT
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#endif

#include <QCheckBox>
#include <QLabel>
#include <QLineEdit>
#include <QDateTimeEdit>
#include <QPlainTextEdit>
#include <QSpinBox>
#include <QTextEdit>

#include "client.h"
#include "log_utils.h"
#include "utils.h"


#undef  LOG_DEFAULT_CATEGORY
#define LOG_DEFAULT_CATEGORY CLIENT

ClientMainWindow::ClientMainWindow(cloud::Client &client, QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::ClientMainWindow),
	plot_timer_(this),
	client_(client)
{
	ui->setupUi(this);
	connect(this, SIGNAL(signal_queryInstallResponse(std::int64_t, bool, std::string, std::list<cloud::QueryInfo>)), this, SLOT(slot_queryInstallResponse(std::int64_t, bool, std::string, std::list<cloud::QueryInfo>)), Qt::BlockingQueuedConnection);
	connect(this, SIGNAL(signal_setFallbackContactsResponse(std::int64_t, bool)), this, SLOT(slot_SetFallbackContactsResponse(std::int64_t, bool)), Qt::BlockingQueuedConnection);
	connect(this, SIGNAL(signal_updateZoneDataResponse(std::int64_t, cloud::ZoneData)), this, SLOT(slot_updateZoneDataResponse(std::int64_t, cloud::ZoneData)), Qt::BlockingQueuedConnection);
	connect(this, SIGNAL(signal_getAttributeValueResponse(std::int64_t, std::list<cloud::Attribute>)), this, SLOT(slot_getAttributeValueResponse(std::int64_t, std::list<cloud::Attribute>)), Qt::BlockingQueuedConnection);
	connect(&plot_timer_, SIGNAL(timeout()), this, SLOT(getAttributeValue()));
	connect(this, SIGNAL(signal_setPlotText(QString)), ui->plot_values_text_edt, SLOT(setPlainText(QString)));
	ui->query_validity_date_time_edt->setDateTime(QDateTime::currentDateTime().addSecs(10 * 60));
	ui->qcustomplot_widget->addGraph();
	ui->qcustomplot_widget->xAxis->setLabel("periods");
	ui->qcustomplot_widget->yAxis->setLabel("values");
}

void ClientMainWindow::closeEvent(QCloseEvent *event)
{
	LOG("ClientMainWindow::closeEvent");
	client_.stop();
	LOG("ClientMainWindow::stopped");
}

ClientMainWindow::~ClientMainWindow()
{
	delete ui;
}

void ClientMainWindow::slot_queryInstallResponse(std::int64_t id, bool result, std::string message, std::list<cloud::QueryInfo> queries)
{
	LOG("slot_queryInstallResponse" LG(current_install_query_id_, id, result, message, queries));
	if (current_install_query_id_ == id) {
		std::stringstream label_ss;
		label_ss << "Result: " <<  result << " " << message;
		LOG("instalujemy query");
		ui->query_install_status_lbl->setText(QString::fromStdString(label_ss.str()));
		if (result) {
			std::stringstream text_ss;
			text_ss << "Installed_queries:" << std::endl;
			for (auto q: queries) {
				if (std::get<1>(q)) {
					text_ss << std::get<2>(q) << " " << std::get<0>(q) << std::endl;
				}
			}
			ui->installed_queries_text_edt->setPlainText(QString::fromStdString(text_ss.str()));
		}
		current_install_query_id_ = 0;
	} else {
		LOG(WARN, "slot_queryInstallResponse invalid ID" LG(current_install_query_id_, id));
	}
}

void ClientMainWindow::slot_setFallbackContactsResponse(std::int64_t id, bool result)
{
	LOG("slot_queryInstallResponse" LG(current_set_contacts_id_, id, result));
	if (current_set_contacts_id_ == id) {
		std::stringstream ss;
		ss << "Contact installation result: " << result;
		ui->zone_data_status_lbl->setText(QString::fromStdString(ss.str()));
	} else {
		LOG(WARN, "slot_queryInstallResponse invalid ID" LG(current_set_contacts_id_, id));
	}
}

void ClientMainWindow::slot_updateZoneDataResponse(std::int64_t id, cloud::ZoneData zone_data)
{
	LOG("slot_updateZoneDataResponse" LG(current_update_data_id_, id, zone_data));
	if (current_update_data_id_ == id) {
		std::stringstream header_ss;
		header_ss << "Zone name: " << cloud::printZoneName(zone_data.path_);
		ui->header_zone_name_lbl->setText(QString::fromStdString(header_ss.str()));
		std::map<size_t, std::pair<cloud::zone_path, std::list<cloud::Attribute>>> sorted_zones;
		for (auto zone: zone_data.attrs_) {
			cloud::insertIntoMap(sorted_zones, zone.first.size(), zone);
		}

		LOG("zone attribute tree" LG(sorted_zones));
		std::stringstream zone_attrs_ss;
		for (auto zone: sorted_zones) {
			zone_attrs_ss << cloud::printZoneName(zone.second.first) << std::endl;
			for (auto a: zone.second.second) {
				zone_attrs_ss << "    " << a << std::endl;
			}
		}

		ui->zone_attributes_text_edt->setPlainText(QString::fromStdString(zone_attrs_ss.str()));
		std::string prefix;
		std::stringstream kept_zones_ss;

		for (auto zone_level: zone_data.kept_zones_) {
			for (auto zone: zone_level.second) {
				kept_zones_ss << "/" << prefix << zone.first << std::endl;
			}
			if (!zone_level.first.empty()) {
				prefix += zone_level.first + "/";
			}
		}
		ui->kept_zones_text_edt->setPlainText(QString::fromStdString(kept_zones_ss.str()));
	} else {
		LOG(WARN, "slot_updateZoneDataResponse invalid ID" LG(current_update_data_id_, id));
	}
}

void ClientMainWindow::slot_getAttributeValueResponse(std::int64_t id, std::list<cloud::Attribute> a)
{
	LOG("slot_getAttributeValueResponse" LG(current_get_attribute_value_id_, id, a));
	if (current_get_attribute_value_id_ == id) {
		if (a.empty()) {
			ui->plot_status_lbl->setText(tr("nie dostalismy wartosci"));
		} else {
			cloud::Attribute attr = a.front();

			std::stringstream plot_zone_attr_ss;
			plot_zone_attr_ss << "Ostatnio znaleziona wartosc: " << attr;
			ui->plot_status_lbl->setText(QString::fromStdString(plot_zone_attr_ss.str()));

			std::stringstream val_ss;
			val_ss << attr.value_ << std::endl;
			QString text = ui->plot_values_text_edt->document()->toPlainText();
			text += QString::fromStdString(val_ss.str());
			emit signal_setPlotText(text);

			bool is_plotable = false;
			double value = 0.0;
			if (attr.type_.isSimpleType()) {
				switch (attr.type_.type_) {
				case cloud::SimpleType::DOUBLE:
					is_plotable = true;
					if (!attr.value_.is_null()) {
						value = cloud::get_value_wrapper<double>(attr.value_.val);
					}
					break;

				case cloud::SimpleType::INTEGER:
					is_plotable = true;
					if (!attr.value_.is_null()) {
						value = cloud::get_value_wrapper<cloud::integer>(attr.value_.val);
					}
					break;

				default:
					break;
				}
			}
			if (is_plotable) {
				x_plot_vals_.append(++curr_plot_size);
				y_plot_vals.append(value);
			}

			size_t x_size = curr_plot_size < 10 ? 10 : curr_plot_size + 3;
			double min_val, max_val;
			if (y_plot_vals.empty()) {
				min_val = 0.0;
				max_val = 5.0;
			} else {
				min_val	= *std::min_element(y_plot_vals.begin(), y_plot_vals.end());
				min_val = min_val < 0.0 ? (min_val - 1) : 0.0;
				max_val	= *std::max_element(y_plot_vals.begin(), y_plot_vals.end()) + 1.0;
			}
			ui->qcustomplot_widget->graph(0)->setData(x_plot_vals_, y_plot_vals);
			ui->qcustomplot_widget->xAxis->setRange(0, x_size);
			ui->qcustomplot_widget->yAxis->setRange(min_val, max_val);
			ui->qcustomplot_widget->replot();
		}
	} else {
		LOG(WARN, "slot_getAttributeValueResponse invalid ID" LG(current_get_attribute_value_id_, id));
	}
}

void ClientMainWindow::slot_setAttributeValueResponse(std::int64_t id, bool result)
{
	LOG("slot_setAttributeValueResponse" LG(current_set_contacts_id_, id, result));
	if (current_set_contacts_id_ == id) {
		std::stringstream ss;
		ss << "Value installation result: " << result;
		ui->zone_data_status_lbl->setText(QString::fromStdString(ss.str()));
	} else {
		LOG(WARN, "slot_setAttributeValueResponse invalid ID" LG(current_set_attribute_value_id_, id));
	}
}

void ClientMainWindow::on_install_query_btn_clicked()
{
	std::string query = ui->query_line_edt->text().toStdString();
	bool install = ui->query_install_chk->isChecked();
	cloud::time validity = cloud::time(ui->query_validity_date_time_edt->dateTime().toMSecsSinceEpoch());
	current_install_query_id_ = client_.startInstallQuery(query, install, validity, [this] (std::int64_t id, bool result, std::string &&message, std::list<cloud::QueryInfo> &&queries) {
		emit signal_queryInstallResponse(id, result, message, queries);
	});
}

void ClientMainWindow::on_show_zone_btn_clicked()
{
	cloud::zone_path path;
	std::string path_str = ui->show_zone_edt->text().toStdString();
	if (!path_str.empty()) {
		path = cloud::parseZoneName(path_str);
		if (path.empty()) {
			ui->zone_data_status_lbl->setText(tr("Wpisz poprawna sciezke."));
			return;
		}
	}
	current_update_data_id_ = client_.updateZoneData(path, [this] (std::int64_t id, cloud::ZoneData &&zone_data) {
		emit signal_updateZoneDataResponse(id, zone_data);
	});
}

void ClientMainWindow::on_set_contacts_btn_clicked()
{
	std::string contacts_string = ui->set_contacts_edt->text().toStdString();
	std::set<cloud::contact> contacts_set = cloud::contact::setFromString(contacts_string);
	if (contacts_set.empty()) {
		std::stringstream ss;
		ss << "problem z parsowaniem kontaktow";
		ui->zone_data_status_lbl->setText(QString::fromStdString(ss.str()));
		return;
	}
	current_set_contacts_id_ = client_.startSetFallBackContacts(contacts_set, [this] (std::int64_t id, bool result) {
		emit signal_setFallbackContactsResponse(id, result);
	});
}

void ClientMainWindow::on_set_attribute_value_btn_clicked()
{
	cloud::zone_path path;
	std::string path_str = ui->show_zone_edt->text().toStdString();
	if (!path_str.empty()) {
		path = cloud::parseZoneName(path_str);
		if (path.empty()) {
			ui->zone_data_status_lbl->setText(tr("Wpisz poprawna scieżkę."));
			return;
		}
	}
	std::string attr_name_str = ui->set_attribute_name_edt->text().toStdString();
	std::string attr_type_str = ui->set_attribute_type_edt->text().toStdString();
	std::string attr_value_str = ui->set_attribute_value_edt->text().toStdString();
	try {
		cloud::value_type vt(cloud::SimpleType::BOOL);
		if (!vt.fromString(attr_type_str)) {
			ui->zone_data_status_lbl->setText(tr("niepoprawna nazwa typu"));
			return;
		}
		cloud::nullable_value nv;
		if (attr_value_str.empty()) {
			nv = cloud::nullable_value(false, cloud::get_empty_value(vt));
		} else {
			nv = cloud::value_from_string(vt, attr_value_str);
		}
		cloud::Attribute attr(attr_name_str, vt, nv);
		current_set_attribute_value_id_ = client_.setAttributeValue(path, attr, [this] (std::int64_t id, bool result) {
			emit signal_setAttributeValueResponse(id, result);
		});
	} catch (std::exception &e) {
		ui->zone_data_status_lbl->setText(tr(e.what()));
		return;
	}
}

void ClientMainWindow::on_start_btn_clicked()
{
	curr_plot_size = 0;
	x_plot_vals_.clear();
	y_plot_vals.clear();
	ui->plot_values_text_edt->setPlainText(tr(""));
	plot_timer_.stop();
	plot_attribute_ = ui->plot_attribute_edt->text().toStdString();
	plot_path_ = cloud::parseZoneName(ui->plot_zone_edt->text().toStdString());
	if (!plot_attribute_.empty() && !plot_path_.empty() && ui->plot_frequency_sb->value()) {
		plot_timer_.start(ui->plot_frequency_sb->value() * 1000);
	} else {
		ui->plot_status_lbl->setText(tr("Invalid attribute or path or value or frequency"));
		LOG(WARN, "Invalid attribute or path or value" LG(plot_attribute_, plot_path_, ui->plot_frequency_sb->value()));

	}
}

void ClientMainWindow::on_stop_btn_clicked()
{
	plot_timer_.stop();
}

void ClientMainWindow::getAttributeValue()
{
	current_get_attribute_value_id_ = client_.getAttributeValue(plot_path_, plot_attribute_, [this] (std::int64_t id, std::list<cloud::Attribute> a) {
		emit signal_getAttributeValueResponse(id, a);
	});

}

void registerMetaTypes()
{
	qRegisterMetaType<std::int64_t> ("std::int64_t");
	qRegisterMetaType<std::string> ("std::string");
	qRegisterMetaType<std::list<cloud::QueryInfo>> ("std::list<cloud::QueryInfo>");
	qRegisterMetaType<cloud::ZoneData> ("cloud::ZoneData");
	qRegisterMetaType<std::list<cloud::Attribute>> ("std::list<cloud::Attribute>");
}
