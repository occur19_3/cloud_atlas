#ifndef CLIENTMAINWINDOW_H
#define CLIENTMAINWINDOW_H

#include <map>

#include <QMainWindow>
#include <QTimer>

#include "client.h"
#include "qcustomplot.h"
#include "value_types.h"

namespace Ui {
class ClientMainWindow;
}

/** Model dzialania, MainWindow wywoluje post'a na io_service client'a,
 *  podajac callbacki emitujace sygnal z danymi, po koncu jest koniec */
class ClientMainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit ClientMainWindow(cloud::Client &client, QWidget *parent = 0);
	~ClientMainWindow();

	void closeEvent(QCloseEvent *event);

signals:
	void signal_queryInstallResponse(std::int64_t id, bool result, std::string message, std::list<cloud::QueryInfo> queries);
	void signal_setFallbackContactsResponse(std::int64_t id, bool result);
	void signal_updateZoneDataResponse(std::int64_t id, cloud::ZoneData zone_data);
	void signal_getAttributeValueResponse(std::int64_t id, std::list<cloud::Attribute> attributes);
	void signal_setAttributeValueResponse(std::int64_t id, bool result);
	void signal_setPlotText(QString text);

private slots:
	void slot_queryInstallResponse(std::int64_t id, bool result, std::string message, std::list<cloud::QueryInfo> queries);
	void slot_setFallbackContactsResponse(std::int64_t id, bool result);
	void slot_updateZoneDataResponse(std::int64_t id, cloud::ZoneData zone_data);
	void slot_getAttributeValueResponse(std::int64_t id, std::list<cloud::Attribute> attributes);
	void slot_setAttributeValueResponse(std::int64_t id, bool result);

	void on_install_query_btn_clicked();
	void on_show_zone_btn_clicked();
	void on_set_contacts_btn_clicked();
	void on_start_btn_clicked();
	void on_stop_btn_clicked();
	void on_set_attribute_value_btn_clicked();

	void getAttributeValue();


private:

	Ui::ClientMainWindow *ui;

	std::int64_t current_install_query_id_ = 0;
	std::int64_t current_set_contacts_id_ = 0;
	std::int64_t current_update_data_id_ = 0;
	std::int64_t current_get_attribute_value_id_ = 0;
	std::int64_t current_set_attribute_value_id_ = 0;

	QTimer plot_timer_;
	std::string plot_attribute_;
	cloud::zone_path plot_path_;
	size_t curr_plot_size = 0;
	QVector<double> x_plot_vals_, y_plot_vals;


	cloud::Client &client_;
};

void registerMetaTypes();

#endif // CLIENTMAINWINDOW_H
