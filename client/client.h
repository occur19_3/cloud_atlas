#pragma once

#include "crypto.h"
#include "network.h"
//#include  "utils.h"
#include "value_types.h"

namespace cloud {

/* * * * * Operation Structs * * * * */

using QueryInfo = std::tuple<std::string, bool, time>;

struct InstallQueryData {
	enum class state {
		WAITING_FOR_SIGNER,
		WAITING_FOR_AGENT,
		WAITING_FOR_QUERIES,
		FINISHED
	} state_;
	std::int64_t id_;
	std::string query_;
	bool install_;
	time validity_;
	time timestamp_;
	Signature signature_;
	std::function<void(std::int64_t, bool, std::string, std::list<QueryInfo>&&)> callback_;
};

struct SetFallbackContactsData {
	std::int64_t id_;
	std::function<void(std::int64_t, bool)> callback_;
};

struct ZoneData {
	zone_path path_;
	ZmiFreshness kept_zones_;
	std::list<std::pair<zone_path, std::list<Attribute>>> attrs_;
};

struct UpdateZoneData {
	enum class state {
		WAITING_FOR_KEPT_ATTRIBUTES,
		WAITING_FOR_ATTRIBUTES,
		FINISHED
	} state_;
	std::int64_t id_;
	zone_path path_;
	std::set<zone_path> requested_zones_;
	ZoneData data_;
	std::function<void(std::int64_t, ZoneData&&)> callback_;
};

struct GetAttributeValueData {
	std::int64_t id_;
	std::string name_;
	std::function<void(std::int64_t, std::list<Attribute>)> callback_;
};

struct SetAttributeValueData {
	std::int64_t id_;
	std::function<void(std::int64_t, bool)> callback_;
};

/* * * * * Client * * * * */


class Client {
public:
	Client(boost::asio::io_service &io_serv);

	bool start(std::uint16_t port, const std::string &public_key_file, const std::string &agent_host, std::uint16_t agent_port, const std::string &query_signer_host, std::uint16_t query_signer_port);
	void stop();

	/** Metody uzywane przez ClientMainWindow wszstkie sa thread_safe, robia post na io_service */
	std::int64_t startInstallQuery(const std::string &query, bool install, time validity, const std::function<void(std::int64_t, bool, std::string&&, std::list<QueryInfo>&&)> &callback);
	std::int64_t updateZoneData(const zone_path& path, const std::function<void(std::int64_t, ZoneData&&)> &callback);
	std::int64_t startSetFallBackContacts(const std::set<contact> &contacts, const std::function<void(std::int64_t, bool)> &callback);
	std::int64_t getAttributeValue(const zone_path &path, const std::string &name, const std::function<void(std::int64_t, std::list<Attribute>)> &callback);
	std::int64_t setAttributeValue(const zone_path &path, const Attribute &attr, const std::function<void(std::int64_t, bool result)> &callback);

private:
	bool load_public_key(const std::string &public_key_file);
	void packet_arrived(NetworkManager::RcvData &&packet);

	void processQuerySignResponse(const NetworkManager::RcvData &rcv_data, std::int64_t id, time validity, time timestamp, const Signature &signature);
	void processSetFallbackContactsResponse(const NetworkManager::RcvData &, std::int64_t id, bool result);
	void processGetKeptZoneNamesResponse(const NetworkManager::RcvData &rcv_data, std::int64_t id, const ZmiFreshness &zones, const std::list<serialization::query_wrapper> &queries);
	void processGetZoneAttributesResponse(const NetworkManager::RcvData &rcv_data, std::int64_t id, const zone_path &zone, time timestamp, const std::list<Attribute> &attributes);
	void processSetSingletonZoneAttributeResponse(const NetworkManager::RcvData &rcv_data, std::int64_t id, bool result);
	void processInstallQueryResponse(const NetworkManager::RcvData &rcv_data, std::int64_t id, bool result);

	std::map<std::int64_t, InstallQueryData> query_installs_;
	std::map<std::int64_t, SetFallbackContactsData> set_fallback_contacts_requests_;
	std::map<std::int64_t, UpdateZoneData> update_zone_data_requests_;
	std::map<std::int64_t, GetAttributeValueData> get_attribute_value_requests_;
	std::map<std::int64_t, SetAttributeValueData> set_attribute_value_requests_;

	contact agent_, query_signer_;

	boost::asio::io_service &io_serv_;
	NetworkManager network_;
	PublicKey pk_;
};

} /* namespace cloud */

std::ostream & operator <<(std::ostream & os, const cloud::InstallQueryData &v);
std::ostream & operator <<(std::ostream & os, const cloud::SetFallbackContactsData &v);
std::ostream & operator <<(std::ostream & os, const cloud::ZoneData &v);
std::ostream & operator <<(std::ostream & os, const cloud::UpdateZoneData &v);
std::ostream & operator <<(std::ostream & os, const cloud::SetAttributeValueData &v);
