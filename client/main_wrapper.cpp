#include <iostream>

#include <QApplication>

#include <boost/asio/io_service.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/program_options.hpp>
#include <boost/thread/thread.hpp>

#include "client.h"
#include "clientmainwindow.h"
#include "log_utils.h"

#undef  LOG_DEFAULT_CATEGORY
#define LOG_DEFAULT_CATEGORY CLIENT

int main_wrapper(int argc, char* argv[])
{
	registerMetaTypes();
	std::uint16_t port;
	std::string public_key_file;
	std::string agent_host;
	std::uint16_t agent_port;
	std::string query_signer_host;
	std::uint16_t query_signer_port;

	boost::program_options::options_description desc("CloudAtlas Client options");
	desc.add_options()
		("help", "Shows help message")
		("port", boost::program_options::value<std::uint16_t>(&port)->default_value(0), "client socket bind port")
		("public-key-file,p", boost::program_options::value<std::string>(&public_key_file)->default_value("public_key.key"), "public key file")
		("agent-host,h", boost::program_options::value<std::string>(&agent_host)->default_value(cloud::kDefaultCloudHost), "associated agent listen host")
		("agent-port,p", boost::program_options::value<std::uint16_t>(&agent_port)->default_value(cloud::kDefaultCloudAgentPort), "associated agent listen port")
		("query-signer-host", boost::program_options::value<std::string>(&query_signer_host)->default_value(cloud::kDefaultCloudHost), "associated query signer listen host")
		("query-signer-port", boost::program_options::value<std::uint16_t>(&query_signer_port)->default_value(cloud::kDefaultCloudQuerySignerPort), "associated query signer listen port")
	;

	try {
		boost::program_options::variables_map vm;
		boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(desc).run(), vm);
		boost::program_options::notify(vm);

		if (vm.count("help")) {
			std::cout << desc;
			return 0;
		}

	} catch (std::exception &e) {
		std::cout << "Invalid arguments provided: " << e.what() << std::endl;
		std::cout << "Check --help for usage" << std::endl;
		return 1;
	}

	boost::asio::io_service io_serv;
	cloud::Client client(io_serv);
	boost::asio::signal_set signals_set(io_serv, SIGINT);
	std::function<void(const boost::system::error_code& ec, int n)> handler;
	handler = [&client, &signals_set, &handler](const boost::system::error_code& ec, int n) {
		LOG("Received signal" LG(ec, n));
		if (!ec) {
			client.stop();
		};
	};
	signals_set.async_wait(handler);

	// start qt
	QApplication a(argc, argv);
	ClientMainWindow w(client);
	w.show();
	boost::thread thr([&client, &a, port, public_key_file, agent_host, agent_port, query_signer_host, query_signer_port] () {
		bool client_run_result = client.start(port, public_key_file, agent_host, agent_port, query_signer_host, query_signer_port) ? 0 : 1;
		LOG("" LG(client_run_result));
		a.exit(client_run_result ? 0 : 1);
		LOG("po a.exit");
	});
	LOG("uruchamiam aplikacje");
	int result = a.exec();
	LOG("aplikacja sie zakonczyla" LG(result));
	if (thr.joinable()) {
		LOG("czekam na watek klienta");
		thr.join();
	}
	LOG("watek klienta zakonczony");
	return result;
}

