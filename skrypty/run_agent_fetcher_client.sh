#!/bin/bash

# użycie ./run_agent_fetcher zone port fallback contacts

./cloudatlas_agent --zone $1 --port $2 -f $3 &
./cloudatlas_fetcher --agent-port $2 &
./cloudatlas_client --agent-port $2 &&
fg
