#!/bin/bash

#generuj klucze
./cloudatlas_key_generator

#uruchom podpisywacza
./cloudatlas_query_signer &


## main


#/main/pjwstk/computer1
./cloudatlas_agent --zone /main/pjwstk/computer1 -s random_exp -g 2 --port 9100 -f "127.0.0.1:9101,127.0.0.1:9200,127.0.0.1:9180" 2> agent_main_computer1_log.txt &
./cloudatlas_fetcher --agent-port 9100 2> fetcher_main_computer1_log.txt &

#/main/pjwstk/computer2
./cloudatlas_agent --zone /main/pjwstk/computer2 -s random_exp -g 2 --port 9101 -f "127.0.0.1:9100,127.0.0.1:9200,127.0.0.1:9180" 2> agent_main_computer2_log.txt &
./cloudatlas_fetcher --agent-port 9101 2> fetcher_main_computer2_log.txt &

#/main/uw/khaki13
./cloudatlas_agent --zone /main/uw/khaki13 -s random_exp -g 2 --port 9200 -f "127.0.0.1:9101,127.0.0.1:9201,127.0.0.1:9180" 2> agent_main_khaki13_log.txt &
./cloudatlas_fetcher --agent-port 9200 2> fetcher_main_khaki13_log.txt &

#/main/uw/khaki31
./cloudatlas_agent --zone /main/uw/khaki31 -s random_exp -g 2 --port 9201 -f "127.0.0.1:9101,127.0.0.1:9200,127.0.0.1:9180" 2> agent_main_khaki31_log.txt &
./cloudatlas_fetcher --agent-port 9201 2> fetcher_main_khaki31_log.txt &

#/main/uw/violet07
./cloudatlas_agent --zone /main/uw/violet07 -s random_exp -g 2 --port 9202 -f "127.0.0.1:9101,127.0.0.1:9200,127.0.0.1:9180" 2> agent_main_violet07_log.txt &
./cloudatlas_fetcher --agent-port 9202 2> fetcher_main_violet07_log.txt &


## backup


#/backup/pjwstk/computer1
./cloudatlas_agent --zone /backup/pjwstk/computer1 -s random_exp -g 2 --port 9180 -f "127.0.0.1:9181,127.0.0.1:9280,127.0.0.1:9200" 2> agent_computer1_log.txt &
./cloudatlas_fetcher --agent-port 9180 2> fetcher_computer1_log.txt &

#/backup/pjwstk/computer2
./cloudatlas_agent --zone /backup/pjwstk/computer2 -s random_exp -g 2 --port 9181 -f "127.0.0.1:9180,127.0.0.1:9280,127.0.0.1:9200" 2> agent_computer2_log.txt &
./cloudatlas_fetcher --agent-port 9181 2> fetcher_computer2_log.txt &

#/backup/uw/khaki13
./cloudatlas_agent --zone /backup/uw/khaki13 -s random_exp -g 2 --port 9280 -f "127.0.0.1:9181,127.0.0.1:9281,127.0.0.1:9200" 2> agent_khaki13_log.txt &
./cloudatlas_fetcher --agent-port 9280 2> fetcher_khaki13_log.txt &

#/backup/uw/khaki31
./cloudatlas_agent --zone /backup/uw/khaki31 -s random_exp -g 2 --port 9281 -f "127.0.0.1:9181,127.0.0.1:9280,127.0.0.1:9200" 2> agent_khaki31_log.txt &
./cloudatlas_fetcher --agent-port 9281 2> fetcher_khaki31_log.txt &

#/backup/uw/violet07
./cloudatlas_agent --zone /backup/uw/violet07 -s random_exp -g 2 --port 9282 -f "127.0.0.1:9181,127.0.0.1:9280,127.0.0.1:9200" 2> agent_violet07_log.txt &
./cloudatlas_fetcher --agent-port 9282 2> fetcher_violet07_log.txt &&
fg
