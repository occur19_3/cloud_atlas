#!/bin/bash

#generuj klucze
./cloudatlas_key_generator

#uruchom podpisywacza
./cloudatlas_query_signer &

#/pjwstk/computer1
./cloudatlas_agent --zone /pjwstk/computer1 -s random_exp -g 2 --port 9100 -f "127.0.0.1:9101,127.0.0.1:9200,127.0.0.1:9201,127.0.0.1:9202" 2> agent_computer1_log.txt &
./cloudatlas_fetcher --agent-port 9100 2> fetcher_computer1_log.txt &

#/pjwstk/computer2
./cloudatlas_agent --zone /pjwstk/computer2 -s random_exp -g 2 --port 9101 -f "127.0.0.1:9100,127.0.0.1:9200,127.0.0.1:9201,127.0.0.1:9202" 2> agent_computer2_log.txt &
./cloudatlas_fetcher --agent-port 9101 2> fetcher_computer2_log.txt &

#/uw/khaki13
./cloudatlas_agent --zone /uw/khaki13 -s random_exp -g 2 --port 9200 -f "127.0.0.1:9100,127.0.0.1:9101,127.0.0.1:9201,127.0.0.1:9202" 2> agent_khaki13_log.txt &
./cloudatlas_fetcher --agent-port 9200 2> fetcher_khaki13_log.txt &

#/uw/khaki31
./cloudatlas_agent --zone /uw/khaki31 -s random_exp -g 2 --port 9201 -f "127.0.0.1:9100,127.0.0.1:9101,127.0.0.1:9200,127.0.0.1:9202" 2> agent_khaki31_log.txt &
./cloudatlas_fetcher --agent-port 9201 2> fetcher_khaki31_log.txt &

#/uw/violet07
./cloudatlas_agent --zone /uw/violet07 -s random_exp -g 2 --port 9202 -f "127.0.0.1:9100,127.0.0.1:9101, 127.0.0.1:9200,127.0.0.1:9201" 2> agent_violet07_log.txt &
./cloudatlas_fetcher --agent-port 9202 2> fetcher_violet07_log.txt &&
fg
