#include "network.h"

#include <boost/asio/placeholders.hpp>
#include <boost/bind.hpp>

#include "log_utils.h"
#include "value_types.h"

#undef  LOG_DEFAULT_CATEGORY
#define LOG_DEFAULT_CATEGORY NET


namespace cloud {
NetworkManager::NetworkManager(boost::asio::io_service &io_serv, std::function<void(RcvData &&)> handle_packet): handle_packet_(handle_packet), io_serv_(io_serv), udp_socket_(io_serv_)
{
	if (!handle_packet_) {
		std::runtime_error("musisz dodac handler obslugi przychodzacych pakietow");
	}
}

NetworkManager::~NetworkManager()
{
	stop();
	delete [] udp_snd_buffer_;
	udp_snd_buffer_ = nullptr;
}

bool NetworkManager::start(std::uint16_t port)
{
	LOG("Start" LG(port));
	try {
		udp_socket_ = std::move(boost::asio::ip::udp::socket(io_serv_,
				boost::asio::ip::udp::endpoint(boost::asio::ip::udp::v4(), port)
		));
		if(!udp_socket_.is_open()) {
			LOG(ERROR, "nie udalo sie otworzyc socketu robimy z palca");
			udp_socket_.open(boost::asio::ip::udp::v4());
		}
	} catch (std::exception &e) {
		LOG(ERROR, "nie udalo sie otworzyc socketu" LG(port) << e.what());
		return false;
	}
	start_receive();
	return true;
}

void NetworkManager::stop()
{
	LOG("stopping");
	close_socket(udp_socket_);
	for (auto sp: udp_snd_queue_) {
		delete [] std::get<1>(sp).data_;
	}
	udp_snd_queue_.clear();
	LOG("stopped");
}


void NetworkManager::push_to_udp_send_queue(contact c, PacketData &&p)
{
	LOG("push_to_send_queue" LG(c, p.size_, sending_));
	if (sending_) {
		udp_snd_queue_.push_back(std::make_tuple(c, std::move(p)));
	} else {
		send_udp(std::make_tuple(c, std::move(p)));
	}
}

void NetworkManager::start_receive()
{
	LOG("start_receive" LG(udp_socket_.is_open(), udp_socket_.local_endpoint()));
	udp_socket_.async_receive_from(boost::asio::buffer(udp_rcv_buffer_, kUdpBufferSize), rcv_endpoint_,
		boost::bind(&NetworkManager::handle_receive, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)
	);
}

void NetworkManager::handle_receive(const boost::system::error_code& ec, std::size_t size)
{
	auto rcvd = getCurrentTime();
	LOG("handle_receive" LG(ec.message(), size, rcvd));
	if (!ec)
	{
		LOG("received" LG(size, rcv_endpoint_.address()));
		if (size < sizeof(std::int64_t) + sizeof(PacketType)) {
			LOG(WARN, "dostalismy maly pakiet, nie ma nawet miejsca na timestamp");
		} else {
			RcvData r;
			r.c = contact(rcv_endpoint_.address().to_v4().to_ulong(), rcv_endpoint_.port());
			r.their_snd_timestamp = getTimeFromBuffer(udp_rcv_buffer_);
			r.my_rcv_timestamp = rcvd;
			r.type = static_cast<PacketType>(udp_rcv_buffer_[PacketData::kTypeOffset]);
			r.data = PacketData(udp_rcv_buffer_ + PacketData::kPacketOffset, size - PacketData::kPacketOffset);
			LOG(WARN, "rozpoczynamy obsluge" << r);
			handle_packet_(std::move(r));
		}
	} else {
		LOG(WARN, "problem receiving packet " LG(ec, size));
		if (ec == boost::asio::error::operation_aborted) {
			return;
		}
	}
	start_receive();
}

void NetworkManager::send_udp(SendData &&send_data)
{
	snd_endpoint_ = boost::asio::ip::udp::endpoint(boost::asio::ip::address_v4(std::get<0>(send_data).ip), std::get<0>(send_data).port);
	udp_snd_size_ = std::get<1>(send_data).size_;
	udp_snd_buffer_ = std::get<1>(send_data).data_;
	sending_ = true;
	writeTimeToBuffer(getCurrentTime(), udp_snd_buffer_);
	LOG("NetworkManager async_send_to" LG(snd_endpoint_, udp_snd_size_, udp_socket_.is_open(), udp_socket_.local_endpoint()));
	udp_socket_.async_send_to(boost::asio::buffer(udp_snd_buffer_, udp_snd_size_), snd_endpoint_,
			boost::bind(&NetworkManager::handle_send, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

void NetworkManager::handle_send(const boost::system::error_code& ec, std::size_t size)
{
	LOG("handle_send" LG(ec.message(), udp_snd_size_, size, snd_endpoint_));
	sending_ = false;

	delete[] udp_snd_buffer_;
	udp_snd_buffer_ = nullptr;
	udp_snd_size_ = 0;

	if (!udp_snd_queue_.empty()) {
		send_udp(std::move(udp_snd_queue_.front()));
		udp_snd_queue_.pop_front();
	}
}

void NetworkManager::close_socket(boost::asio::ip::udp::socket &socket)
{
	boost::system::error_code ec;
	if (socket.is_open()) {
		socket.close(ec);
		LOG("close socket" LG(ec));
	}
}

} /* namespace cloud */

std::ostream & operator << (std::ostream &os, const cloud::NetworkManager::RcvData &r)
{
	os << "rcv_packet<" << r.type << ">[" << r.c << ", snd:" << cloud::time(r.their_snd_timestamp) << ",rcv:" << cloud::time(r.my_rcv_timestamp) << "|" << r.data.size_ << "]";
	return os;
}
