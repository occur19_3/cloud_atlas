#pragma once

#include "Absyn.H"

namespace cloud {

class QueryNameInterpreter: public Visitor
{
private:
	std::string query_name_;
	std::vector<std::string> attributes_;

	std::string last_ident_;
	std::string last_attribute_;
public:
	static std::pair<std::string, std::vector<std::string>> process_query(const std::string &query);

	//Program
	void visitProgram(Program* p) override;
	void visitProgramQuery(ProgramQuery* p) override;

	//Stmt
	void visitStmt(Stmt* p) override;
	void visitListStmt(ListStmt* p) override;
	void visitSelStmt(SelStmt* p) override;
	void visitSelOrdStmt(SelOrdStmt* p) override;
	void visitSelWhrStmt(SelWhrStmt* p) override;
	void visitSelWhrOrdStmt(SelWhrOrdStmt* p) override;

	//SelectClause
	void visitSelectClause(SelectClause* p) override;
	void visitSelectClauseRule(SelectClauseRule* p) override;

	//WhereClause
	void visitWhereClause(WhereClause* p) override;
	void visitWhereClauseRule(WhereClauseRule* p) override;

	//OrderClause
	void visitOrderClause(OrderClause* p) override;
	void visitOrderClauseRule(OrderClauseRule* p) override;

	//Order
	void visitOrder(Order* p) override;
	void visitListOrder(ListOrder* p) override;
	void visitOrderSim(OrderSim* p) override;
	void visitOrderNulls(OrderNulls* p) override;
	void visitOrderOptSim(OrderOptSim* p) override;
	void visitOrderOptNulls(OrderOptNulls* p) override;

	//OrderOpt
	void visitOrderOpt(OrderOpt* p) override;
	void visitOrderOptAsc(OrderOptAsc* p) override;
	void visitOrderOptDesc(OrderOptDesc* p) override;

	//OrderNullsOpr
	void visitOrderNullsOpt(OrderNullsOpt* p) override;
	void visitOrderNullsFirst(OrderNullsFirst* p) override;
	void visitOrderNullsLast(OrderNullsLast* p) override;

	//Select
	void visitSelect(Select* p) override;
	void visitListSelect(ListSelect* p) override;
	void visitSelectSim(SelectSim* p) override;
	void visitSelectAs(SelectAs* p) override;

	//SelectStart
	void visitSelectStart(SelectStart* p) override;
	void visitSelectStartSim(SelectStartSim* p) override;
	void visitSelectStartOpt(SelectStartOpt* p) override;

	//SelModOpt
	void visitSelModOpt(SelModOpt* p) override;
	void visitSelModOptAll(SelModOptAll* p) override;
	void visitSelModOptDist(SelModOptDist* p) override;

	//Expr
	void visitExpr(Expr* p) override;
	void visitListExpr(ListExpr* p) override;

	void visitEOr(EOr* p) override;
	void visitEAnd(EAnd* p) override;
	void visitENot(ENot* p) override;
	void visitERel(ERel* p) override;
	void visitERegexp(ERegexp* p) override;
	void visitEAdd(EAdd* p) override;
	void visitEMul(EMul* p) override;
	void visitENeg(ENeg* p) override;


	void visitETrue(ETrue* p) override;
	void visitEFalse(EFalse* p) override;
	void visitEInt(EInt* p) override;
	void visitEDouble(EDouble* p) override;
	void visitEString(EString* p) override;
	void visitEVar(EVar* p) override;

	void visitEFun(EFun* p) override;
	void visitEStmt(EStmt* p) override;

	void visitEBraceEmpty(EBraceEmpty* p) override;
	void visitEBracketExpr(EBracketExpr* p) override;
	void visitEFunStar(EFunStar* p) override;

	//RelOp
	void visitRelOp(RelOp* p) override;
	void visitLTH(LTH* p) override;
	void visitLE(LE* p) override;
	void visitGTH(GTH* p) override;
	void visitGE(GE* p) override;
	void visitEQU(EQU* p) override;
	void visitNE(NE* p) override;

	//AddOp
	void visitAddOp(AddOp* p) override;
	void visitPlusOp(PlusOp* p) override;
	void visitMinusOp(MinusOp* p) override;

	//MulOp
	void visitMulOp(MulOp* p) override;
	void visitTimesOp(TimesOp* p) override;
	void visitDivideOp(DivideOp* p) override;
	void visitModuloOp(ModuloOp* p) override;

	//BaseValues
	void visitChar(Char x) override;
	void visitDouble(Double x) override;
	void visitIdent(Ident x) override;
	void visitInteger(Integer x) override;
	void visitString(String x) override;

};


}
