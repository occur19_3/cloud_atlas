#pragma once

#include "crypto.h"
#include "network.h"

namespace cloud {

class QuerySigner {	
public:
	struct QueryData {
		time validity_;
		time timestamp_;
		std::set<std::string> attributes_;
	};

	QuerySigner(boost::asio::io_service &io_serv);

	bool start(std::uint16_t port, const std::string &public_key_file, const std::string &secret_key_file);
	void stop();

private:
	bool load_and_verify_keys(const std::string &public_key_file, const std::string &secret_key_file);
	void packet_arrived(NetworkManager::RcvData &&packet);

	serialization::signature_wrapper installQuery(const std::string &query, const std::string &query_name, const std::vector<std::string> &attrs, time validity, time timestamp);
	std::pair<serialization::signature_wrapper, time> uninstallQuery(const std::string &query, const std::string &query_name, const std::vector<std::string> &attrs, time timestamp);

	void eraseQuery(std::map<std::string, QueryData>::iterator iter);
	boost::asio::io_service &io_serv_;
	NetworkManager network_;

	PublicKey pk_;
	PrivateKey sk_;
	std::map<std::string, QueryData> installed_queries_;
	std::set<std::string> installed_attributes_;
};

} /* namespace cloud */
