#include <iostream>

#include <boost/asio/io_service.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/program_options.hpp>

#include "crypto.h"
#include "log_utils.h"
#include "query_signer.h"

#undef  LOG_DEFAULT_CATEGORY
#define LOG_DEFAULT_CATEGORY SIGNER

int main(int argc, char* argv[])
{
	std::uint16_t port;
	std::string public_key_file;
	std::string secret_key_file;

	boost::program_options::options_description desc("CloudAtlas QuerySigner options");
	desc.add_options()
		("help", "Shows help message")
		("port,P", boost::program_options::value<std::uint16_t>(&port)->default_value(cloud::kDefaultCloudQuerySignerPort), "QuerySigner listen port")
		("public-key-file,p", boost::program_options::value<std::string>(&public_key_file)->default_value("public_key.key"), "Public key outfile")
		("secret-key-file,s", boost::program_options::value<std::string>(&secret_key_file)->default_value("secret_key.key"), "Secret key outfile")
	;
	try {
		boost::program_options::variables_map vm;
		boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(desc).run(), vm);
		boost::program_options::notify(vm);

		if (vm.count("help")) {
			std::cout << desc;
			return 0;
		}
		if (vm.count("port") == 0) {
			throw std::logic_error("please specify listening port");
		}

	} catch (std::exception &e) {
		std::cout << "Invalid arguments provided: " << e.what() << std::endl;
		std::cout << "Check --help for usage" << std::endl;
		return 1;
	}

	boost::asio::io_service io_serv;
	cloud::QuerySigner signer(io_serv);
	boost::asio::signal_set signals_set(io_serv, SIGINT);
	signals_set.async_wait([&signer](const boost::system::error_code& ec, int n) {
		LOG("Received signal" LG(ec, n));
		if (!ec) {
			signer.stop();
		}
	});
	return signer.start(port, public_key_file, secret_key_file) ? 0 : 1;
}

