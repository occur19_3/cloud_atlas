#include "query_signer.h"

#include "log_utils.h"
#include "Parser.H"
#include "query_validation.h"

#undef  LOG_DEFAULT_CATEGORY
#define LOG_DEFAULT_CATEGORY SIGNER

namespace cloud {

QuerySigner::QuerySigner(boost::asio::io_service &io_serv): io_serv_(io_serv), network_(io_serv, [this] (NetworkManager::RcvData && p) { packet_arrived((std::move(p))); })
{}

bool QuerySigner::start(std::uint16_t port, const std::string &public_key_file, const std::string &secret_key_file)
{
	LOG("starting query signer" LG(port, public_key_file, secret_key_file));
	if (!network_.start(port)) {
		LOG(ERROR, "cannot start network" LG(port));
		return false;
	}
	if (!load_and_verify_keys(public_key_file, secret_key_file)) {
		LOG(ERROR, "problem with keys" LG(public_key_file, secret_key_file));
		return false;
	}
	io_serv_.run();
	LOG("finishing");
	return true;
}

void QuerySigner::stop()
{
	LOG("stopping");
	network_.stop();
	io_serv_.stop();
	LOG("stopped");
}

bool QuerySigner::load_and_verify_keys(const std::string &public_key_file, const std::string &secret_key_file)
{
	if (!cloud::read_from_file(public_key_file, pk_)
		|| !cloud::read_from_file(secret_key_file, sk_)) {
		LOG(ERROR, "cannot read keys" LG(public_key_file, secret_key_file));
		return false;
	}
	std::string test_query = "testowe query do sprawdzenia poprawnosci zapisu i odczytu kluczy";
	time t = time::now();
	cloud::Signature signature = cloud::signQuery(test_query, true, t, t, sk_, 0);
	return cloud::verifySignature(test_query, true, t, t, signature, pk_);
}

void QuerySigner::packet_arrived(NetworkManager::RcvData &&packet)
{
	LOG("packet_arrived" LG(packet));
	switch (packet.type) {
	case PacketType::QuerySignRequest:
	{
		try {
			Packet_QuerySignRequest req = deserializePacket<Packet_QuerySignRequest>(packet.data);
			LOG("packet_arrived succesfully parsed" LG(req));

			std::pair<std::string, std::vector<std::string>> query_attrs = QueryNameInterpreter::process_query(req.query_);
			time val = time(0);
			time t = time::now();
			serialization::signature_wrapper sw;
			if (!query_attrs.first.empty() && !query_attrs.second.empty()) {
				if (req.install_) {
					val = req.validity_;
					sw = installQuery(req.query_, query_attrs.first, query_attrs.second, req.validity_, t);
				} else {
					auto res = uninstallQuery(req.query_, query_attrs.first, query_attrs.second, t);
					val = res.second;
					sw = res.first;
				}
			} else {
				LOG(WARN, "dostalismy lipne query do podpisania" LG(req.query_));
			}

			Packet_QuerySignResponse res(req.id_, val, t, sw);
			PacketData res_data = serializePacketSetTypeAndAddTimeStampOffset<Packet_QuerySignResponse>(res);
			LOG("query succesfully signed sending response packet" LG(res));
			network_.push_to_udp_send_queue(packet.c, std::move(res_data));
		} catch (std::exception &e) {
			LOG(WARN, "unable to parse packet" LG(e.what()));
		}
	}
		break;
	default:
		LOG("unknown packet" LG(packet));
		break;
	}
	delete[] packet.data.data_;
}

serialization::signature_wrapper QuerySigner::installQuery(const std::string &query, const std::string &query_name, const std::vector<std::string> &attrs, time validity, time timestamp)
{
	serialization::signature_wrapper sw;
	if (validity < timestamp) {
		LOG(WARN, "niepoprawna data");
		return sw;
	}
	auto iter = installed_queries_.find(query);
	if (iter != installed_queries_.end()) {
		if (iter->second.validity_ > timestamp) {
			LOG("Instalujemy query juz istnieje" LG(query_name));
			return sw;
		}
		eraseQuery(iter);
	}
	for (auto a: attrs) {
		if (installed_attributes_.find(a) != installed_attributes_.end()) {
			LOG("Instalujemy atrybut juz istnieje" LG(a, installed_attributes_));
			return sw;
		}
	}
	QueryData qd;
	qd.attributes_.insert(attrs.begin(), attrs.end());
	if (qd.attributes_.size() != attrs.size()) {
		LOG(WARN, "w query mamy zduplikowane atrybuty");
		return sw;
	}
	qd.validity_ = validity;
	qd.timestamp_ = timestamp;
	insertIntoMap(installed_queries_, query, qd);
	installed_attributes_.insert(qd.attributes_.begin(), qd.attributes_.end());
	sw = cloud::signQuery(query, true, validity, timestamp, sk_, 1);
	return sw;
}

std::pair<serialization::signature_wrapper, time> QuerySigner::uninstallQuery(const std::string &query, const std::string &query_name, const std::vector<std::string> &attrs, time timestamp)
{
	serialization::signature_wrapper sw;
	auto iter = installed_queries_.find(query);
	if (iter == installed_queries_.end()) {
		LOG(WARN, "nie am takiego query");
		return std::make_pair(sw, time());
	}
	time val = iter->second.validity_;
	eraseQuery(iter);
	sw = cloud::signQuery(query, false, val, timestamp, sk_, 1);
	return std::make_pair(sw, val);
}


void QuerySigner::eraseQuery(std::map<std::string, QueryData>::iterator iter) {
	for (auto a: iter->second.attributes_) {
		installed_attributes_.erase(a);
	}
	installed_queries_.erase(iter);
}

} /* namespace cloud */
