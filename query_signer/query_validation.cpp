#include "query_validation.h"

#include "log_utils.h"
#include "Parser.H"

#undef  LOG_DEFAULT_CATEGORY
#define LOG_DEFAULT_CATEGORY NORMAL


namespace cloud {

std::pair<std::string, std::vector<std::string>> QueryNameInterpreter::process_query(const std::string &query)
{
	Program* p = pProgram(query.c_str());
	if (!p) {
		return std::make_pair("", std::vector<std::string>());
	}
	QueryNameInterpreter qni;
	p->accept(&qni);
	delete p;
	return std::make_pair(qni.query_name_, qni.attributes_);
}

/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * Program * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryNameInterpreter::visitProgram(Program* t) {} //abstract class

void QueryNameInterpreter::visitProgramQuery(ProgramQuery *programquery)
{
	visitIdent(programquery->ident_);
	query_name_ = last_ident_;
	programquery->liststmt_->accept(this);
}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * Stmt  * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryNameInterpreter::visitStmt(Stmt* t) {} //abstract class

void QueryNameInterpreter::visitListStmt(ListStmt* liststmt)
{
	for (ListStmt::iterator i = liststmt->begin() ; i != liststmt->end() ; ++i) {
		(*i)->accept(this);
	}	
}

void QueryNameInterpreter::visitSelStmt(SelStmt *selstmt)
{
	selstmt->selectclause_->accept(this);
}

void QueryNameInterpreter::visitSelOrdStmt(SelOrdStmt *selordstmt)
{
	selordstmt->selectclause_->accept(this);
}

void QueryNameInterpreter::visitSelWhrStmt(SelWhrStmt *selwhrstmt)
{
	selwhrstmt->selectclause_->accept(this);
}

void QueryNameInterpreter::visitSelWhrOrdStmt(SelWhrOrdStmt *selwhrordstmt)
{
	selwhrordstmt->selectclause_->accept(this);
}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * SelectClause  * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryNameInterpreter::visitSelectClause(SelectClause* t) {} //abstract class

void QueryNameInterpreter::visitSelectClauseRule(SelectClauseRule *selectclauserule)
{
	LOG("processing SelectClauseRule");
	selectclauserule->listselect_->accept(this);
}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * WhereClause * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryNameInterpreter::visitWhereClause(WhereClause* t) {} //abstract class


void QueryNameInterpreter::visitWhereClauseRule(WhereClauseRule *whereclauserule) {}



/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * OrderClause * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryNameInterpreter::visitOrderClause(OrderClause* t) {} //abstract class

void QueryNameInterpreter::visitOrderClauseRule(OrderClauseRule *orderclauserule) {}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * Order * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryNameInterpreter::visitOrder(Order* t) {} //abstract class

void QueryNameInterpreter::visitListOrder(ListOrder* listorder) {}

void QueryNameInterpreter::visitOrderSim(OrderSim *ordersim) {}

void QueryNameInterpreter::visitOrderNulls(OrderNulls *ordernulls) {}

void QueryNameInterpreter::visitOrderOptSim(OrderOptSim *orderoptsim) {}

void QueryNameInterpreter::visitOrderOptNulls(OrderOptNulls *orderoptnulls) {}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * OrderOpt  * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryNameInterpreter::visitOrderOpt(OrderOpt*) {} //abstract class

void QueryNameInterpreter::visitOrderOptAsc(OrderOptAsc *) {}

void QueryNameInterpreter::visitOrderOptDesc(OrderOptDesc *) {}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * OrderNullsOpt * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryNameInterpreter::visitOrderNullsOpt(OrderNullsOpt* ) {} //abstract class

void QueryNameInterpreter::visitOrderNullsFirst(OrderNullsFirst *) {}

void QueryNameInterpreter::visitOrderNullsLast(OrderNullsLast *) {}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * Select  * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryNameInterpreter::visitSelect(Select*) {} //abstract class

void QueryNameInterpreter::visitListSelect(ListSelect* listselect)
{
	for (ListSelect::iterator i = listselect->begin() ; i != listselect->end() ; ++i)
	{
		(*i)->accept(this);
		attributes_.push_back(last_attribute_);
	}
}

void QueryNameInterpreter::visitSelectSim(SelectSim *selectsim)
{
	last_attribute_ = "";
}

void QueryNameInterpreter::visitSelectAs(SelectAs *selectas)
{
	visitIdent(selectas->ident_);
	last_attribute_ = last_ident_;
}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * SelectStart * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryNameInterpreter::visitSelectStart(SelectStart*) {} //abstract class

void QueryNameInterpreter::visitSelectStartSim(SelectStartSim *selectstartsim) {}

void QueryNameInterpreter::visitSelectStartOpt(SelectStartOpt *selectstartopt) {}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * SelModOpt * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryNameInterpreter::visitSelModOpt(SelModOpt*) {} //abstract class

void QueryNameInterpreter::visitSelModOptAll(SelModOptAll *) {}

void QueryNameInterpreter::visitSelModOptDist(SelModOptDist *) {}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * Expr  * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryNameInterpreter::visitExpr(Expr*) {} //abstract class

 void QueryNameInterpreter::visitListExpr(ListExpr* listexpr) {}

void QueryNameInterpreter::visitEOr(EOr *eor) {}

void QueryNameInterpreter::visitEAnd(EAnd *eand) {}

void QueryNameInterpreter::visitENot(ENot *enot) {}

void QueryNameInterpreter::visitERel(ERel *erel) {}

void QueryNameInterpreter::visitERegexp(ERegexp *eregexp) {}

void QueryNameInterpreter::visitEAdd(EAdd *eadd) {}

void QueryNameInterpreter::visitEMul(EMul *emul) {}

void QueryNameInterpreter::visitENeg(ENeg *eneg) {}

void QueryNameInterpreter::visitETrue(ETrue *etrue) {}

void QueryNameInterpreter::visitEFalse(EFalse *efalse) {}

void QueryNameInterpreter::visitEInt(EInt *eint) {}

void QueryNameInterpreter::visitEDouble(EDouble *edouble) {}

void QueryNameInterpreter::visitEString(EString *estring) {}

void QueryNameInterpreter::visitEVar(EVar *evar) {}

void QueryNameInterpreter::visitEFun(EFun *efun) {}

void QueryNameInterpreter::visitEStmt(EStmt *estmt) {}

void QueryNameInterpreter::visitEBraceEmpty(EBraceEmpty *ebraceempty) {}

void QueryNameInterpreter::visitEBracketExpr(EBracketExpr *ebracketexpr) {}

void QueryNameInterpreter::visitEFunStar(EFunStar *efunstar) {}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * RelOp * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryNameInterpreter::visitRelOp(RelOp*) {} //abstract class

void QueryNameInterpreter::visitLTH(LTH *) {}

void QueryNameInterpreter::visitLE(LE *) {}

void QueryNameInterpreter::visitGTH(GTH *) {}

void QueryNameInterpreter::visitGE(GE *) {}

void QueryNameInterpreter::visitEQU(EQU *) {}

void QueryNameInterpreter::visitNE(NE *) {}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * AddOp * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryNameInterpreter::visitAddOp(AddOp*) {} //abstract class

void QueryNameInterpreter::visitPlusOp(PlusOp *) {}

void QueryNameInterpreter::visitMinusOp(MinusOp *) {}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * MulOp * * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryNameInterpreter::visitMulOp(MulOp*) {} //abstract class

void QueryNameInterpreter::visitTimesOp(TimesOp *) {}

void QueryNameInterpreter::visitDivideOp(DivideOp *) {}

void QueryNameInterpreter::visitModuloOp(ModuloOp *) {}


/* * * * * * * * * * * * * * * * * * * * * * * */
/* * * * * * * * * * Values  * * * * * * * * * */
/* * * * * * * * * * * * * * * * * * * * * * * */


void QueryNameInterpreter::visitChar(Char x) {}

void QueryNameInterpreter::visitDouble(Double x) {}

void QueryNameInterpreter::visitIdent(Ident x)
{
	last_ident_ = x;
}

void QueryNameInterpreter::visitInteger(Integer x) {}

void QueryNameInterpreter::visitString(String x) {}

} /* namespace cloud */
