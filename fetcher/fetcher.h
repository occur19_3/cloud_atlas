#pragma once

#include <boost/asio/ip/address_v4.hpp>
#include <boost/asio/deadline_timer.hpp>

#include "network.h"
#include "utils.h"

namespace cloud {

enum class AverageStrategy: std::uint8_t;

class Fetcher {
public:
	Fetcher(boost::asio::io_service &io_serv);

	bool start(const std::string &agent_host, std::uint16_t agent_port, std::uint32_t update_period, std::uint32_t average_period,
			const std::string &average_strategy);
	void stop();

private:
	void packet_arrived(NetworkManager::RcvData &&packet);
	void gather_and_send_attributes(boost::system::error_code ec);

	std::tuple<size_t, bool, double> get_cpu_usage_data();

	boost::asio::io_service &io_serv_;
	RepeatTimer update_timer_;

	NetworkManager network_;

	contact agent_;
	std::uint32_t update_period_;
	std::uint32_t average_period_;
	AverageStrategy average_strategy_;

	struct cpu_usage_stats {
		long long unsigned int user;
		long long unsigned int user_low;
		long long unsigned int sys;
		long long unsigned int idle;
	};

	std::list<cpu_usage_stats> cpu_load_vals_;
};

#define AVERAGE_STRATEGY_LIST \
	CLOUD_ENUM(normal)

enum class AverageStrategy: std::uint8_t {
#define CLOUD_ENUM(X) X,
	AVERAGE_STRATEGY_LIST
	LastUnknown
#undef CLOUD_ENUM
};

AverageStrategy parseAverageStrategy(const std::string &str);

} /* namespace cloud */

std::ostream & operator << (std::ostream & os, ::cloud::AverageStrategy v);
