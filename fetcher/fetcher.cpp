#include "fetcher.h"

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sys/statvfs.h>
#include <sys/sysinfo.h>
#include <sys/types.h>
#include <sys/utsname.h>

#include <boost/asio/placeholders.hpp>
#include <boost/bind/bind.hpp>

#include "log_utils.h"

#undef  LOG_DEFAULT_CATEGORY
#define LOG_DEFAULT_CATEGORY FETCH

namespace cloud {

Fetcher::Fetcher(boost::asio::io_service &io_serv):
	io_serv_(io_serv),
	update_timer_(io_serv, boost::bind(&Fetcher::gather_and_send_attributes, this, boost::asio::placeholders::error)),
	network_(io_serv, [this] (NetworkManager::RcvData && p) { packet_arrived((std::move(p))); })
{}

bool Fetcher::start(const std::string &agent_host, std::uint16_t agent_port, std::uint32_t update_period, std::uint32_t average_period,
					const std::string &average_strategy)
{
	LOG("starting fetcher" LG(agent_host, agent_port, update_period, average_period, average_strategy));
	if (!agent_.fromString(agent_host, agent_port) || !agent_.isSet()) {
		LOG(ERROR, "Invalid agent host address or port" LG(agent_, agent_host, agent_port));
		return false;
	}
	if (!network_.start(0)) {
		LOG(ERROR, "Unable to start netowrk module");
		return false;
	}

	update_period_ = update_period;
	average_period_ = average_period;
	average_strategy_ = parseAverageStrategy(average_strategy);

	if (update_period_ == 0 || average_period_ == 0 || average_strategy_ == AverageStrategy::LastUnknown) {
		LOG(ERROR, "Invalid params provided" LG(update_period, average_period, average_strategy));
		return false;
	}
	update_timer_.rerun_time_ms_ = update_period * 1000;
	update_timer_.repeat();
	io_serv_.run();
	LOG("finishing");
	return true;
}

void Fetcher::stop()
{
	LOG("stopping");
	network_.stop();
	io_serv_.stop();
	LOG("stopped");
}

void Fetcher::packet_arrived(NetworkManager::RcvData &&packet)
{
	delete[] packet.data.data_;
}

void Fetcher::gather_and_send_attributes(boost::system::error_code ec)
{
	LOG("gather_and_send_attributes start" LG(ec.message()));
	if (!ec) {
		std::list<serialization::Attribute> attrs;

		/// meminfo and num_processes

		struct sysinfo mem_info;
		if (sysinfo(&mem_info)) {
			LOG("nie udalo sie name mem_info");
		} else {
			attrs.push_back(serialization::Attribute("free_ram", integer_vt, new serialization::ValueInteger(mem_info.freeram)));
			attrs.push_back(serialization::Attribute("total_ram", integer_vt, new serialization::ValueInteger(mem_info.totalram)));
			attrs.push_back(serialization::Attribute("free_swap", integer_vt, new serialization::ValueInteger(mem_info.freeswap)));
			attrs.push_back(serialization::Attribute("total_swap", integer_vt, new serialization::ValueInteger(mem_info.totalswap)));
			attrs.push_back(serialization::Attribute("num_processes", integer_vt, new serialization::ValueInteger(mem_info.procs)));
		}

		/// processes usage

		auto cpu_usage = get_cpu_usage_data();
		LOG("cpu_usage stats " << cpu_usage);
		if (!std::get<1>(cpu_usage)) {
			attrs.push_back(serialization::Attribute("cpu_load", double_vt, new serialization::ValueDouble(std::get<2>(cpu_usage))));
		}
		attrs.push_back(serialization::Attribute("num_cores", integer_vt, new serialization::ValueInteger(std::get<0>(cpu_usage))));

		/// disk space

		struct statvfs disk;
		if (statvfs("/", &disk)) {
			LOG("nie udalo sie name statvfs");
		} else {
			unsigned long long free_disk = disk.f_bfree * disk.f_frsize;
			unsigned long long total_disk = disk.f_blocks * disk.f_frsize;
			attrs.push_back(serialization::Attribute("free_disk", integer_vt, new serialization::ValueInteger(free_disk)));
			attrs.push_back(serialization::Attribute("total_disk", integer_vt, new serialization::ValueInteger(total_disk)));
		}

		/// kernel version and dns_names

		struct utsname ut;
		if (uname(&ut)) {
			LOG("nie udalo sie uname");
		} else {
			std::string kernel_ver = std::string() + ut.sysname + " " + ut.release + " " + ut.version;
			attrs.push_back(serialization::Attribute("kernel_ver", string_vt, new serialization::ValueString(kernel_ver)));
			attrs.push_back(serialization::Attribute("machine_dns_names", set_string_vt, new serialization::ValueSet(
				std::list<serialization::Value*>{new serialization::ValueString(ut.nodename)})));
		}

		/// logged_users

		FILE* logged_f = popen("users | wc -w", "r");
		if (logged_f) {
			long unsigned logged_users = 0;
			fscanf(logged_f, "%lu", &logged_users);
			attrs.push_back(serialization::Attribute("logged_users", integer_vt, new serialization::ValueInteger(logged_users)));
			pclose(logged_f);
		}

		/// create and send packet

		Packet_FetcherData fetcher_data(attrs);
		LOG("gather_and_send_attributes" LG(fetcher_data));
		PacketData pd = serializePacketSetTypeAndAddTimeStampOffset(fetcher_data);
		network_.push_to_udp_send_queue(agent_, std::move(pd));
		update_timer_.repeat();
	}
}

std::tuple<size_t, bool, double> Fetcher::get_cpu_usage_data()
{
	size_t num_cores = 0;
	bool overflow = false;
	double cpu_load = 0.0;
	{
		std::ifstream ifs("/proc/stat");
		std::string line;
		if (!std::getline(ifs, line)) {
			LOG("unable to open /proc/stat");
			return std::make_tuple(num_cores, overflow, cpu_load);
		}
		cpu_usage_stats curr;
		::sscanf(line.c_str(), "cpu %llu %llu %llu %llu", &curr.user, &curr.user_low, &curr.sys, &curr.idle);

		if (!cpu_load_vals_.empty()) {
			cpu_usage_stats & last = cpu_load_vals_.front();

			if (curr.user < last.user
					|| curr.user_low < last.user_low
					|| curr.sys < last.sys
					|| curr.idle < last.idle)
			{
				LOG("Overflow detection. Just skip this value."
					LG(curr.user, curr.user_low, curr.sys, curr.idle)
					LG(last.user, last.user_low, last.sys, last.idle));
				overflow = true;
			} else {
				long long unsigned int total = (curr.user - last.user)
						+ (curr.user_low - last.user_low)
						+ (curr.sys = last.sys);
				cpu_load = total;
				total += (curr.idle - last.idle);
				cpu_load /= total;
			}
		}
		cpu_load_vals_.push_back(std::move(curr));
		if (cpu_load_vals_.size() > average_period_) {
			cpu_load_vals_.pop_front();
		}
		while (std::getline(ifs, line) && line.substr(0,3) == std::string("cpu")) ++num_cores;
	}
	return std::make_tuple(num_cores, overflow, cpu_load);
}

AverageStrategy parseAverageStrategy(const std::string &str)
{
#define CLOUD_ENUM(X) \
	if (str == #X) { \
		return AverageStrategy::X; \
	} else

	AVERAGE_STRATEGY_LIST
#undef CLOUD_ENUM
	{
		return AverageStrategy::LastUnknown;
	}
}

} /* namespace cloud */

std::ostream & operator << (std::ostream & os, ::cloud::AverageStrategy v)
{
#define CLOUD_ENUM(X) \
	case ::cloud::AverageStrategy::X: \
		os << #X; \
		break;

	switch (v) {
		AVERAGE_STRATEGY_LIST
	default:
		os << "";
		break;
	}
	return os;
#undef CLOUD_ENUM
}
