#include <iostream>
#include <string>

#include <boost/program_options.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/signal_set.hpp>

#include "fetcher.h"
#include "log_utils.h"

#undef  LOG_DEFAULT_CATEGORY
#define LOG_DEFAULT_CATEGORY FETCH

int main(int argc, char* argv[])
{
	std::string agent_host;
	std::uint16_t agent_port;
	std::uint32_t update_period;
	std::uint32_t average_period;
	std::string average_strategy;

	boost::program_options::options_description desc("CloudAtlas Fetcher options");
	desc.add_options()
		("help", "Shows help message")
		("agent-host,h", boost::program_options::value<std::string>(&agent_host)->default_value(cloud::kDefaultCloudHost), "associated agent listen host")
		("agent-port,p", boost::program_options::value<std::uint16_t>(&agent_port)->default_value(cloud::kDefaultCloudAgentPort), "associated agent listen port")
		("update-period,u", boost::program_options::value<std::uint32_t>(&update_period)->default_value(5), "update agent value period")
		("average-period,A", boost::program_options::value<std::uint32_t>(&average_period)->default_value(5), "average computation period")
		("average-strategy,s", boost::program_options::value<std::string>(&average_strategy)->default_value("normal"), "average computation strategy")
	;

	try {
		boost::program_options::variables_map vm;
		boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(desc).run(), vm);
		boost::program_options::notify(vm);

		if (vm.count("help")) {
			std::cout << desc;
			return 0;
		}

	} catch (std::exception &e) {
		std::cout << "Invalid arguments provided: " << e.what() << std::endl;
		std::cout << "Check --help for usage" << std::endl;
		return 1;
	}

	boost::asio::io_service io_serv;
	boost::asio::io_service::work io_work(io_serv);	//!< zeby nam sie czasem nie zatrzymal, bo nie nasluchujemy nigdzie
	cloud::Fetcher fetcher(io_serv);
	boost::asio::signal_set signals_set(io_serv, SIGINT);
	signals_set.async_wait([&fetcher](const boost::system::error_code& ec, int n) {
		LOG("Received signal" LG(ec, n));
		if (!ec) {
			fetcher.stop();
		}
	});
	return fetcher.start(agent_host, agent_port, update_period, average_period, average_strategy);
}

